<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home_Controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login'] = "home_Controller/login";

$route['student_involve'] = "Home_Controller/student_involve";
$route['image_vs_reality_internship'] = "Home_Controller/image_vs_reality_internship";

$route['home'] = 'home_Controller/index';
$route['youth_Empower'] = 'Home_Controller/youth_Empower';
$route['alternate_edu_skill_devep'] = 'Home_Controller/alternate_edu_skill_devep';
$route['levelihood_entrepreneurship'] = 'Home_Controller/levelihood_entrepreneurship';

$route['women_empowerment'] = 'Home_Controller/women_empowerment';

$route['connecting_communities'] = 'Home_Controller/connecting_communities';

$route['tribal_culture'] = 'Home_Controller/tribal_culture';
$route['logout'] = 'home_Controller/logut';
$route['aboutshivganga'] = 'home_Controller/aboutshivganga';
$route['contribute_now'] = 'home_Controller/contribute_now';
$route['showdoner'] = 'home_Controller/showdoner';
$route['blogfullview'] = 'home_Controller/blogfullview';
$route['aboutjhabua'] = 'home_Controller/aboutjhabua';
$route['award'] = 'home_Controller/award';
$route['waterconservation'] = 'home_Controller/waterconservation';
$route['programforest'] = 'home_Controller/programforest';
$route['projectsaadhtalab'] = 'home_Controller/projectsaadhtalab';
$route['blogs'] = 'home_Controller/blogs';
$route['events'] = 'home_Controller/events';
$route['joinus'] = 'home_Controller/joinus';
$route['donate'] = 'home_Controller/donate';

$route['donate/(:any)'] = 'home_Controller/donate/$1';
$route['halma2020'] = 'home_Controller/halma2020';
$route['eventsviewmore'] = 'home_Controller/eventsviewmore';
$route['add-more'] = "AddMoreController";
$route['add-more-post']['post'] = "AddMoreController/storePost";
// $route['thanks'] = 'payment/RazorThankYou';


$route['Admin'] = 'admin/adminpanel';


// $route['aboutshivganga'] = 'home_Controller/aboutshivganga';