<?php
class Blog_model extends CI_Model
{

    public function _construct()
    {
    }
    public function get_blog()
    {


        // $this->db->select('LAST(program)');
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('program', 8);
        return $query->result();
    }
    public function record_count()
    {
        return $this->db->count_all("program");
    }
    public function get_program($limit, $start)
    {


        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get("program");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_banner()
    {

        $query = $this->db->get('banner');
        return $query->result();
    }
    public function get_contribution()
    {

        $query = $this->db->get('contribution');
        return $query->result();
    }
    public function get_comment()
    {

        $query = $this->db->get('blog_comments');
        return $query->result();
    }
    public function delete_blog_comment($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('blog_comments');
    }
    public function get_recentproject()
    {

        $query = $this->db->get('recentproject');
        return $query->result();
    }
    public function get_extraActivities()
    {

        $query = $this->db->get('extra-activities');
        return $query->result();
    }
    public function get_campaign()
    {

        $query = $this->db->get('campaign');
        return $query->result();
    }
    public function campaignfullview($slug)
    {

        $this->db->where('slug', $slug);
        $q = $this->db->get('campaign');
        return $q->result();
    }
    public function get_banner1()
    {

        // $this->db->where('id', $id1);
        $q = $this->db->get('banner');
        return $q->result();
    }
    public function get_contact_us()
    {

        $q = $this->db->get('contact_us');
        return $q->result();
    }
    public function blogfullview($slug)
    {

        $this->db->where('slug', $slug);
        $q = $this->db->get('program');
        return $q->result();
    }
    public function get_blog_comment($slug)
    {

        $this->db->where('slug', $slug);
        $q = $this->db->get('blog_comments');
        return $q->result();
    }
    public function blogfullview2($slug)
    {

        $this->db->where('slug', $slug);
        $q = $this->db->get('blog');
        return $q->result();
    }
    public function get_latest_blog()
    {

        $this->db->select("*");
        $this->db->from("blog");
        $this->db->limit(5);
        $this->db->order_by('id', "DESC");
        $query = $this->db->get();
        $result = $query->result();


        return $query->result();
    }
    public function get_user_name($user_id)
    {

        $this->db->where('id', $user_id);
        $q = $this->db->get('users');
        return $q->result();
    }
    public function get_cause($cause)
    {

        $this->db->select('title');
        $this->db->from('contribution');
        $this->db->where('id', $cause);
        return $this->db->get()->result();
    }
     
    public function get_user_id($email)
    {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email);
        return $this->db->get()->result();
    }
    public function get_payment_details($email)
    {

        $this->db->where('email', $email);
        $q = $this->db->get('payments');
        return $q->result();
    }
    function generateNumericOTP($n)
    {

        // Take a generator string which consist of 
        // all numeric digits 
        $generator = "1357902468";

        // Iterate for n-times and pick a single character 
        // from generator and append it to $result 

        // Login for generating a random character from generator 
        //     ---generate a random number 
        //     ---take modulus of same with length of generator (say i) 
        //     ---append the character at place (i) from generator to result 

        $result = "";

        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand() % (strlen($generator))), 1);
        }

        // Return result 
        return $result;
    }
    function random_number($maxlength = 17)
    {
        $chary = array(
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
        );
        $return_str = "";
        for ($x = 0; $x <= $maxlength; $x++) {
            $return_str .= $chary[rand(0, count($chary) - 1)];
        }
        return $return_str;
    }
    public function check_email($email)
    {

        $this->db->where('email', $email);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}