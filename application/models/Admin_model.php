<?php
class Admin_model extends CI_Model
{

    public function _construct()
    {
    }
    public function validat_user()
    {
        // print_r($_POST); // user input values 
        $password = md5($_POST['pswd']);
        $this->db->where('email', $_POST['user_name']);
        $this->db->where('password', $password);
        $result = $this->db->get('users');
        $result1 = $result->result_array();
        // echo'<pre>';
        // print_r($result1['0']['id']);die;
        if ($result->num_rows() > 0) {
            $sess_array = array(

                'user_name' => $result1[0]['first_name'],
                'id'  => $result1[0]['id']
            );
            $this->session->set_userdata($sess_array);
            print_r($sess_array['user_name']);
            // die;
            redirect('home');
        } else {
            $this->session->set_flashdata('login_failed', 'Invalid login. plese try again.');
            // print_r($_SESSION['login_failed']);die;
            redirect('login');
        }
    }
    public function get_campaign_by_id($id)
    {
        

        $this->db->where('id', $id);
        $q = $this->db->get('campaign');
        $data = $q->result();
      
        return $data[0]->title;
    }
    public function get_admin()
    {

        $query = $this->db->get('admin');
        return $query->result();
    }
    public function get_campaign()
    {

        $query = $this->db->get('campaign');
        return $query->result();
    }
      public function get_payment()
    {

        $query = $this->db->get('payment_campaign');
        return $query->result();
    }
    public function get_total_donation_amount()
    {

        $otherdb = $this->load->database('otherdb', TRUE);

        $otherdb->select('amount');
        $query = $otherdb->get('payment');
        return $query->result();
    }
    public function donor_request()
    {
        $otherdb = $this->load->database('otherdb', TRUE);

        $otherdb->select('*');
        $query = $otherdb->get('cacel_or_make_changes');
        return $query->result();
    }
    public function get_doners()
    {

        $otherdb = $this->load->database('otherdb', TRUE);
        $query = $otherdb->get('payment');
        return $query->result();
    }
    public function get_customer_details($id)
    {
        $otherdb = $this->load->database('otherdb', TRUE);

        $otherdb->where('id', $id);
        $q = $otherdb->get('payment');
        return $q->result();
    }
    public function get_program()
    {

        $query = $this->db->get('program');
        return $query->result();
    }
    public function get_blog2_data()
    {

        $query = $this->db->get('blog');
        return $query->result();
    }
    public function delete_blog($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('blog');
    }
    
    
  
    public function delete_campaign($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('campaign');
    }
    public function delete_program($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('program');
    }
    public function delete_activities($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('extra-activities');
    }
    public function delete_contribution($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('contribution');
    }
    public function delete_recent_project($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('recentproject');
    }
    public function update_blog($id)
    {

        $this->db->where('id', $id);
        $this->db->update('blog');
    }
    public function get_edit_blog_data($id)
    {

        $this->db->where('id', $id);
        $q = $this->db->get('blog');
        return $q->result();
    }
    public function get_edit_program($id)
    {

        $this->db->where('id', $id);
        $q = $this->db->get('program');
        return $q->result();
    }
    public function get_edit_recent_project($id)
    {

        $this->db->where('id', $id);
        $q = $this->db->get('recentproject');
        return $q->result();
    }
    public function get_user_email($id)
    {
        $this->db->select('email');
        $this->db->where('id', $id);
        $q = $this->db->get('users');
        return $q->result();
    }




    var $table = "payment";

    var $select_column = array(
        "id", "payment_id", "name", "amount", "email1", "contact_no1", "address", "pan_no", "is_success", "updated_at", "created_at", null, null
    );
    var $order_column = array(
        "id", "payment_id", "name", "amount", "email1", "contact_no1", "address", "pan_no", "is_success", "updated_at", "created_at", null, null
    );
    function make_query()
    {

        // print_r($_POST);
        // die;
        $mail = $_POST['mail'];

        $this->db->select($this->select_column);
        $this->db->from($this->table);
        // if ($mail != '') {
        //     $search_arr[] = " city='" . $mail . "' ";
        // }
        if ($_POST["search"]["value"]) {
            $this->db->or_like("id", $_POST["search"]["value"]);
            $this->db->like("name", $_POST["search"]["value"]);
            $this->db->or_like("email1", $_POST["search"]["value"]);
            $this->db->or_like("contact_no1", $_POST["search"]["value"]);
            $this->db->or_like("payment_id", $_POST["search"]["value"]);
            $this->db->or_like("pan_no", $_POST["search"]["value"]);
            $this->db->or_like("is_success", $_POST["search"]["value"]);
            $this->db->or_like("created_at", $_POST["search"]["value"]);
        } else {

            if ($_POST['mail'] == 0 || $_POST['mail'] == 1) {


                $this->db->or_like("updated_at", $_POST["mail"]);
            }
            if ($_POST['mail'] == 2) {

                $value = $_POST['mail'] - 1;

                $this->db->or_like("is_success", $value);
            }
            if ($_POST['mail'] == 3) {

                $value = $_POST['mail'] - 3;

                $this->db->or_like("is_success", $value);
            }
        }


        if (isset($_POST["order"])) {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by('id', 'DESC');
        }
    }
    function make_datatables()
    {
        $this->make_query();
        if ($_POST["length"] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
    function get_filtered_data()
    {
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function get_all_data()
    {
        $this->db->select("*");
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
