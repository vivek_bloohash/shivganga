<div class="container-fluid">
<section id="">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><br><br>
                    <h4 class="our-purpose">Our Respected Doners List</h4>
                    <br><br>
                </div>
                <div class="card-content bg-f8">
                    <div class="card-body card-dashboard">
                        <div class="table-responsive">
                            <table class="table zero-configuration">
                                <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Name</th>
                                        <th>Amount</th>
                                        <th>Email</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>State</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $sr=1;?>
                                    <?php foreach($doners as $row){?>
                                    <tr>
                                        <td><?php echo $sr?></td>
                                        <td><?php echo $row->first_name;?><?php echo $row->last_name;?></td>
                                        <td><?php echo $row->amount;?></td>
                                        <td><?php echo $row->email;?></td>
                                        <td><?php echo $row->city;?></td>
                                        <td><?php echo $row->state;?></td>
                                        <td><?php echo date('d-m-Y H:i' , strtotime($row->created_at))?></td>
                                    </tr>
                                    <?php $sr++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script src="../../../app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js">
</script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/core/app-menu.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/core/app.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/components.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/datatables/datatable.js"></script>
