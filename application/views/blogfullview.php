<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
// print_r($showblogData);die;
?>

<style>
    #p {
        margin-top: 1em;
        margin-bottom: 1em;
        margin-left: 233;
        margin-right: 0;
        display: block;
    }

    .container2 {
        border: 2px solid #dedede;
        background-color: #f1f1f1;
        border-radius: 5px;
        padding: 10px;
        margin: 10px 0;
    }

    .coment_box {
        border: 2px solid #dedede;
        background-color: #f1f1f1;
        border-radius: 5px;
        height: 147px;

    }

    .darker {
        border-color: #ccc;
        background-color: #ddd;
    }

    .coment_box img {
        height: 61px;
        float: left;
        max-width: 60px;
        width: 100%;
        margin-right: 20px;
        border-radius: 50%;
    }

    .coment_box img.right {
        margin-top: 18px;
        float: left;
        margin-left: -8px;
        margin-right: 0;
    }


    .time-left {
        float: left;
        color: #999;
    }
</style>
<?php foreach ($showblogData as $row) { ?>
    <div class="halma2020-div header-image-blog" style="background-image:linear-gradient(2deg, #040504b3, transparent), url('<?php echo base_url(); ?><?php echo $row->filename ?>');">
        <div class="carousel-caption">
            <h1><?php echo $row->title ?></h1>
            <p class="top-in-text" style="font-size:20px; color:white;padding-top: 19px;">By :
                <?php echo $row->writer_name ?>, <?php echo $row->created_at; ?></p>
            <p class="top-in-text" style="font-size:18px; color:rgb(17 30 75)"></p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8 mt-5">
                <?php echo $row->content ?>
                <div class="col-md-6 my-4">
                    <a href="<?php echo base_url(); ?>blogs" class="btn donate-page btn-donate">Back to Blogs</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="container">
    <!-- <div class="row pt-5">
            <div class="col-md-6 offset-md-3 text-center">
                <h4 class="our-purpose">Latest Post</h4>
            </div>
        </div>
        <div class="row pt-5">
            <div class="col-md-10 offset-md-1">
                <div class="owl-one owl-carousel">

                    <?php foreach ($latest_post as $row) { ?>
                    <div class="item">
                        <a href="<?php echo base_url('home_Controller/blogfullview/') ?><?php echo $row->slug ?>"
                            class="know-more-txt">
                            <img src="<?php echo base_url(); ?><?php echo $row->filename ?>" alt="" />
                            <div class="pt-3 pb-3">
                                <h5 class="affor-txt-heading text-center"><?php echo $row->title ?></h5>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div> -->
</div>
</div>
<script>
    $(document).ready(function() {
        $('.owl-one').owlCarousel({
            margin: 20,
            autoWidth: false,
            nav: true,
            loop: true,
            items: 3,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                }
            },
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ]
        });
    });
</script>