<!DOCTYPE html>
<html>

<head>
    <title>Thank You - shivganga</title>
    <link rel="shortcut icon" href="<?= base_url() ?>assets/files/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/psrstyle.css">

    <style>
    /* td,
    th {
        text-align: center;
        font-family: Tahoma, Verdana, sans-serif;

        vertical-align: middle;
    } */
    .checkmark__circle {
        stroke-dasharray: 166;
        stroke-dashoffset: 166;
        stroke-width: 2;
        stroke-miterlimit: 10;
        stroke: #7ac142;
        fill: none;
        animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards;
    }

    .checkmark {
        width: 56px;
        height: 56px;
        border-radius: 50%;
        display: block;
        stroke-width: 2;
        stroke: #fff;
        stroke-miterlimit: 10;
        margin: 10% auto;
        box-shadow: inset 0px 0px 0px #7ac142;
        animation: fill .4s ease-in-out .4s forwards, scale .3s ease-in-out .9s both;
    }

    .checkmark__check {
        transform-origin: 50% 50%;
        stroke-dasharray: 48;
        stroke-dashoffset: 48;
        animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards;
    }

    @keyframes stroke {
        100% {
            stroke-dashoffset: 0;
        }
    }

    @keyframes scale {

        0%,
        100% {
            transform: none;
        }

        50% {
            transform: scale3d(1.1, 1.1, 1);
        }
    }

    @keyframes fill {
        100% {
            box-shadow: inset 0px 0px 0px 30px #7ac142;
        }
    }

    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        transition: 0.3s;
        width: 100%;
        margin-top: 20px;
        border-radius: 5px;
        background-color: #faeadc;
        text-align: center;

    }

    .card:hover {
        box-shadow: 0 20px 20px 0 rgba(0, 0, 0, 0.2);
    }

    img {
        border-radius: 5px 5px 0 0;
    }

    /* .container {
        padding: 2px 16px;
    } */
    #dd {
        font-family: "Lucida Console", "Courier New", monospace;

    }

    @media screen and (min-width: 480px) {
        /* body {
    background-color: lightgreen;
  } */
    }
    </style>
</head>

<body class="bg-img-gullak bg-faeabc" style="background: #faf5dc">

    <div class="container ">
        <div class="card ">
            <div class="row">
                <div class="col-12 text-center">
                    <img src="https://shivgangajhabua.org/wp-content/uploads/2019/03/sj.png"
                        style="height:100px; width:100px">
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12">
                    <h2 id="dd" class="text-center rale-font3 py-3 mob-font">Payment Details</h2>
                </div>
            </div>

            <div id="dd" class="row rale-font3  mob-font">

                <div class="col-3 offset-3" style="text-align: left;display: inline-grid;">
                    <h4>PyamentId:</h4>
                    <h4>Name:</h4>
                    <h4>Email:</h4>
                    <h4>Amount:</h4>
                    <h4>Time:</h4>
                    <h4>Status:</h4>
                </div>

                <div id="dd" class="col-6  text-left">
                    <?php foreach ($paymet_details as $row) { ?>
                    <h4><?= $row->payment_id; ?></h4>
                    <h4><?= $row->name; ?></h4>
                    <h4><?= $row->email1; ?></h4>
                    <h4><?= $row->amount; ?></h4>
                    <h4><?= date('d-m-Y h:i:s A', strtotime($row->created_at)); ?></h4>

                    <h4> <?php echo "Success"; ?></h4>

                    <?php } ?>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12">
                    <svg class="checkmark  mt-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                    </svg>

                </div>
            </div>
            <div class="row " style="margin:0 auto;">
                <a class="btn btn-success text-center" href="<?php echo base_url() ?>">
                    << back to Shivganga <i class="fa fa-window-restore "></i>
                </a>

            </div>
            <br>
        </div>
    </div>
</body>

</html>