<?php
//   if(isset($_SESSION['user_name'])) {
//     redirect('home1');
  //}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>student</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
    .bg-gray {
        background: #3bb6d6;
        /* box-shadow: 0px 0px 15px 5px #e6e6e6; */
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
    }

    .txt-login {
        font-weight: bold;
        color: #fff;
    }

    .height-100 {
        height: 100vh;
    }

    .btn-sbmit {
        background: #f65571;
        border-radius: 0px;
        border: 0px;
        padding: 6px 30px;
        color: #fff;
    }

    .btn-sbmit:hover,
    .btn-sbmit:active {
        background: #f65571 !important;
        color: #fff;
    }

    .input-box {
        border-radius: 0px;
    }
    </style>
</head>

<body>
    <div class="container">
        <div class="row height-100">
            <div class="col-md-4 offset-md-4 bg-gray align-self-center">
                <h2 class="mt-3 txt-login text-center"><span>Enter Enter New Password</span></h2>
                <!-- <form > -->
                <form method="post" action="<?php echo base_url('admin/updatepassword') ?>" class="validatedForm">
                    <div class="form-group mb-2">
                        <!-- <h4>Check Otp on your Email...</h4> -->
                        <label class="text-white" for="user-name">New Password:</label>
                        <input type="password" class="form-control input-box" id="txtNewPassword"
                            placeholder="Enter Your New Password" name="password" required>
                    </div>
                    <div class="form-group mb-2">
                        <!-- <h4>Check Otp on your Email...</h4> -->
                        <label class="text-white" for="user-name">Confirm Password:</label>
                        <input type="password" class="form-control input-box" id="txtConfirmPassword"
                            placeholder="Re-Enter Your Password" name="otp" required>
                    </div>
                    <div id="CheckPasswordMatch">
                    </div>

                    <!-- <div  id ="showprice">
            <?php
            // if(isset($_SESSION['login_failed'])) {
            //   print_r($_SESSION['login_failed']);
            //}
            ?>
          </div> -->
                    <div class="text-center mb-4">
                        <input type="Submit" class="btn btn-sbmit" value="submit">
                    </div>
                    <!-- <div class="mb-1">
            <label class="checkbox text-white">
              <input type="checkbox" value="remember-me">
              Remember me
            </label>
          </div> -->

                </form>
            </div>
        </div>
    </div>
</body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function checkPasswordMatch() {
    var password = $("#txtNewPassword").val();
    var confirmPassword = $("#txtConfirmPassword").val();
    if (password != confirmPassword)
        $("#CheckPasswordMatch").html("Passwords does not match!");
    else
        $("#CheckPasswordMatch").html("Passwords match.");
}
$(document).ready(function() {
    $("#txtConfirmPassword").keyup(checkPasswordMatch);
});
</script>