<?php
	// if(empty($_SESSION['user_name'])) {
	// 	redirect('login');
	// }
?>
<div class="halma2020-div header-image-taalab">
   <div class="carousel-caption">
      <h1>Saadh Talab</h1>
   </div>
</div>

<div class="bg-f8">
    <div class="container-fluid">
        <div class="row pt-5">
            <div class="col-md-2"></div>
            <div class="col-md-8 text-left saadh-up-div">
                <div>
                    <p class="what-we-do">THE STORY</p>
                    <h4 class="our-purpose">Saadh Talab</h4>
                    <p class="the-primary">There was a pond already existing, time-worn, unable to serve its
                        purpose. The people of Saadh village were adversely struck by the water crisis. They
                        cried before government but the government refused to reconstruct the pond citing risk.
                    </p>
                    <p class="the-primary">
                        Then, some youth from Saadh and nearby villages garnered strength in their feet and
                        stood to reverse the condition. They started taking training under ‘Gram Engineering
                        Varg’ flagship program of Shivganga. Soon, a team of youth was ready and they gave
                        the clarion call for Halma.
                    </p>
                    <p class="the-primary">
                        Halma was organised by Shivganga, in which more than 800 people from nearby villages
                        came together to rescue themselves from the water crisis. In 45 days, merely costing
                        30 lakhs, people of Saadh did, what the government couldn’t do in 5 years.
                    </p>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row pb-5">
            <div class="col-md-12 text-center pt-5 green-txt side-by-btn">
                <span>25% funding requires to achieve the target of Rs.2,00,000</span>
                <a href="<?php echo base_url();?>donate" class="btn donate-page btn-donate">DONATE</a>
            </div>
        </div>
    </div>
</div>
<div class="bg-f8">
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-6 offset-md-3 text-center">
                <h4 class="our-purpose">Other Programs</h4>
            </div>
        </div>
        <div class="row pt-5">
            <div class="col-md-10 offset-md-1">
                <div class="owl-one owl-carousel">
                    <div class="item">
                        <img src="<?php echo base_url(); ?>assets/img/water-cons.png" alt="" />
                        <div class="pt-3 pb-3">
                            <h5 class="affor-txt-heading">Water Conservation</h5>
                            <a href="#" class="know-more-txt">Know More</a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url(); ?>assets/img/youth-emp.png" alt="" />
                        <div class="pt-3 pb-3">
                            <h5 class="affor-txt-heading">Youth Empowerment</h5>
                            <a href="#" class="know-more-txt">Know More</a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url(); ?>assets/img/women-emp.png" alt="" />
                        <div class="pt-3 pb-3">
                            <h5 class="affor-txt-heading">Women Empowerment</h5>
                            <a href="#" class="know-more-txt">Know More</a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url(); ?>assets/img/water-cons.png" alt="" />
                        <div class="pt-3 pb-3">
                            <h5 class="affor-txt-heading">Water Conservation</h5>
                            <a href="#" class="know-more-txt">Know More</a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo base_url(); ?>assets/img/youth-emp.png" alt="" />
                        <div class="pt-3 pb-3">
                            <h5 class="affor-txt-heading">Youth Empowerment</h5>
                            <a href="#" class="know-more-txt">Know More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-34">
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-12 pb-5 text-center">
                <p class="green-txt">In India, the average water footprint is 3287L/day/person</p>
                <p class="green-txt pb-3">Help in restoring the water consumed by you</p>
                <a href="<?php echo base_url();?>donate" style="    height: 60px;width: 194px; margin: 35px; padding: 19px;" class="btn donate-page btn-donate">DONATE</a>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.owl-one').owlCarousel({
        margin: 20,
        autoWidth: false,
        nav: true,
        loop: true,
        items: 3,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            }
        },
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ]
    });
});
</script>