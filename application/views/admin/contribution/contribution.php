<?php
if (empty($_SESSION['user_name'])) {
    redirect('adminLogin');
}
?>
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Create Contibution</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Forms</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Create Contibution</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="feather icon-settings"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a
                                class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">

                <!-- // Basic Horizontal form layout section end -->

                <!-- Basic Vertical form layout section start -->
                <section id="basic-vertical-layouts">

                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Create Contribution Form</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form class="form form-vertical" method="post"
                                        action="<?php echo base_url('admin/storepost') ?>"
                                        enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label for="first-name-icon">Title:</label>
                                                        <div class="position-relative has-icon-left">
                                                            <!-- <input type="text" id="first-name-icon" class="form-control"
                                                                name="title" placeholder="Enter Title"> -->

                                                            <input type="text" name="title" id="first-name-icon"
                                                                class="form-control" placeholder="Enter title"
                                                                class="form-control name_list" required="" />
                                                            <div class="form-control-position">
                                                                <i class="feather icon-file"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <table class="table table-bordered" id="dynamic_field">
                                                    <tr>
                                                        <td><input type="text" name="addmore[][price]"
                                                                placeholder="Enter price" class="form-control name_list"
                                                                required="" /></td>
                                                        <td><button type="button" name="add" id="add"
                                                                class="btn btn-success">Add More</button></td>
                                                    </tr>
                                                </table>

                                                <!-- <table class="table table-bordered" id="dynamic_field">
                                                    <p>Suggested Donation Amonut:-</p>
                                                    <tr>
                                                        <td>Amount:<input type="text" name="addmore[][price]"
                                                                placeholder="Enter price" class="form-control name_list"
                                                                required="" /></td>
                                                        <td>Description:<input type="text" name="addmore[][description]"
                                                                placeholder="Enter description"
                                                                class="form-control name_list" required="" /></td>
                                                        <td><button type="button" name="add" id="add"
                                                                class="btn btn-success">+Add More</button></td>

                                                    </tr>
                                                </table> -->
                                                <!-- <p>Suggested Donation Amonut:-</p> -->
                                                <!-- <table class="table table-bordered">
                                                    <tr>
                                                        <td>Goal:<input type="text" name="goal" placeholder="Enter Goal"
                                                                class="form-control name_list" required="" /></td>
                                                        <td>End Date:<input type="date" name="end_date"
                                                                placeholder="Enter " class="form-control name_list"
                                                                required="" /></td>
                                                    </tr>
                                                </table> -->

                                                <div class="col-12">
                                                    <button type="submit"
                                                        class="btn btn-primary mr-1 mb-1">Submit</button>
                                                    <button type="reset"
                                                        class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        </section>

    </div>
</div>
</div>
<!-- BEGIN: Vendor JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/core/app-menu.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/core/app.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/components.js"></script>














<!DOCTYPE html>

<html>

<head>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> -->
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
</head>


<script type="text/javascript">
$(document).ready(function() {
    var i = 1;
    $('#add').click(function() {
        i++;
        $('#dynamic_field').append('<tr id="row' + i +
            '" class="dynamic-added"><td><input type="text" name="addmore[][price]" placeholder="Enter price" class="form-control name_list" required /></td><td><button type="button" name="remove" id="' +
            i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
    });
    $(document).on('click', '.btn_remove', function() {
        var button_id = $(this).attr("id");
        $('#row' + button_id + '').remove();
    });
});
</script>
</body>

</html>