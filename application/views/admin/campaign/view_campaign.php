<!-- END: Main Menu-->
<?php
if (empty($_SESSION['user_name'])) {
    redirect('adminLogin');
}
?>

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Campaign</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">View Campaign
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Tables start -->
            <div class="row" id="basic-table">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">View Campaign</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">

                                <!-- Table with outer spacing -->
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Image</th>
                                                <th>Title</th>
                                                <th>Price</th>
                                                <th>Content</th>
                                                <th>Target</th>
                                                <th>Donation Type</th>
                                                <th>Piece Title</th>
                                                <th>Piece Quantity</th>


                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($view_campaign as $row) { ?>
                                                <tr>
                                                    <td><?php echo $sr ?></td>
                                                    <td><img src="<?php echo base_url() . $row->image ?>" style="width: 50px;height:50px"></td>
                                                    <td><?php echo $row->title ?></td>
                                                    <td><?php echo 'Rs. ' . $row->price; ?></td>
                                                    <td> <button type="button" class="btn  btn_content" id="<?= $row->id ?>" style="background-color:#52bd4b;color:white" data-toggle="modal" data-target="#myModal">Open Content</button>
                                                    </td>
                                                    <!-- <td><?php echo $row->content; ?></td> -->
                                                    <td><?php echo 'Rs. ' . $row->target; ?></td>
                                                    <td><?php echo $row->donation_type; ?></td>
                                                    <?php
                                                    if ($row->piece_title == "") { ?>
                                                        <td><?= '----' ?></td>


                                                    <?php  } else { ?>
                                                        <td><?= $row->piece_title; ?></td>


                                                    <?php    } ?>
                                                    <?php
                                                    if ($row->item_quantity == "") { ?>
                                                        <td><?= '----' ?></td>


                                                    <?php  } else { ?>
                                                        <td><?= $row->item_quantity; ?></td>


                                                    <?php    } ?>

                                                    <td>
                                                        <a href="<?php echo base_url('admin/edit_campaign/') ?><?php echo $row->id ?>">Edit</a><br><br>
                                                        <a href="<?php echo base_url('admin/delete_campaign/') ?><?php echo  $row->id ?>">Delete</a>
                                                    </td>
                                                </tr>

                                                <!-- Modal -->

                                            <?php $sr++;
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                            <h4 class="modal-title text-left">Content</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="show_content">

                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Responsive tables end -->
                        </div>
                    </div>
                </div>
                <!-- END: Content-->

                <div class="sidenav-overlay"></div>
                <div class="drag-target"></div>

                <!-- BEGIN: Footer-->

                <!-- END: Footer-->


                <!-- BEGIN: Vendor JS-->
                <script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/vendors.min.js"></script>
                <!-- BEGIN Vendor JS-->

                <!-- BEGIN: Page Vendor JS-->
                <!-- END: Page Vendor JS-->

                <!-- BEGIN: Theme JS-->
                <script src="<?php echo base_url(); ?>assets/app-assets/js/core/app-menu.js"></script>
                <script src="<?php echo base_url(); ?>assets/app-assets/js/core/app.js"></script>
                <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/components.js"></script>
                <!-- END: Theme JS-->

                <!-- BEGIN: Page JS-->
                <!-- END: Page JS-->
                <script>
                    $(document).on('click', '.btn_content', function() {
                        var id = $(this).attr("id");
                        $.ajax({
                            url: '<?= base_url() ?>admin/get_content',
                            type: 'POST',
                            data: {
                                id: id
                            },
                            success: function(response) {


                                document.getElementById("show_content").innerHTML = response;
                            }
                        })
                    });
                </script>
                </body>
                <!-- END: Body-->

                </html>