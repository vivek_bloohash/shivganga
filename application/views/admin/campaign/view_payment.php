<!-- END: Main Menu-->
<?php
if (empty($_SESSION['user_name'])) {
    redirect('adminLogin');
}
?>

    
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Pyament</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">View Payment
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Tables start -->
            <div class="row" id="basic-table">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">View Payment

                                &nbsp;<a class="pull-right btn btn-warning btn-large" style="margin-right:40px" href="<?php echo site_url(); ?>admin/createexcel"><i class="fa fa-file-excel-o"></i> Export Excel</a>

                                <!--<button class="btn btn-success ml-5">Export</button>-->


                            </h4>
                        </div>
                        <?php

                        $servername = "localhost";
                        $username = "u251981260_new_shivganga";
                        $password = "New_shivganga@123";
                        $dbname = "u251981260_new_shivganga";

                        // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }





                        ?>
                        <div class="card-content">
                            <div class="card-body">

                                <!-- Table with outer spacing -->
                                <div class="table-responsive">
                                    <table class="table" id="myTable">
                                        <thead>
                                            <tr>
                                                <th width="10%">Receipt Id</th>
                                                <th width="35%">Campaign Name</th>

                                                <th width="35%">Payment Id</th>
                                                <th width="35%">Payment Method</th>

                                                <th width="35%">Name</th>

                                                <th width="10%">Amount</th>
                                                <th width="10%">Email</th>
                                                <th width="10%">Contact</th>
                                                <th width="10%">Address</th>
                                                <th width="10%">Pan No</th>
                                                <th width="10%">Payment Status</th>
                                                <!--<th width="10%">Mail Status</th>-->
                                                <th width="10%">Date</th>
                                                <th width="10%">Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($view_payment as $row) { ?>
                                                <tr>
                                                    <td><?php echo $row->id ?></td>


                                                    <td>
                                                        <?php
                                                        $sql = "SELECT title FROM campaign WHERE id=" . $row->campaign_id;
                                                        $result = $conn->query($sql);
                                                        $title = $result->fetch_assoc();

                                                        echo $title['title'];
                                                        ?>
                                                    </td>

                                                    <td><?php

                                                        if ($row->payment_id == "") {
                                                            echo $row->order_id;
                                                        } else {

                                                            echo $row->payment_id;
                                                        }


                                                        ?></td>

                                                    <td><?php echo $row->payment_type ?></td>

                                                    <td><?php echo $row->name ?></td>

                                                    <td><?php echo $row->amount ?></td>

                                                    <td><?php echo $row->email1 ?></td>

                                                    <td><?php echo $row->contact_no1 ?></td>

                                                    <td><?php echo $row->address ?></td>

                                                    <td><?php echo $row->pan_no ?></td>
                                                    <td>
                                                        <?php
                                                        if ($row->is_success == 1) {
                                                            echo "Success";
                                                        } else {
                                                            echo "Failed";
                                                        }

                                                        ?>


                                                    </td>

                                                    <!--<td><?php echo $row->id ?></td>-->

                                                    <td><?php echo $row->created_at ?></td>

                                                    <td><a href="https://shivgangajhabua.org/new_shivganga/campaign/Payment/download_by_id/<?php echo $row->id ?>">Print Receipt</a></td>

                                                </tr>

                                                <!-- Modal -->

                                            <?php
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                                            <h4 class="modal-title text-left">Content</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="show_content">

                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Responsive tables end -->
                        </div>
                    </div>
                </div>
                <!-- END: Content-->

                <div class="sidenav-overlay"></div>
                <div class="drag-target"></div>

                <!-- BEGIN: Footer-->

                <!-- END: Footer-->
// <script type="text/javascript">
// $(document).ready(function() {
//     $('#myTable').DataTable();
// });
// </script>

                <!-- BEGIN: Vendor JS-->
                <script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/vendors.min.js"></script>
                <!-- BEGIN Vendor JS-->

                <!-- BEGIN: Page Vendor JS-->
                <!-- END: Page Vendor JS-->

                <!-- BEGIN: Theme JS-->
                <script src="locales.js"></script>
                <script src="<?php echo base_url(); ?>assets/app-assets/js/core/app.js"></script>
                <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/components.js"></script>
                <!-- END: Theme JS-->

                <!-- BEGIN: Page JS-->
                <!-- END: Page JS-->
            
           
                </body>
                <!-- END: Body-->

                </html>