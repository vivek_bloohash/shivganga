<?php
if (empty($_SESSION['user_name'])) {
    redirect('adminLogin');
}
?>

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Edit Campaign</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=base_url()?>admin">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="#">Edit Campaign</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="feather icon-settings"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Horizontal form layout section start -->
            <section id="basic-horizontal-layouts">

                <!-- // Basic Horizontal form layout section end -->

                <!-- Basic Vertical form layout section start -->
                <section id="basic-vertical-layouts">

                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Donation Type</h4>

                            </div>
                            <div class="card-content">
                                <div class="card-body">



                                    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
                                    <script>
                                        $(document).ready(function() {
                                            $('input[type="radio"]').click(function() {
                                                var inputValue = $(this).attr("value");
                                                var targetBox = $("." + inputValue);
                                                $(".box").not(targetBox).hide();
                                                $(targetBox).show();
                                            });
                                        });
                                    </script>
                                    </head>

                                    <body>
                                        <div>
                                            <label><input type="radio" name="colorRadio" value="red" checked> Price</label>
                                        </div>
                                        <div class="red box">
                                            <form class="form form-vertical" method="post" action="<?php echo base_url('admin/update_campaign') ?>" enctype="multipart/form-data">
                                                <div class="form-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label for="first-name-icon">Title:</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="text" id="first-name-icon" class="form-control" value="<?= $edit_campaign[0]->title ?>" name="title" placeholder="Enter Title">
                                                                    <div class="form-control-position">
                                                                        <i class="feather icon-file"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label for="first-name-icon">Custom Slug:</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="text" id="first-name-icon" class="form-control" value="<?= $edit_campaign[0]->custom_Slug ?>" name="custom_Slug" placeholder="Enter Custom Slug" required>
                                                                    <div class="form-control-position">
                                                                        <i class="feather icon-file"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label for="contact-info-icon">Image:</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="file" onchange="previewFile(this);" id="contact-info-icon" class="form-control" name="filename" placeholder="Image">
                                                                    <div class="form-control-position">
                                                                        <i class="feather icon-file"></i>
                                                                    </div>

                                                                </div>
                                                                <?php if ($edit_campaign[0]->image == "") { ?>
                                                                    <img src="<?= base_url() ?>assets/product.png" class="mt-1" style="height: 100px;width: 229px; border-radius: 10px;">

                                                                <?php } else { ?>
                                                                    <img src="<?php echo base_url() . $edit_campaign[0]->image ?>" class="mt-1" id="previewImg" style="height: 100px;width: 229px; border-radius: 10px;">
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <table class="table table-bordered" id="dynamic_field">
                                                            <p>Suggested Donation Amonut:-</p>
                                                            <tr>
                                                                <input type="hidden" name="donation_type" value="price">
                                                                <td>Amount:<input type="number" name="price" value="<?= $edit_campaign[0]->price ?>" placeholder="Enter price" class="form-control name_list" required="" /></td>
                                                                <!-- <td>Description:<input type="text" name="addmore[][description]" placeholder="Enter description" class="form-control name_list" required="" /></td> -->
                                                                <!-- <td><button type="button" name="add" id="add" class="btn btn-success">+Add More</button></td> -->

                                                            </tr>
                                                        </table>
                                                        <table class="table table-bordered">
                                                            <!-- <p>Suggested Donation Amonut:-</p> -->

                                                            <tr>
                                                                <td>Target:<input type="text" name="target" value="<?= $edit_campaign[0]->target ?>" placeholder="Enter Target Amount" class="form-control name_list" required="" /></td>
                                                                <td>End Date:<input type="date" value="<?= $edit_campaign[0]->end_date ?>" name="end_date" placeholder="Enter " class="form-control name_list" required="" /></td>
                                                            </tr>
                                                        </table>
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label for="email-id-icon">Content:</label>
                                                                <input name="content" id="inp_htmlcode" type="hidden" />


                                                                <?php $main = $edit_campaign[0]->content ?>
                                                                <div id="div_editor1" class="richtexteditor" style="width: 100%;margin:0 auto;">
                                                                    <?php echo $main;  ?>

                                                                </div>

                                                                <script>
                                                                    var editor1 = new RichTextEditor(document.getElementById("div_editor1"));
                                                                    editor1.attachEvent("change", function() {
                                                                        document.getElementById("inp_htmlcode").value = editor1.getHTMLCode();
                                                                    });
                                                                </script>
                                                            </div>
                                                        </div>
                                                        <div class="col-12">
                                                            <button type="submit" class="btn btn-primary mr-1 mb-1">Submit</button>
                                                            <button type="reset" class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>





                                </div>
                            </div>
                        </div>
                    </div>
        </div>
        </section>

    </div>
</div>
</div>
<!-- END: Content-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<!-- BEGIN: Footer-->

<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/vendors.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/core/app-menu.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/core/app.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/components.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<!-- END: Page JS-->
<script type="text/javascript">
    $(document).ready(function() {
        var i = 1;
        $('#add').click(function() {
            i++;
            $('#dynamic_field').append('<tr id="row' + i +
                // '" class="dynamic-added"><td><input type="text" name="addmore[][price]" placeholder="Enter price" class="form-control name_list" required /></td><td><input type="text" name="addmore[][description]" placeholder="Enter description" class="form-control name_list" required /></td><td><button type="button" name="remove" id="' +
                i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    });
    $(document).ready(function() {
        var i = 1;
        $('#add2').click(function() {
            i++;
            $('#dynamic_field2').append('<tr id="row' + i +
                '" class="dynamic-added"><td>Item Quantity :<input type="text" name="addmore[]" placeholder="Enter Item Quantity" class="form-control name_list" required="" /></td><td><button type="button" name="remove" id="' +
                i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    });


    function previewFile(input) {
        var file = $("input[type=file]").get(0).files[0];

        if (file) {
            var reader = new FileReader();

            reader.onload = function() {
                $("#previewImg").attr("src", reader.result);
            }

            reader.readAsDataURL(file);
        }
    }
</script>
</body>
<!-- END: Body-->

</html>