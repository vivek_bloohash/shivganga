<?php
//   if(isset($_SESSION['user_name'])) {
//     redirect('home1');
  //}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>student</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <h2>Create About Us</h2>
        <form method="post" action="<?php echo base_url('admin/saveaboutus') ?>" enctype="multipart/form-data">
            <div class="form-group">
                <label for="email">Title:</label>
                <input type="text" class="form-control" id="title" placeholder="Enter title" name="title">
            </div>
            <div class="form-group">
                <label for="email">Sub Title:</label>
                <input type="text" class="form-control" id="sub_title" placeholder="Enter sub title" name="sub_title">
            </div>
            <div class="form-group">
                <label for="email">Content:</label>
                <textarea type="text" class="form-control" id="content" placeholder="Enter content"
                    name="content"></textarea>
            </div>
            <div class="form-group">
                <label for="email">Main About us Image:</label>
                <input type="file" class="form-control" id="image" name="filename">
            </div>
            <div class="form-group">
                <label for="email">Our Story Title:</label>
                <input type="text" class="form-control" id="our_story_title" placeholder="Enter our story title"
                    name="our_story_title">
            </div>
            <div class="form-group">
                <label for="email">Our Story Content:</label>
                <textarea type="text" class="form-control" id="our_story_content" placeholder="Enter our story content"
                    name="our_story_content"></textarea>
            </div>
            <div class="form-group">
                <label for="email">Our Story Image:</label>
                <input type="file" class="form-control" id="image" name="our_story_filename">
            </div>
            <button type="submit" class="btn btn-default" style="color:white; background-color:skyblue">Submit</button>
        </form>
    </div><br>
</body>

</html>