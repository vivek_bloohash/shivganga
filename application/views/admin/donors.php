<?php
if (empty($_SESSION['user_name'])) {
    redirect('admin/adminLogin');
}
?>
<html>

<head>
    <title>Shivgnaga-Donations</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/app-assets/images/portrait/small/shiv_ganga-logo.png">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #f1f1f1;
        }

        .box {
            width: 900px;
            padding: 20px;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <div class="container-fluid" style="margin-top:15px;">
        <div class="row">
            <div class="col-md-4">
                <h3>Total No of Donations <h1 style="color:green"><?php echo count($total_donation_amount); ?></h1>
                    <label>Mail:</label>
                    <select id="mail" class="form-control" name="mail" style="width:28%">
                        <option value="">-----All-----</option>
                        <option value="1">Delivered</option>
                        <option value="0">Undelivered</option>
                        <option value="2">Success</option>
                        <option value="3">Failed</option>
                    </select>
            </div>
            <div class="col-md-4">
                <div class="text-center">
                    <img src="<?php echo base_url(); ?>assets/app-assets/images/portrait/small/shiv_ganga-logo.png" style="border-radius: 50px;width: 100px;
    height: 100px;">
                </div>

                <h1 align="center" style="color:#52bd4b" class="mt-5"> <b><?php echo 'Shivganga Donations' ?></b></h1>
                <br />
            </div>

            <div class="col-md-4">
                <div class="text-right">
                    <a style="padding-left:84%" href="<?= base_url() ?>admin/logout"><button class="btn btn-danger">Logout</button></a>
                    <?php
                    $total_amount = 0;
                    $num= 0;
                    foreach ($total_donation_amount as $row) {
                        $num = $num + $row->amount;
                    }
                    function thousandsCurrencyFormat($num)
                    {
                        if ($num > 1000) {

                            $x = round($num);
                            $x_number_format = number_format($x);
                            $x_array = explode(',', $x_number_format);
                            $x_parts = array('k', 'm', 'b', 't');
                            $x_count_parts = count($x_array) - 1;
                            $x_display = $x;
                            $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
                            $x_display .= $x_parts[$x_count_parts - 1];

                            return $x_display;
                        }

                        return $num;
                    }
                    ?>
                    <h3>Total Donation Amount <h1 style="color:green">Rs. <?php echo thousandsCurrencyFormat($num) ?></h1>
                        <a href="<?= base_url() ?>admin">
                            <p style="color:blue">Dashboard<p>
                                <a href="https://psr.shivgangajhabua.org/credit-card/Subscription/receipt_form"><p style="color:blue">Create New Receipt<p></a>
                        </a>
                    </h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="table-responsive ">
            <br />
            <table id="user_data" class="table table-bordered table-striped mb-5">
                <thead>
                    <tr>
                        <th width="10%">Receipt Id</th>
                        <th width="35%">Payment Id</th>
                        <th width="35%">Name</th>
                        <th width="10%">Amount</th>
                        <th width="10%">Email</th>
                        <th width="10%">Contact</th>
                        <th width="10%">Address</th>
                        <th width="10%">Pan No</th>
                        <th width="10%">Payment Status</th>
                        <th width="10%">Mail Status</th>
                        <th width="10%">Date</th>
                        <th width="10%">Pan & Address</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</body>

</html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<body>

    <div class="w3-container">
        <div id="id01" class="w3-modal">
            <div class="w3-modal-content" style="width: 740px;">
                <div class="w3-container">
                    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-center">&times;</span>

                    <span class=" pb-5" id="Show_table"></span><br>

                </div>
            </div>
        </div>
    </div>
</body>

</html>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        var dataTable = $('#user_data').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                url: "<?php echo base_url() . 'Admin/fetch_user'; ?>",
                type: "POST",
                data: function(data) {

                    data.mail = $('#mail').val();

                }
            },
            "columnDefs": [{
                "targets": [0],
                "orderable": false,
            }, ],
        });
        $('#mail').change(function() {
            dataTable.draw();
        });
    });
</script>
<script>
    function send(d) {
        document.getElementById('id01').style.display = 'block';
        var str = $(d).parent().find('.hash').val();
        // alert(str)

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("Show_table").innerHTML = this.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url() ?>admin/view_details?id=" + str, true);
        xmlhttp.send();
    }
</script>