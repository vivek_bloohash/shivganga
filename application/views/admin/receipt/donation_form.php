<?php
if (empty($_SESSION['user_name'])) {
    redirect('admin/adminLogin');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>PSR - Akshay Dan - Recurring Donation - Shivganga Jhabua</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/files/favicon.png" type="image/x-icon">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->
    <style>
    body {

        background-color: #faeadc;
    }

    .mons-font {
        font-family: 'Montserrat', sans-serif;
        font-weight: 500;
    }

    .mons-font6 {
        font-family: 'Montserrat', sans-serif;
        font-weight: 600;
    }

    .rale-font {
        font-family: 'Raleway', sans-serif;
    }

    .rale-font3 {
        font-family: 'Raleway', sans-serif;
        font-weight: 300;
    }

    .rale-font4 {
        font-family: 'Raleway', sans-serif;
        font-weight: 400;
    }

    .rale-font5 {
        font-family: 'Raleway', sans-serif;
        font-weight: 500;
    }

    .dropdown:hover>.dropdown-menu {
        display: block;
    }

    .dropdown>.dropdown-toggle:active {
        pointer-events: none;
    }

    .navbar-nav>li {
        padding: 0px 14px;
    }

    .navbar-nav>li>a {
        font-size: 15px;
        letter-spacing: 1px;
        color: #292929 !important;
    }

    .navbar-nav>li>a:hover {
        color: #81d742 !important;
    }

    .cus-dd-menu {
        padding: 16px 0;
        background-color: #2d2e32;
    }

    .fix-menu-width {
        width: 258px;
    }

    .cus-dd-menu>a {
        padding: 3px 25px;
        color: #999;
        font-size: 13px;
        font-family: "Montserrat", sans-serif;
        text-transform: uppercase;
    }

    .cus-dd-menu>a:focus,
    .cus-dd-menu>a:hover {
        color: #fff;
        text-decoration: underline;
        background-color: transparent;
    }

    .bg-faeabc {
        background: #faeadc;
    }

    .akshay-gram-hindi {
        font-weight: 600;
        color: #d60047;
        font-size: 40px;
    }

    .akshay-dan-hindi {
        font-weight: 600;
        color: #2b2b2b;
        font-size: 60px;
    }

    .samajik-hindi {
        font-size: 24px;
    }

    .green-brdr {
        border-bottom: 1px solid #81d742;
    }

    .font-size14 {
        font-size: 14px;
    }

    .bg-ededed {
        background: #ededed;
    }

    .min-height100 {
        min-height: 100px;
    }

    .bg-img-peoples {
        background-image: url(./files/MOD-TALAB.jpg);
        background-position: 20%;
    }

    .bg-stu {
        background-size: cover;
        background-position: center center;
        box-shadow: inset 0 0 0 50vw rgb(148 143 80 / 30%);
        color: white;
        display: flex;
        align-items: center;
        justify-content: center;
        background-image: url(./files/mountain.jpg);
        background-repeat: no-repeat;
    }

    .btn-become {
        padding: 10px 30px;
        border-radius: 0px;
        color: #fff;
        background: #81d742;
        border-color: #81d742;
    }

    .btn-become:hover {
        color: #fff;
        background: #72bb3c;
        border-color: #72bb3c;
    }

    .btn-magenta {
        background: #d60047;
        border: 1px solid #d60047;
    }

    .btn-magenta:hover {
        color: #fff;
        background: #bb0033;
        border-color: #bb0033;
    }

    .btn-gold {
        background: #ffd700;
        border: 1px solid #ffd700;
    }

    .btn-gold:hover {
        color: #fff;
        background: #f5ce00;
        border-color: #f5ce00;
    }

    .btn-set-blue {
        background: #06A4A3;
        border: 1px solid #06A4A3;
    }

    .btn-set-blue:hover {
        color: #fff;
        background: #038281;
        border-color: #038281;
    }

    .font-aws-fb {
        color: #3b5998;
    }

    .font-aws-fb:hover {
        color: #3b5998;
    }

    .font-aws-in {
        color: #c13584;
    }

    .font-aws-in:hover {
        color: #c13584;
    }

    .font-aws-tw {
        color: #028ef9;
    }

    .font-aws-tw:hover {
        color: #028ef9;
    }

    .font-aws-yo {
        color: #ff3233;
    }

    .font-aws-yo:hover {
        color: #ff3233;
    }

    .font-aws-wh {
        color: #6fd341;
    }

    .font-aws-wh:hover {
        color: #6fd341;
    }

    .flink {
        font-size: 13px;
        color: rgba(255, 255, 255, .5);
        padding: 10px 20px;
    }

    .flink:hover {
        color: rgba(255, 255, 255, .8);
    }

    .pr-table-value {
        width: 60%;
        margin: 0 auto;
        background: #81d742;
        padding: 6px 0px;
    }

    .bg-magenta {
        background: #d60047;
    }

    .bg-set-blue {
        background: #06A4A3;
    }

    .bg-gold {
        background: #ffd700;
    }

    .min-height70 {
        min-height: 70px;
    }

    .no-brdr-top {
        border: 2px solid #efefef;
        border-top: 0;
    }

    .per-term {
        font-size: 13px;
        color: #fff;
        margin-bottom: 0px;
    }

    .per-figure {
        color: #fff;
        font-size: 42px;
        line-height: 1;
    }

    .tbl-pay tr td:nth-child(1) {
        text-align: right
    }

    .tbl-pay tr td {
        border: none;
    }

    .btn-cntr-gullak {
        position: absolute;
        transform: translate(-50%, 50%);
        left: 50%;
        top: 40%;
    }

    .bg-img-gullak {
        background-image: url(./files/gullak-1.png);
        height: 100vh;
        background-position: right;
        background-size: 32.2% 100%;
        background-repeat: no-repeat;
    }

    .mobile-show {
        display: none;
    }

    .mobile-hide {
        display: flex;
    }

    .mobile-hide-block {
        display: block;
    }

    .mobile-show-block {
        display: none;
    }

    .carousel-control-prev-icon {
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%2381d742' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
    }

    .carousel-control-next-icon {
        background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%2381d742' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
    }

    /* faq */
    .acc-plus {
        position: absolute;
        right: 10px;
        top: 24px;
    }

    .no-focus {
        box-shadow: none !important;
    }

    @media (max-width: 520px) {
        .mob-height {
            min-height: 70px;
        }
    }

    @media (max-width: 767px) {
        .mob-font {
            font-size: 1.5rem;
        }

        .mobile-show {
            display: flex;
        }

        .mobile-hide,
        .mobile-hide-block {
            display: none;
        }

        .mobile-cntr {
            justify-content: center;
        }

        .bg-stu {
            margin-bottom: 30px;
        }

        .mobile-inspire-student {
            padding: 30px 0px;
        }

        .bg-img-gullak {
            background-size: 100% 100%;
        }

        .step-2-0f-2 {
            color: #fff;
            background: #81d742;
            padding: 10px;
        }

        .mobile-show-block {
            display: block;
        }

        .akshay-dan-hindi {
            font-size: 40px;
        }

        .akshay-gram-hindi {
            font-size: 32px;
        }

        .samajik-hindi {
            font-size: 18px;
        }

        .social-change-div-height {
            display: none;
        }
    }

    .charitable-metabox-wrap label {
        display: inline-block;
        width: 50%;
        padding: 8px 5px;
    }

    .charitable-fieldset-wrap {
        max-width: 400px;
        margin: 0 auto;
    }

    h2.postbox-title,
    h3.charitable-metabox-header {
        text-align: center;
    }

    .save-form {
        width: 100px;
        margin: 0 auto;
        padding: 5px;
        margin-top: 10px;
        background-color: chartreuse;
    }
    </style>
</head>

<body>
    <!-- <nav class="navbar navbar-expand-lg pt-0 pb-0 bg-white navbar-light sticky-top">
        <div class="container">
            <a class="navbar-brand pt-0 pb-0" href="https://shivgangajhabua.org/">
                <img src="https://shivgangajhabua.org/emandate/files/sj.png" alt="Logo" style="width:70px;height: 70px;">
            </a>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="mons-font collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="https://shivgangajhabua.org/">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            About
                        </a>
                        <div class="dropdown-menu fix-menu-width cus-dd-menu mt-0">
                            <a class="dropdown-item" href="https://shivgangajhabua.org/aboutus/">About Us</a>
                            <a class="dropdown-item" href="https://shivgangajhabua.org/achievements/">Recognition &
                                Media</a>
                            <a class="dropdown-item" href="https://shivgangajhabua.org/philosophies/">Our
                                Philosophies</a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            Programs
                        </a>
                        <div class="dropdown-menu fix-menu-width cus-dd-menu mt-0">
                            <a class="dropdown-item-text"
                                href="https://shivgangajhabua.org/afforestation-3/">AFFORESTATION</a>
                            <a class="dropdown-item-text" href="https://shivgangajhabua.org/water-conservation/">WATER
                                CONSERVATION</a>
                            <a class="dropdown-item-text"
                                href="https://shivgangajhabua.org/youth-empowerment-and-social-leadership-generation/">YOUTH
                                EMPOWERMENT AND SOCIAL LEADERSHIP GENERATION</a>
                            <a class="dropdown-item-text"
                                href="https://shivgangajhabua.org/alternative-education-skill-development/">ALTERNATIVE
                                EDUCATION & SKILL DEVELOPMENT</a>
                            <a class="dropdown-item-text"
                                href="https://shivgangajhabua.org/livelihood-entrepreneurship/">LIVELIHOOD &
                                ENTREPRENEURSHIP</a>
                            <a class="dropdown-item" href="https://shivgangajhabua.org/women-empowerment/">WOMEN
                                EMPOWERMENT</a>
                            <a class="dropdown-item-text"
                                href="https://shivgangajhabua.org/connecting-communities/">CONNECTING COMMUNITIES</a>
                            <a class="dropdown-item-text" href="https://shivgangajhabua.org/tribal-culture/">TRIBAL
                                CULTURE</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://shivgangajhabua.org/contact-us/">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://psr.shivgangajhabua.org/">Psr</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://shivgangajhabua.org/campaigns/contribute/donate/">Donate</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://test-emandate.shivgangajhabua.org/emandate/doners.php">Respected Doners</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            Forums
                        </a>
                        <div class="dropdown-menu cus-dd-menu mt-0">
                            <a class="dropdown-item" href="https://shivgangajhabua.org/forums/">Forums</a>
                            <a class="dropdown-item" href="https://shivgangajhabua.org/topics/">Topics</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav> -->

    <div id="donation-form" class="postbox container">
        <div class="postbox-header">
            <h2 class="postbox-title">Donation Form</h2>
        </div>
        <div class="inside">
            <div class="charitable-form-fields primary">
                <form method="POST" action="<?php echo base_url() ?>Admin/save_donation_data">
                    <fieldset id="charitable-donation-fields-wrap"
                        class="charitable-metabox-wrap charitable-fieldset-wrap">
                        <div id="charitable-campaign-donations-metabox-wrap"
                            class="charitable-metabox-wrap charitable-campaign-donations-wrap">
                            <table id="charitable-campaign-donations" class="widefat">
                                <thead>
                                    <tr class="table-header">
                                        <th><label id="campaign-donations-campaign-label">Campaign</label></th>
                                        <th><label id="campaign-donations-amount-label">Amount</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <select name="campaign_donations[0][campaign_id]"
                                                labelledby="campaign-donations-campaign-label" tabindex="2" required>
                                                <option value="">Select a campaign</option>
                                                <option value="22093">Shivganga Akshay Daan</option>
                                                <option value="22003">Navbhagirath</option>
                                                <option value="21841">Jhabua Crafts Website Development</option>
                                                <option value="21253">My Plants in Jhabua</option>
                                                <option value="21153">Water Campaign</option>
                                                <option value="19907">SHIVGANGA DONATION</option>
                                                <option value="18953">Halma</option>
                                                <option value="17300">Diwali Par Paudha</option>
                                                <option value="14997">Plant is bae!</option>
                                                <option value="14831">Contribute</option>
                                                <option value="14550">Our Share of Water</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="currency-input"
                                                name="campaign_donations[0][amount]"
                                                labelledby="campaign-donations-amount-label" tabindex="2" value=""
                                                required>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>

                    <h3 class="charitable-metabox-header">Donor</h3>

                    <fieldset id="charitable-user-fields-wrap" class="charitable-metabox-wrap charitable-fieldset-wrap">

                        <div id="charitable-first-name-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="first-name">
                                First Name </label>
                            <input type="text" id="first-name" name="first_name" value="" tabindex="102" required>
                        </div>
                        <!-- #charitable-first-name-wrap -->
                        <div id="charitable-last-name-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="last-name" required>
                                Last Name </label>
                            <input type="text" id="last-name" name="last_name" value="" tabindex="103" required>
                        </div>
                        <!-- #charitable-last-name-wrap -->
                        <div id="charitable-email-wrap" class="charitable-metabox-wrap charitable-email-wrap">
                            <label for="email">
                                E-mail </label>
                            <input type="email" id="email" name="email" value="" tabindex="104" required>
                        </div>
                        <!-- #charitable-email-wrap -->
                        <div id="charitable-address-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="address">
                                Address </label>
                            <input type="text" id="address" name="address" value="" tabindex="105" required>
                        </div>
                        <!-- #charitable-address-wrap -->
                        <div id="charitable-address-2-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="address-2">
                                Address 2 </label>
                            <input type="text" id="address-2" name="address_2" value="" tabindex="106" required>
                        </div>
                        <!-- #charitable-address-2-wrap -->
                        <div id="charitable-city-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="city">
                                City </label>
                            <input type="text" id="city" name="city" value="" tabindex="107" required>
                        </div>
                        <!-- #charitable-city-wrap -->
                        <div id="charitable-state-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="state">
                                State </label>
                            <input type="text" id="state" name="state" value="" tabindex="108" required>
                        </div>
                        <!-- #charitable-state-wrap -->
                        <div id="charitable-postcode-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="postcode">
                                Postcode </label>
                            <input type="text" id="postcode" name="postcode" value="" tabindex="109" required>
                        </div>
                        <!-- #charitable-postcode-wrap -->
                        <div id="charitable-country-wrap" class="charitable-metabox-wrap charitable-select-wrap">
                            <label for="country">
                                Country </label>
                            <select id="country" name="country" tabindex="110" required>
                                <option value="IN" selected="selected">India</option>
                                <option value="OTHER">Other</option>
                            </select>
                        </div>
                        <!-- #charitable-country-wrap -->
                        <div id="charitable-phone-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="phone">
                                Phone Number </label>
                            <input type="text" id="phone" name="phone" value="" tabindex="111" required>
                        </div>
                        <!-- #charitable-phone-wrap -->
                        <div id="charitable-pan-number-wrap" class="charitable-metabox-wrap charitable-text-wrap">
                            <label for="pan-number">
                                Pan Number(Compulsory above ₹2000)</label>
                            <input type="text" id="pan-number" name="pan_number" value="" tabindex="112" required>
                        </div>
                        <div style="width: 100%; text-align: center;">
                            <input type="submit" value="Save" class="save-form">
                            <a href="<?php echo base_url('Admin/adminpanel') ?>"><input type="button" value="Back"
                                    class="save-form"></a>

                        </div>
                        <div style="    margin: 20px 10px 0px 119px;" class="">

                            <?php

                            if ($this->session->flashdata('item')) {
                                $message = $this->session->flashdata('item');
                            ?>
                            <div class="<?php echo $message['class'] ?> "><?php echo $message['message']; ?>

                            </div>
                            <?php
                            }

                            ?>
                        </div>
                    </fieldset>

                </form>


            </div>
        </div>
    </div>


    <!-- <footer id="sticky-footer" class="py-4 bg-dark text-white-50" style="margin-top:40vh">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-4">
            <p class="mb-0 pt-3">Shivganga-Vikas ka Jatan</p>
          </div>
          <div class="col-lg-6 col-md-8 text-right">
            <a href="https://shivgangajhabua.org/blog/" class="mons-font6 flink">BLOG</a>
            <a href="https://shivgangajhabua.org/refund-policy-2/" class="mons-font6 flink">REFUND POLICY</a>
            <a href="https://shivgangajhabua.org/contact-us/" class="mons-font6 flink">CONTACT</a>
            <a href="https://shivgangajhabua.org/refund-policy-2/" class="mons-font6 flink">PRIVACY POLICY</a>
            <a href="https://shivgangajhabua.org/newsletter-shivganga/" class="mons-font6 flink">NEWSLETTER – SHIVGANGA</a>
          </div>
        </div>
      </div>
    </footer>
<script>

</script> -->