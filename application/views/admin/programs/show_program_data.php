<!-- END: Main Menu-->
<?php
	if(empty($_SESSION['user_name'])) {
		redirect('adminLogin');
	}
?>

<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Programs</h2>
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Programs
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrum-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                class="feather icon-settings"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Chat</a><a
                                class="dropdown-item" href="#">Email</a><a class="dropdown-item" href="#">Calendar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Tables start -->
            <div class="row" id="basic-table">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Programs</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">

                                <!-- Table with outer spacing -->
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Sr. No</th>
                                                <th>Image</th>
                                                <th>Title</th>
                                                <th>Content</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $sr = 1; 
                                                foreach($blog_data as $row){?>
                                            <tr>
                                                <td><?php echo $sr?></td>
                                                <td><img src="<?php echo base_url()?><?php echo $row->image?>"
                                                        height="50px" width="50px"></td>
                                                <td><?php echo $row->title?></td>
                                                <td><?php echo $row->content?></td>
                                                <p>
                                                    <td><?php echo date('d-m-Y H:i' , strtotime($row->created_at))?>
                                                    </td>
                                                </p>
                                                <td><a
                                                        href="<?php echo base_url('admin/edit_program/')?><?php echo $row->id?>">Edit</a><br><a
                                                        href="<?php echo base_url('admin/delete_program/')?><?php echo  $row->id?>">Delete</a>
                                                </td>
                                            </tr>
                                            <?php $sr++; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Responsive tables end -->
                        </div>
                    </div>
                </div>
                <!-- END: Content-->

                <div class="sidenav-overlay"></div>
                <div class="drag-target"></div>

                <!-- BEGIN: Footer-->

                <!-- END: Footer-->


                <!-- BEGIN: Vendor JS-->
                <script src="<?php echo base_url(); ?>assets/app-assets/vendors/js/vendors.min.js"></script>
                <!-- BEGIN Vendor JS-->

                <!-- BEGIN: Page Vendor JS-->
                <!-- END: Page Vendor JS-->

                <!-- BEGIN: Theme JS-->
                <script src="<?php echo base_url(); ?>assets/app-assets/js/core/app-menu.js"></script>
                <script src="<?php echo base_url(); ?>assets/app-assets/js/core/app.js"></script>
                <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/components.js"></script>
                <!-- END: Theme JS-->

                <!-- BEGIN: Page JS-->
                <!-- END: Page JS-->

                </body>
                <!-- END: Body-->

                </html>