<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Shivganga</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
    h3,
    h2 {
        color: black;
        font-style: initial;
    }
    </style>
</head>

<body>
    <div align="center">
        <table border="5" style="height:100%; width:60%;">
            <div align="center">
                <img src="https://shivgangajhabua.org/wp-content/uploads/2019/03/sj.png">
                <h1 align="center" style="color: green">Shivganga Samragra Gramvikas Parishad</h1>
            </div>
            <div align="center">
                <h5 style="color:back;">IIT Roorkee - Haridwar Highway, Roorkee, Uttarakhand 247667</h5>
            </div>
            <div align="center">
                <h2 style="color:back;">This is to certify that following donation has been made.</h2>
            </div>

            <div style="margin-left:85px">
                <?php 
            foreach($details as $row){?>

                <h3>Donation Id: <?php echo $row->payment_id; ?></h3>
                <h3>Name: <?php echo $row->first_name; ?> <?php echo $row->last_name; ?></h3>
                <h3>Amount: <?php echo $row->amount; ?></h3>
                <h3>Campaign: For Planting</h3>
                <h3>Address: <?php echo $row->address; ?></h3>
                <h3>Date: <?php echo date('d-m-Y H:i' , strtotime($row->created_at));?></h3>
                <h3>Summary: Thank You for Your Contribution</h3>
            </div>

            <?php } ?>
    </div>
    </table>
    <div align="right">

        <h4>-issued ByTreasurer, Shivganga</h4>
        <h4>(Please send us your PAN at contact@shivganga.com for the donation amount >Rs 2000)</h4>
    </div>

</body>

</html>