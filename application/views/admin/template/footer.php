<footer class="footer footer-static footer-light">
    <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT
            &copy; 2021<a class="text-bold-800 grey darken-2" href="" target="_blank">Shivganga,</a>All rights
            Reserved</span><span class="float-md-right d-none d-md-block"></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
    </p>
</footer>
<script>
$(document).ready(function() {
    var activeurl = window.location;
    console.log(activeurl)
    $('a[href="' + activeurl + '"]').parent('li').addClass('active');
    $('a[href="' + activeurl + '"]').parents('li').addClass('open');
    if (activeurl == "<?php echo base_url(); ?>") {
        console.log("<?php echo base_url(); ?>")
        $('a[href="' + activeurl + 'home"]').parent('li').addClass('active');
    }
})
</script>