<?php
	// if(empty($_SESSION['user_name'])) {
	// 	redirect('login');
	// }
?>
<div class="bg-f8">
		<div class="container">
			<div class="row pt-5">
				<div class="col-md-12 text-center">
					<h4>Upcoming Events</h4>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-md-4">
					<div class="wpo-rural-event">
						<img src="<?php echo base_url(); ?>assets/img/img-events.png" alt="" class="four-img">
						<div class="pl-2 pt-2 event-div">
							<h4>Jhabua Summer Camp</h4>
							<p class="event-p1">Earlier children used to go to their Nani or Dadi’s house, which used to be in villages.
							</p>
							<a href="<?php echo base_url();?>eventsviewmore" class="event-view-more">View More</a>
							<p class="event-date pt-2 mb-0">November 20, 2019</p>
							<p class="time mb-0">9 AM - 2 PM</p>
							<p class="event-add pb-3 mb-0">Golden Jubilee Hall, SGSITS, Indore.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="wpo-rural-event">
						<img src="<?php echo base_url(); ?>assets/img/img-events.png" alt="" class="four-img">
						<div class="pl-2 pt-2 event-div">
							<h4>Jhabua Summer Camp</h4>
							<p class="event-p1">Earlier children used to go to their Nani or Dadi’s house, which used to be in villages.
							</p>
							<a href="<?php echo base_url();?>eventsviewmore" class="event-view-more">View More</a>
							<p class="event-date pt-2 mb-0">November 20, 2019</p>
							<p class="time mb-0">9 AM - 2 PM</p>
							<p class="event-add pb-3 mb-0">Golden Jubilee Hall, SGSITS, Indore.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="wpo-rural-event">
						<img src="<?php echo base_url(); ?>assets/img/img-events.png" alt="" class="four-img">
						<div class="pl-2 pt-2 event-div">
							<h4>Jhabua Summer Camp</h4>
							<p class="event-p1">Earlier children used to go to their Nani or Dadi’s house, which used to be in villages.
							</p>
							<a href="<?php echo base_url();?>eventsviewmore" class="event-view-more">View More</a>
							<p class="event-date pt-2 mb-0">November 20, 2019</p>
							<p class="time mb-0">9 AM - 2 PM</p>
							<p class="event-add pb-3 mb-0">Golden Jubilee Hall, SGSITS, Indore.</p>
						</div>
					</div>
				</div>
							
			</div>
			<div class="row pt-5">
				<div class="col-md-12 text-center">
					<h4 >Previous Events</h4>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-md-4">
					<div class="wpo-rural-event">
						<img src="<?php echo base_url(); ?>assets/img/img-events.png" alt="" class="four-img">
						<div class="pl-2 pt-2 event-div">
							<h4>Jhabua Summer Camp</h4>
							<p class="event-p1">Earlier children used to go to their Nani or Dadi’s house, which used to be in villages.
							</p>
							<a href="<?php echo base_url();?>eventsviewmore" class="event-view-more">View More</a>
							<p class="event-date pt-2 mb-0">November 20, 2019</p>
							<p class="time mb-0">9 AM - 2 PM</p>
							<p class="event-add pb-3 mb-0">Golden Jubilee Hall, SGSITS, Indore.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="wpo-rural-event">
						<img src="<?php echo base_url(); ?>assets/img/img-events.png" alt="" class="four-img">
						<div class="pl-2 pt-2 event-div">
							<h4>Jhabua Summer Camp</h4>
							<p class="event-p1">Earlier children used to go to their Nani or Dadi’s house, which used to be in villages.
							</p>
							<a href="<?php echo base_url();?>eventsviewmore" class="event-view-more">View More</a>
							<p class="event-date pt-2 mb-0">November 20, 2019</p>
							<p class="time mb-0">9 AM - 2 PM</p>
							<p class="event-add pb-3 mb-0">Golden Jubilee Hall, SGSITS, Indore.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="wpo-rural-event">
						<img src="<?php echo base_url(); ?>assets/img/img-events.png" alt="" class="four-img">
						<div class="pl-2 pt-2 event-div">
							<h4>Jhabua Summer Camp</h4>
							<p class="event-p1">Earlier children used to go to their Nani or Dadi’s house, which used to be in villages.
							</p>
							<a href="<?php echo base_url();?>eventsviewmore" class="event-view-more">View More</a>
							<p class="event-date pt-2 mb-0">November 20, 2019</p>
							<p class="time mb-0">9 AM - 2 PM</p>
							<p class="event-add pb-3 mb-0">Golden Jubilee Hall, SGSITS, Indore.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>