<?php
if (empty($_SESSION['user_name'])) {
} else {

	redirect('login');
}
?>
<style>
	.gallery .prev,
	.gallery .next {
	position: fixed;
	top: 0;
	width: 15%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	text-decoration: none;
	z-index: 999;
	color: rgba(255,255,255,.5);
	font-size: 8rem;
	font-family: monospace;
	transition: all .2s;
}

	.gallery .prev:hover,
	.gallery .next:hover {
	color: rgba(255,255,255,.8);
}

	.gallery .prev {left: -25%;}
	.gallery .next {right: -25%;}

	.gallery .active .prev {left: 0;}
	.gallery .active .next {right: 0;}

	.gallery .container {
	margin: 10vh auto;
	max-width: 80%;
	display: grid;
	grid-gap: 5px;
	grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
	grid-auto-rows: 250px;
	grid-auto-flow: dense;
}

.gallery div {
	text-align: center;
	padding: 1rem 0;
	color: white;
	font-size: 3rem;
	text-transform: uppercase;
	background: rgba(0,0,0,.2);
	overflow: hidden;
	padding: 0;
	display: flex;
	align-items: stretch;
	justify-content: center;
}

.gallery div img {
	width: 100%;
	height: 100%;
	display: block;
	object-fit: cover;
	object-position: center;
	transition: all .5s;
}

.gallery div.show::before {
	content: '';
	display: block;
	position: fixed;
	z-index: 555;
	width: 100vw;
	height: 100vh;
	top: 0; left: 0;
	background: rgba(0,0,0,.6);
}

.gallery div.show img {
	position: fixed;
	top: 0; left: 0;
	margin: 30px;
	align-self: center;
	object-fit: contain;
	z-index: 666;
	width: calc(100% - 60px);
	height: calc(100% - 60px);
	filter: drop-shadow(0 3px 15px black);
	cursor: url(close-button.png), grab; /* custom cursor to signal close on click */
}

.gallery div:not(.show):hover img {
	cursor: pointer;
	transform: scale(1.3);
}


.gallery .horizontal {
	grid-column: span 2;
}

.gallery .vertical {
	grid-row: span 2;
}

.gallery .big {
	grid-column: span 2;
	grid-row: span 2;
}

.gallery .active div:not(.show) img {
	filter: blur(3px);
}
</style>
<script>
	// TODO: touch events

const divs = document.querySelectorAll(".gallery div");
const body = document.body;
const prev = document.querySelector(".gallery .prev");
const next = document.querySelector(".gallery .next");

checkPrev = () =>
	document.querySelector("div:first-child").classList.contains("show")
		? (prev.style.display = "none")
		: (prev.style.display = "flex");

checkNext = () =>
	document.querySelector("div:last-child").classList.contains("show")
		? (next.style.display = "none")
		: (next.style.display = "flex");

Array.prototype.slice.call(divs).forEach(function (el) {
	el.addEventListener("click", function () {
		this.classList.toggle("show");
		body.classList.toggle("active");
		checkNext();
		checkPrev();
	});
});

prev.addEventListener("click", function () {
	const show = document.querySelector(".show");
	const event = document.createEvent("HTMLEvents");
	event.initEvent("click", true, false);

	show.previousElementSibling.dispatchEvent(event);
	show.classList.remove("show");
	body.classList.toggle("active");
	checkNext();
});

next.addEventListener("click", function () {
	const show = document.querySelector(".show");
	const event = document.createEvent("HTMLEvents");
	event.initEvent("click", true, false);

	show.nextElementSibling.dispatchEvent(event);
	show.classList.remove("show");
	body.classList.toggle("active");
	checkPrev();
});

</script>
<div class="halma2020-div header-image-award">
	<div class="carousel-caption">
		<h1>Awards and achievements</h1>
	</div>
</div>
	<div class="bg-f8">
		<div class="container">
			<div class="row pt-5">
				<div class="col-md-12 text-center">
					<h4 class="our-purpose"></h4>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-md-10 offset-md-1">
					<div class="owl-one award-slider owl-carousel">
						<div class="item wpo-rural-item-jhabua">
							<img src="<?php echo base_url(); ?>assets/img/award/padma.png" alt="" />
							<div class="insider-div pl-0">
								<h4 class="f-contact pl-0">Padma Awards</h4>
								<p>Shri Mahesh Sharma, founder member and President of Shivganga was awarded Padmshri for his contribution in the empowerment of tribal rural areas in Madhya Pradesh.
								</p>
							</div>
						</div>
						<div class="item wpo-rural-item-jhabua">
							<img src="<?php echo base_url(); ?>assets/img/award/iprenuer.jpg" alt="" />
							<div class="insider-div pl-0">
								<h4 class="f-contact pl-0">Ipreneur'18</h4>
								<p>
									Jhabua Naturals represented by Nitin Dhakad and Vijen Amlaiyar received the 1st position in the ‘Enterprise’ category in the event ‘Iprenerur’18’ organised by TISS Mumbai.. </p>
							</div>
						</div>
						<div class="item wpo-rural-item-jhabua">
							<img src="<?php echo base_url(); ?>assets//img/award/chirangjilaldhanuka.jpg" alt="" />
							<div class="insider-div pl-0">
								<h4 class="f-contact pl-0">Chiranjilal Dhanuka Smriti Samaj Seva Award</h4>
								<p>Lions Club New Delhi Alakhnanda awarded Shivganga Samagra Gramvikas Parishad the most prestigious award ‘Chiranjilal Dhanuka Smriti Samaj Seva Award' under Major Ngo category for year 2020 his remarkable contribution in the field of rural development and tribal upliftment.</p>
							</div>
						</div>
						<div class="item wpo-rural-item-jhabua">
							<img src="<?php echo base_url(); ?>assets/img/award/watermission.jpg" alt="" />
							<div class="insider-div pl-0">
								<h4 class="f-contact pl-0">National Water Mission Award</h4>
								<p>Shivganga was being awarded by National Water Mission Award 2019 by Ministry of Jal Shakti India in category of Focused attention to vulnerable areas including over-exploited areas. Jal Shakti Minister Shree Gajendra Shekhawat Ji awarded this to Shree Mahesh Sharma ji and Shree Rajaram Katara ji of Shivganga. </p>
							</div>
						</div>
						<div class="item wpo-rural-item-jhabua">
							<img src="<?php echo base_url(); ?>assets/img/award/antoday.jpg" alt="" />
							<div class="insider-div pl-0">
								<h4 class="f-contact pl-0">The Vision of Antodaya</h4>
								<p>In year 19-20 Shivganga’s approach towards holistic and sustainable village development got a place in the book “The Vision for Antodaya” which was inagratued by Hanourable Vice-President of India, Shree Vainkaya Naidu Ji. This book is on 408 best prectices of India which work on ground level and have substantial changes in society. Shree Mahesh Sharmaji was presented in inaguration ceremony held at President House. </p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row pt-1">
				<div class="col-md-12 text-center">
					<h4 class="our-purpose">Media coverage</h4>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-md-8 offset-md-2">
					<div class="owl-four owl-carousel media-slider">
						<div class="item">
							<img src="<?php echo base_url(); ?>assets/img/media/1.jpg
							" alt="" class="t-img" />
						</div>
						<div class="item">
							<img src="<?php echo base_url(); ?>assets/img/media/2.jpg
							" alt="" class="t-img" />
						</div>
						<div class="item">
							<img src="<?php echo base_url(); ?>assets/img/media/3.jpg
							" alt="" class="t-img" />
						</div>
						<div class="item">
							<img src="<?php echo base_url(); ?>assets/img/media/4.jpg
							" alt="" class="t-img" />
						</div>
						<div class="item">
							<img src="<?php echo base_url(); ?>assets/img/media/5.jpg
							" alt="" class="t-img" />
						</div>
						<div class="item">
							<img src="<?php echo base_url(); ?>assets/img/media/6.jpg
							" alt="" class="t-img" />
						</div>
											<div class="item">
							<img src="<?php echo base_url(); ?>assets/img/media/7.jpg" alt="" class="t-img" />
						</div>
					</div>
				</div>
			</div>
			<div class="row pt-5 pb-5">
				<div class="col-md-12 text-center">
					<h4 class="our-purpose">Gallery</h4>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="container-fluid gallery">
<a href="#" class="prev">&lt;</a>
	<a href="#" class="next">&gt;</a>
	<main class="container">
		<div><img src="<?php echo base_url(); ?>assets/img/carsl1-2-min.jpeg" alt=""></div>
		<div class="vertical"><img src="<?php echo base_url(); ?>assets/img/halma2020-video-img.jpeg" alt=""></div>
		<div class="horizontal"><img src="<?php echo base_url(); ?>assets/img/img-gallery2.jpeg" alt=""></div>
		<div><img src="<?php echo base_url(); ?>assets/img/img-gallery.jpeg" class="img-about3"></div>
		<div><img src="<?php echo base_url(); ?>assets/img/img-gallery1.jpeg" alt=""></div>
		<div><img src="<?php echo base_url(); ?>assets/img/1.jpg" alt=""></div>

		<!-- <div class="big"><img src="<?php echo base_url(); ?>assets/img/1.jpg" alt=""></div> -->
		<!-- <div><img src="https://picsum.photos/id/27/800" alt=""></div>
		<div class="vertical"><img src="https://picsum.photos/id/28/800" alt=""></div>
		<div><img src="https://picsum.photos/id/29/800" alt=""></div>
		<div class="horizontal"><img src="https://picsum.photos/id/10/800" alt=""></div>
		<div><img src="https://picsum.photos/id/11/800" alt=""></div>
		<div class="big"><img src="https://picsum.photos/id/12/800" alt=""></div>
		<div><img src="https://picsum.photos/id/13/800" alt=""></div>
		<div class="horizontal"><img src="https://picsum.photos/id/14/800" alt=""></div>
		<div><img src="https://picsum.photos/id/15/800" alt=""></div>
		<div class="big"><img src="https://picsum.photos/id/16/800" alt=""></div>
		<div><img src="https://picsum.photos/id/17/800" alt=""></div>
		<div class="vertical"><img src="https://picsum.photos/id/18/800" alt=""></div> -->
	</main>
</div>
<script>
	$(document).ready(function() {
		$('.owl-one').owlCarousel({
			margin: 20,
			autoWidth: false,
			nav: true,
			autoplay: true,
			autoPlayTimeout: 2000,
			autoplaySpeed: 2500,
			smartSpeed: 2500,
			loop: true,
			items: 3,
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 2
				},
				992:{
					items: 3
				}
			},
			navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			
		});
		$('.owl-four').owlCarousel({
			margin: 20,
			autoWidth: false,
			nav: true,
			loop: true,
			autoplay: true,
			autoPlayTimeout: 2000,
			autoplaySpeed: 2500,
			smartSpeed: 2500,
			items: 3,
			navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			responsive: {
                        0: {
                            items: 1,
        
                        },
						600: {
							items: 2
						},
                        912: {
                            items: 2
                        }
            }
		});
	});
</script>