<?php
// if(empty($_SESSION['user_name'])) {
// }else{
// 	redirect('login');

// }
// print_r($show_shivganga_about_usData);die;
?>
<div class="halma2020-div about header-image-about">
	<?php foreach ($show_shivganga_about_usData as $about_us) { ?>
		<div class="carousel-caption">
			<h1><?php echo $about_us->title ?></h1>
		</div>
</div>
<div class="bg-f8 about">
	<div class="container">
		<div class="row pt-5 pb-5">
			<div class="col-md-12 text-center">
				<p class="what-we-do">ABOUT SHIVGANGA</p>
				<h4 class="our-purpose"><?php echo $about_us->sub_title ?></h4>
				<p class="the-primary halma-primary-txt"><?php echo $about_us->content ?>
				</p>
				<p class="the-primary halma-primary-txt">

				</p>
			<?php } ?>
			</div>
		</div>
	</div>
</div>

<div class="bg-34 about">
	<div class="container pt-5 pb-5">
		<div class="row">
			<div class="col-md-6">
				<img src="<?php echo base_url(); ?>assets/img/img-about2.png" class="img-activism pb-4">
			</div>
			<div class="col-md-6">
				<p class="what-we-do text-center">OUR STORY</p>
				<h4 class="our-purpose">A Progressive unfoldment</h4>
				<p class="the-primary">
					It all started around twenty years back. Mahesh Ji roamed around village-to village in Jhabua for five years, observing and living among the tribal people. He felt that a community living with the highest values of humanity is suffering and is perceived as backward, uncivilised and even ‘aboriginal’. Mahesh Ji and Shri Harsh Chouhan - a tribal himself, began discussing a need for a movement. In 2005, along with some tribal social leaders, they organised a training camp for youths. It was the first time the tribal youths of Jhabua were discussing the pain of their community. The water crisis came out to be the most stressful pain. Thus began Shivganga - a movement taking inspiration from Raja Bhagirath’s effort to bring Ganga to Earth - to bring Ganga (water) to quench Jamee Mata (mother earth) of Jhabua.  In 2007, it became Shivganga Samagra Gramvikas Parishad - a voluntary nonprofit organisation for ‘Holistic Rural Development’.
				</p>
				<!-- <p class="the-primary">
						Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et
						commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
						Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula.
						Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros
						est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi.
						Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo
						eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing
						sapien, sed malesuada diam lacus eget erat.
					</p> -->
			</div>
		</div>
	</div>
</div>

<div class="container pt-5">
	<div class="political-header-bottom m-top-02">
		<div class="row">
			<div class="col-lg-4 col-md-12">
				<div class="our-vision-item wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
					<div class="vision-bg">
						<div class="content">
							<div class="subtitle">
								<p></p>
								<div class="icon">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
							</div>
							<h4 class="title">OUR PHILOSOPHY</h4>
							<div class="btn-wrapper">
								<a href="<?php echo base_url() ?>donate" class="boxed-btn btn-sanatory style-01 reverse"><i class="fas fa-arrow-right"></i>Donate</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 green-icon">
				<div class="vision-single-item-wrapper">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="our-vision-single-item about-new Parmartha wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; height: 292px;">
								<div class="content">
									<h4 class="title">Parmartha</h4>
									<p class="the-primary">Leveraging of any kind of motivation for self,
										family or village becomes a deadlock in bringing collective prosperity and leads to
										conflict. Parmarth is about finding blissfulness in working for others without expecting.
										The motivation of Parmarth upholds self-respect and leads to sustainable development for
										everyone
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="our-vision-single-item about-new style-01 Swavlambhan wow animate__ animate__fadeInRight  animated" style="visibility: visible; animation-name: fadeInRight; height: 292px;">
								<div class="content">
									<h4 class="title">Swavlambhan</h4>
									<p class="the-primary">
										The real development of a person or community means ending their vulnerability and
										empowering them to become self-dependent(Swavlambhan). But , it is often forgotten
										that self-reliance comes only from self-esteem(Swabhiman). Hence, the building of
										self-esteem is the beginning of development.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="our-vision-single-item about-new style-02 Swabhiman wow animate__ animate__fadeInUp animate__delay-1s animated" style="visibility: visible; animation-name: fadeInUp; height: 292px;">
								<div class="content">
									<h4 class="title">Swabhiman</h4>
									<p class="the-primary">
										Samvardhan leads to Samruddhi’ (Prosperity through Enrichment) is a principle which
										says it is not enough to conserve the natural resources, but there is a need for
										enriching them. The five basic resources of this planet .i.e., Jan(people), Jal(water),
										Jungle(forest), jameen(farmland) and Janawar(animal), be not only conserved but also be
										enriched.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="our-vision-single-item about-new style-02 Sustainable wow animate__ animate__fadeInUp animate__delay-1s animated" style="visibility: visible; animation-name: fadeInUp; height: 292px;">
								<div class="content">
									<h4 class="title">Sustainable Development</h4>
									<p class="the-primary">Sustainable Development is the ultimate goal. The solid base of sustainable development is Swavlamban. Only a self-reliant community can attain sustainable development. Swavlamban comes from Swabhiman, i.e. a community with high self-esteem. And one can have high self-esteem only by virtue of Parmarth.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="wpo-rural-item">
						<p class="the-primary">Leveraging of any kind of motivation for self,
						family or village becomes a deadlock in bringing collective prosperity and leads to
						conflict. Parmarth is about finding blissfulness in working for others without expecting.
						The motivation of Parmarth upholds self-respect and leads to sustainable development for
						everyone </p>
					</div>
			</div>
			<div class="col-md-4">
				<div class="wpo-rural-item">
					<p class="the-primary">
						The real development of a person or community means ending their vulnerability and
						empowering them to become self-dependent(Swavlambhan). But , it is often forgotten
						that self-reliance comes only from self-esteem(Swabhiman). Hence, the building of
						self-esteem is the beginning of development.
					</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="wpo-rural-item">
					<p class="the-primary">
						Samvardhan leads to Samruddhi’ (Prosperity through Enrichment) is a principle which
						says it is not enough to conserve the natural resources, but there is a need for
						enriching them. The five basic resources of this planet .i.e., Jan(people), Jal(water),
						Jungle(forest), jameen(farmland) and Janawar(animal), be not only conserved but also be
						enriched.
					</p>
				</div>
			</div>
		</div>
	</div> -->
<div class="container-fluid mb-5">
	<div class="container">
		<div class="administration-section">
			<div class="row pt-5">
				<div class="col-md-12 text-center">
					<h4 class="our-purpose">Meet the Founders</h4>
				</div>
			</div>
			<div class="container custom-container mt-5">
				<img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
				<img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
				<div class="row justify-content-center">
					<div class="col-lg-6 col-md-10 col-sm-12">
						<div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
							<div class="administrative-bg">
								<img class="founder-img" src="<?php echo base_url(); ?>assets/img/mahesh.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-10 col-sm-12">
						<div class="administration-single-items style-01">
							<div class="content">
								<h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Mahesh Sharma</h4>
							</div>
							<div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
								<h5 class="title">Founder</h5>
								<div class="icon">
									<img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
								</div>
							</div>
							<div class="content">
								<p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Shri Mahesh Sharma spent his formative years in the company of social reformers and
									leaders. This instilled in him the drive to work towards the betterment of communities.
									With an experience of over 20 years in the social reform field; he came to Jhabua in 1998
									to study the tribal ecosystem there. Over the years, he has developed a deep understanding
									of the tribal ecosystem and now has networks in over 800 villages. He has played a
									critical role in bringing the tribal people together and was recently conferred with the
									‘Padma Shri’ in recognition of his tremendous contribution</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-center mt-5">
					<div class="col-lg-6 col-md-10 col-sm-12">
						<div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
							<div class="administrative-bg">
								<img class="founder-img" src="<?php echo base_url(); ?>assets/img/harshchohan.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-10 col-sm-12">
						<div class="administration-single-items style-01">
							<div class="content">
								<h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Harsh Chouhan</h4>
							</div>
							<div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
								<h5 class="title">Founder</h5>
								<div class="icon">
									<img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
								</div>
							</div>
							<div class="content">
								<p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Graduated from SGSITS, B.Tech in Mechanical in 1985, completed in Masters from IIT Delhi. Since 1988, he has been active in the field of social work. Beginning with Vanvasi Kalyan Ashram, he’s been part of Shivganga since its conceptual inception in 1998. Shri Harsh Chouhan has been recently appointed as Chairman of NCST - National Commission for Scheduled Caste by the President of India.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<div class="bg-f8 about">
	<div class="container">
		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<p class="what-we-do">Make Your Contribution</p>
				<h4 class="our-purpose">Stories of change</h4>
			</div>
		</div>
		<div class="row pt-5">
			<div class="col"></div>
			<div class="col-md-8">
				<div class="owl-three about-carocel owl-carousel stories-change">
					<div class="item about-wpo-rural-item">
						<img src="<?php echo base_url(); ?>assets/img/story/nitin.jpg
							" style="height:220px" alt="" />
						<p class="what-we-do mt-3">FELLOWSHIP</p>


						<p class="the-primary">Nitin Dhakad, B.Tech-Electrical-IIT Roorkee (2016), has been in Jhabua for four years. He came to Jhabua in the Image vs Reality program in 2014. After that, he took a fellowship program for two years to develop the Rural Entrepreneurship Incubation Centre. He conceptualized and initiated Jhabua Naturals. As a full-time volunteer, he is coordinator of the Image vs Reality program, heading Jhabua Crafts venture and engaged in various projects of urban outreach and fundraising.
						</p>
						<h5 class="std-name">NITIN DHAKAD</h5>
					</div>
					<div class="item about-wpo-rural-item">
						<img src="<?php echo base_url(); ?>assets/img/story/vishwanath.jpg" style="height:220px" alt="" />
						<p class="what-we-do mt-3">FELLOWSHIP</p>


						<p class="the-primary">Vishwanath Allannavar, BA-LLB-Bangalore University - Law College(2018), has been working with Shivganga for three years. He came to Jhabua in 2016 through the Image vs Reality program. After some visits, he became a fellow for two years, taking up a project of Social Capital Generation through Youth Empowerment Camps. As a full-time volunteer, Vishwanath is engaged in various projects like Community Forests Rights and Jan Samvardhan dimension of Shivganga.
						</p>
						<h5 class="std-name">Vishwanath Allannavar</h5>
					</div>
					<div class="item about-wpo-rural-item">
						<img src="<?php echo base_url(); ?>assets/img/story/satyajeet.jpg" style="height:220px" alt="" />
						<p class="what-we-do mt-3">FELLOWSHIP</p>


						<p class="the-primary">Satyajeet Patel, B.Tech-Electrical-IIT Roorkee(2018), completed his two-year fellowship this year(2021). He came to Jhabua in his college days in 2016 under the Image vs Reality program. He, with Shivganga’s tribal entrepreneur - Vijendra Amaliyar-has led the Jhabua Naturals venture in his fellowship. Now a full-time volunteer, Satyajeet is serving as Project Leader of Jhabua Naturals. The duo is also leading the Nav-Vigyan Samvardhan dimension, under which the Rural Technology Innovation Lab has been initiated. 
						</p>
						<h5 class="std-name">Satyajeet Patel</h5>
					</div>
					<div class="item about-wpo-rural-item">
						<img src="<?php echo base_url(); ?>assets/img/story/kumar.jpg" style="height:220px" alt="" />
						<p class="what-we-do mt-3">FELLOWSHIP</p>


						<p class="the-primary">Kumar Harsh, B.Arch-IIT Roorkee(2020), is pursuing the fellowship program of Shivganga for one year. He came to Jhabua in 2018 under the Image vs Reality program and also did his final year thesis in Jhabua. Today, he is involved in the Communication Design team and other projects in Shivganga’s Dharmapuri Gurukul.
						</p>
						<h5 class="std-name">Kumar Harsh</h5>
					</div>
					<div class="item about-wpo-rural-item">
						<img src="<?php echo base_url(); ?>assets/img/story/rishab.jpg" style="height:220px" alt="" />
						<p class="what-we-do mt-3">FELLOWSHIP</p>


						<p class="the-primary">Rishabh Seth, B.Tech-IIT Roorkee(2019), met Shri Mahesh Sharma and Nitin Dhakad in Vision India Foundation’s program in Delhi in 2018. Inspired by their address on Rural Development, post-college, Rishabh left his job and came to Jhabua in 2020. As a fellow, he is now involved in the Jhabua Tourism venture and Bamboo Training Centre at Meghnagar.
						</p>
						<h5 class="std-name">Rishabh Seth</h5>
					</div>
					<div class="item about-wpo-rural-item">
						<img src="<?php echo base_url(); ?>assets/img/IMG_1946.jpg" style="height:220px" alt="" />
						<p class="what-we-do mt-3">FELLOWSHIP</p>


						<p class="the-primary">Avinash Mattur, LLM-Amity University (2020), came to Jhabua in 2019 under the Image vs Reality program. After completing his studies, he took on the project of Community Forest Rights in Jhabua. Since then, as a fellow, he has been working on the CFR project spread over 25 villages. </p>
						<h5 class="std-name">Avinash Mattur</h5>
					</div>
				</div>
			</div>
			<div class="col"></div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('.owl-three').owlCarousel({
			margin: 20,
			autoWidth: false,
			nav: true,
			loop: true,
			items: 2,
			navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			responsive: {
				0: {
					items: 1,

				},
				912: {
					items: 2
				}
			}
		});
	});
</script>