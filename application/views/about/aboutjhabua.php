<?php
	// if(empty($_SESSION['user_name'])) {
	// 	redirect('login');
	// }
?>
	<div class="halma2020-div header-image-jhabua">
		<div class="carousel-caption">
			<h1>Vikas Ka Jatan</h1>
		</div>
	</div>
	<div class="bg-f8 jhabua-about">
		<div class="container">
			<div class="row pt-5">
				<div class="col-md-6 offset-md-3 text-left">
					<p class="what-we-do">ABOUT JHABUA</p>
					<h4 class="our-purpose">Where is Jhabua?</h4>
					<p class="the-primary">Jhabua is a district town of Madhya Pradesh,
						150 km from Indore. It is located at the boundary area of Gujrat, Rajasthan and
						Madhya Pradesh. Jhabua-Alirajpur belt has a tribal population of 1.5 million*-mainly
						Bhil tribe with subtribes Bhilada and Pateliya.
					</p>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-md-6 offset-md-3 text-left">
					<p class="what-we-do">OUR HISTORY</p>
					<h4 class="our-purpose">Looking back in time</h4>
					<p class="the-primary">Few decades ago the region was rich in natural resources and
						colorful cultural diversity. In notion of material development this region witnessed
						excessive deforestation. Consequently, the ecosystem of sustainable living got disturbed.
						The loss of habitat left tribal with a lot of problems and agony. The region witness
						rainfall which doesn’t suffice the need for water in other seasons for farming. In the
						lack of regular source of income; around 40000 under debt farmer families migrate every
						year to ensure existence. This migration leads to several subsequent problems for health,
						education, loss of social and cultural diversities.
					</p>
				</div>
			</div>
			<div class="row pt-5">
				<div class="col-md-6 offset-md-3 text-left">
					<p class="what-we-do">PEOPLE OF JHABUA</p>
					<h4 class="our-purpose">Knowing the tribals</h4>
					<p class="the-primary">Bhils are traditionally very skilled archers-they relate themselves
						to Eklavya. All the practices & traditions(e.g. Dirha, Halma) of Bhil empowers
						community participation, connection with nature and its preservation-very much
						evident from terms like Jamimata, Matavan, Babadev. Bhils worship deities like
						Sitlamata, Vaghdev as well Hindu deities like Bholababa-Parvati mata Ganesh & Kartikey.
						There are 45 surnames of Bhils, each one of them corresponds to one of panch tatva,
						the title makes them the protector of that entity
					</p>
				</div>
			</div>
			<div class="row pt-5 pb-5">
				<div class="col-md-6 offset-md-3 text-left">
					<p class="what-we-do">PEOPLE OF JHABUA</p>
					<h4 class="our-purpose">Distress of Bhils</h4>
					<p class="the-primary">With plenty of natural resources, Jhabua has enough resources for
						a healthy living. Still, tribal people have to leave their land and family in search of
						livelihood. ‘Majdoori’ is not a problem but ‘Madoori ki Majboori’ is the problem.o
						experience, learn and contribute to development of Rural India
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 jhabua-about">
		<div class="row col-eq-height">
			<div class="col-sm col-md-3 col-xl col-lg top-100">
				<div class="active-member-div wpo-rural-item-jhabua pr-2 min-height250 wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
					<h5 class="affor-txt-heading pt-2 pb-2">Economic Exploitation</h5>
					<p class="the-primary">
						Every tribal family has a loan as high as 10 lakhs.
					</p>
				</div>
			</div>
			<div class="col-sm col-md-3 col-xl col-lg top-100">
			<div class="active-member-div wpo-rural-item-jhabua pr-2 min-height250 wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
					<h5 class="affor-txt-heading pt-2 pb-2">Water Crisis</h5>
					<p class="the-primary">
						In Jhabua due to undulating land and deforestation, there is high water runoff,
						thus water crisis. People travel as far as 5 km to get drinking water.
					</p>
				</div>
			</div>
			<div class="col-sm col-md-3 col-xl col-lg top-100">
			<div class="active-member-div wpo-rural-item-jhabua pr-2 min-height250 wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
					<h5 class="affor-txt-heading pt-2 pb-2">Seasonal Agriculture</h5>
					<p class="the-primary">
						With lots of rocks and sloping terrain, the soil of Jhabua is challenging for
						agriculture. Water crisis limits the cultivation period to just 4-6 months a year.
					</p>
				</div>
			</div>
			<div class="col-sm col-md-3 col-xl col-lg top-100">
			<div class="active-member-div wpo-rural-item-jhabua pr-2 min-height250 wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
					<h5 class="affor-txt-heading pt-2 pb-2">Deforestation</h5>
					<p class="the-primary">
						Tribal people being ‘Vanvasi’ are experts in making sustainable use of forests
						resources. However, the exploitation and hence depletion of forests resources
						still continues.
					</p>
				</div>
			</div>
			<div class="col-sm col-md-4 col-lg col-xl offset-md-4 offset-lg-0 offset-xl-0 top-100">
			<div class="active-member-div wpo-rural-item-jhabua pr-2 min-height250 wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
					<h5 class="affor-txt-heading pt-2 pb-2">Lost Traditions</h5>
					<p class="the-primary">
						Reducing demands of traditional products aided in fading traditional knowledge.
						Lack of basic amenities like Health Clinics, Road, Banks etc in rural Jhabua makes
						life harder.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-f8 jhabua-about">
		<div class="container">
			<div class="row pt-5">
				<div class="col-md-6 offset-md-3 text-left">
					<h4 class="our-purpose">Festivsls and Traditions</h4>
					<p class="the-primary">The land of Jhabua is enriched with various cultures and traditions.
						The tribals keep on organizing various events through out year like ‘Kanwad Yatra’,
						‘Ganesh Pooja’ which involves community participation and interaction among various
						age-groups.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-f8 jhabua-about">
		<div class="container">
			<div class="row pt-5">
				<div class="col-md-4">
					<div class="issues-single-items">
						<div class="issue-img" style="background-image: url(<?php echo base_url(); ?>assets/img/festiv/halma.jpg);">
							<div class="content">
								<h4 class="title"><a href="issues-single.html">Halma</a></h4>
								<p>Halma is an ancient tradition of Bhil tribe for coming together to 
						make something happen. People of Jhabua used to come together to help a person or family 
						in distress with the feeling of selflessness.</p>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="issues-single-items">
						<div class="issue-img" style="background-image: url(<?php echo base_url(); ?>assets/img/festiv/matawan.jpg);">
							<div class="content">
								<h4 class="title"><a href="issues-single.html">Matavan</a></h4>
								<p>‘Matawan’ literally means ‘forests of Mother’-mother here signifies 
									the Mother Earth. This is a traditional term used by Bhils for specially protected forest 
									areas where tribal do not cut trees in any case.</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="issues-single-items">
						<div class="issue-img" style="background-image: url(<?php echo base_url(); ?>assets/img/img-about-jhabua2.png);">
							<div class="content">
								<h4 class="title"><a href="issues-single.html">Bhagodia</a></h4>
								<p>Bhangodia is a festive celebration of 10 days, parallel with Holi, 
						in which people gather, meet, sing & dance together. This is a time when cultivation 
						season ends and farmers bring ‘Rabi’ harvest in their home.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row pt-5 pb-5">
				<div class="col-md-4">
					<div class="issues-single-items">
						<div class="issue-img" style="background-image: url(<?php echo base_url(); ?>assets/img/festiv/kawad.jpg);">
							<div class="content">
								<h4 class="title"><a href="issues-single.html">Kanwad Yatra</a></h4>
								<p>It is held in the Srawan month of Hindu calendar, this is organised 
						to develop leadership in youths.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="issues-single-items">
						<div class="issue-img" style="background-image: url(<?php echo base_url(); ?>assets/img/img-about-jhabua4.jpeg);">
							<div class="content">
								<h4 class="title"><a href="issues-single.html">Ganeshotsav</a></h4>
								<p>‘Matawan’ literally means ‘forests of Mother’-mother here signifies 
						the Mother Earth. This is a traditional term used by Bhils for specially protected forest 
						areas where tribal do not cut trees in any case.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="issues-single-items">
						<div class="issue-img" style="background-image: url(<?php echo base_url(); ?>assets/img/img-about-jhabua3.png);">
							<div class="content">
								<h4 class="title"><a href="issues-single.html">Gaiti Yatra</a></h4>
								<p>Gaiti Yatra’ holds a special place for the tribals because it 
						showcases Gaiti, the spade or mattock used by these tribal farmers, as a weapon of change.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>