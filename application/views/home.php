<?php

foreach ($show_extraActivities_Data as $row1) {
    $halmaImage = $row1->image;
}
?>
<style>
    .owl-prev,
    .owl-next {
        background-color: #00000045 !important;
    }

    body {
        background-color: #f8f8f8;
    }

    p {
        font-size: 35px;
    }

    .vl {
        border-left: 2px solid grey;
        height: 220px;
    }

    .vl2 {
        border-left: 2px solid rgb(11 26 49);
        height: 220px;
    }

    .vl3 {
        height: 220px;
    }

    .carousel-fade .carousel-inner .item {
        opacity: 0;
        -webkit-transition-property: opacity;
        -moz-transition-property: opacity;
        -o-transition-property: opacity;
        transition-property: opacity;
    }

    .carousel-fade .carousel-inner .active {
        opacity: 1;
    }

    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
        left: 0;
        opacity: 0;
        z-index: 1;
    }

    .carousel-fade .carousel-inner .next.left,
    .carousel-fade .carousel-inner .prev.right {
        opacity: 1;
    }

    .carousel-fade .carousel-control {
        z-index: 2;
    }
</style>
<!-- banner start -->
<div id="demo" class="carousel slide crsl-one slide carousel-fade" data-ride="carousel" data-interval="5000">
    <div class="carousel-inner">
        <?php
        foreach ($banner as $slide) {
            if ($slide->id == 1) {
                $isactive = 'active';
            } else {
                $isactive = '';
            }
        ?>

            <div class="carousel-item <?= $isactive ?>">
                <img src="<?php echo base_url(); ?><?php echo $slide->image ?>" style="" alt="carousel">
                <div class="carousel-caption">
                    <div class="row">
                        <div class="col-md-6 text-xs-center">
                            <span class="p1"><b><?php echo $slide->title ?></b></span>
                        </div>
                        <div class="col-md-6 text-md-right text-xs-center px-md-4">
                            <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
                        </div>
                    </div>
                </div>

            </div>
        <?php
        }
        ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<!-- banner end -->
<!-- graphic start -->
<div class="container-fluid shape p-0">
    <img src="<?php echo base_url(); ?>assets/img/shape.jpeg" alt="">
</div>
<!-- graphic end -->
<!-- halma start -->
<?php foreach ($show_extraActivities_Data as $row) { ?>
    <div class="bg-f9 halma-section">
        <div class="container">
            <div class="row">
                <div class="col-12 bg-halma1 bg-halma text-center" style="height:300px">
                    <h3><?php echo $row->title ?></h3>
                    <p class="pb-3"><?php echo $row->des ?></p>
                    <a href="<?php echo base_url(); ?>halma2020" class="btn donate-page btn-donate">KNOW MORE</a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- banner end -->
<!-- Our Programs start -->
<div class="bg-f9 pb-5">
    <div class="container">
        <div class="row p-80">
            <div class="col-md-12 text-center">
                <h4 class="our-purpose">Our Programs</h4><br>
            </div>
        </div>

        <div class="row text-center m-bg-blue">
            <?php
            $count = 1;
            foreach ($showblogData as $blogdata) { ?>

                <div class="col-md-4">
                    <div class="wpo-mission-item">
                        <img src="<?php echo base_url(); ?><?php echo $blogdata->image ?>" class="" style="height:300px" alt="forestation" />

                        <div class="text-center mb-3">
                            <?php
                            if ($count == 1) {
                            ?>
                                <a style="font-size:20px" href="<?php echo base_url() ?>youth_Empower" class="know-more-txt">
                                    Know More
                                </a>
                            <?php
                            } else if ($count == 2) {
                            ?>
                                <a style="font-size:20px" href="<?php echo base_url() ?>waterconservation" class="know-more-txt">
                                    Know More
                                </a>
                            <?php
                            }
                            if ($count == 3) {
                            ?>
                                <a style="font-size:20px" href="<?php echo base_url() ?>levelihood_entrepreneurship" class="know-more-txt">
                                    Know More
                                </a>
                            <?php
                            }
                            if ($count == 4) {
                            ?>
                                <a style="font-size:20px" href="<?php echo base_url() ?>programforest" class="know-more-txt">
                                    Know More
                                </a>
                            <?php
                            }
                            if ($count == 5) {
                            ?>
                                <a style="font-size:20px" href="<?php echo base_url() ?>women_empowerment" class="know-more-txt">
                                    Know More
                                </a>
                            <?php
                            }
                            if ($count == 6) {
                            ?>
                                <a style="font-size:20px" href="<?php echo base_url() ?>connecting_communities" class="know-more-txt">
                                    Know More
                                </a>
                            <?php
                            }
                            $count++;
                            ?>

                        </div>
                    </div>

                </div>

            <?php } ?>
        </div>
    </div>
</div>
<!-- Our Programs end -->
<!-- Our counter start -->
<div class="container-fluid p-0 home-counter">
    <div class="wpo-counter-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wpo-counter-grids">
                        <div class="grid">
                            <div id="counter">
                                <h2><span class="odometer odometer-auto-theme counter-value" data-count="48">0</span>+</h2>
                            </div>
                            <p>Programs</p>
                        </div>
                        <div class="grid">
                            <div id="counter">
                                <h2><span class="odometer odometer-auto-theme counter-value" data-count="900">0</span>+</h2>
                            </div>
                            <p>VILLAGES</p>
                        </div>
                        <div class="grid">
                            <div id="counter">
                                <h2><span class="odometer odometer-auto-theme counter-value" data-count="4000">0</span>+</h2>
                            </div>
                            <p>VOLUNTEERS</p>
                        </div>
                        <div class="grid">
                            <div id="counter">
                                <h2><span class="odometer odometer-auto-theme counter-value" data-count="14500">0</span>+</h2>
                            </div>
                            <p>FAMILIES</p>
                        </div>
                        <p class="year text-center text-white">EVERY YEAR</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Our counter end -->
<!-- Holistic start -->
<div class="container">
    <div class="row p-80">
        <div class="col-12 purpose">

            <h1 class="holistic">HOLISTIC RURAL DEVELOPMENT</h1>
        </div>
    </div>
    <div class="row m-mb-25 text-center pb-0 pb-md-5">
        <div class="col-md-4">
            <div class="wpo-rural-item">
                <h3 class="we-make">We make social leaders</h3>
                <p class="the-primary ">The primary step is to embed values in youths such that they are
                    naturally
                    inclined towards ‘Parmarth’. They begin thinking for others and their village and which
                    eventually
                    builds their social acceptance in the village, thus making them
                    Social Leaders.
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="wpo-rural-item">
                <h3 class="we-make">We train and empower them</h3>
                <p class="the-primary">The primary step is to embed values in youths such that they are
                    naturally
                    inclined towards ‘Parmarth’. They begin thinking for others and their village and which
                    eventually
                    builds their social acceptance in the village, thus making them
                    Social Leaders.
                </p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="wpo-rural-item">
                <h3 class="we-make">They make changes on ground</h3>
                <p class="the-primary">The primary step is to embed values in youths such that they are
                    naturally
                    inclined towards ‘Parmarth’. They begin thinking for others and their village and which
                    eventually
                    builds their social acceptance in the village, thus making them
                    Social Leaders.
                </p>
            </div>
        </div>
    </div>
</div>
<!-- Holistic end -->
<div class="container">
    <div class="row p-80">
        <div class="col-12 purpose">

            <h1 class="holistic">Our Impact</h1>
        </div>
    </div>
    <div class="row m-mb-25 text-center pb-0 pb-md-5">
        <div class="col-md-12">
            <img src="<?php echo base_url(); ?>assets/img/Stats-1.png" width="100%" height="auto" class="d-none d-md-block" alt="">
            <img src="<?php echo base_url(); ?>assets/img/statmobile.png" width="100%" height="auto" class="d-block d-md-none" alt="">
        </div>
    </div>
</div>
<!-- Political start -->
<div class="container">
    <div class="political-header-bottom m-top-02">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="our-vision-item wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                    <div class="vision-bg">
                        <div class="content">
                            <div class="subtitle">
                                <p></p>
                                <div class="icon">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                            <h4 class="title">Join the convoy</h4>
                            <div class="btn-wrapper">
                                <a href="https://jhabua.nic.in/en/" class="boxed-btn btn-sanatory style-01 reverse visit"><i class="fas fa-arrow-right"></i>Visit Jhabua</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 green-icon">
                <div class="vision-single-item-wrapper">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="our-vision-single-item  wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="icon">
                                    <i class="fa fa-handshake-o" style="font-size:50px;"></i>
                                </div>
                                <div class="content">
                                    <h4 class="title">Get Involved</h4>
                                    <div class="btn-wrapper">
                                        <a href="<?php echo base_url() ?>student_involve" class="boxed-btn btn-sanatory style-01 reverse green"><i class="fas fa-arrow-right"></i>Go</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="our-vision-single-item style-01 wow animate__ animate__fadeInRight  animated" style="visibility: visible; animation-name: fadeInRight;">
                                <div class="icon">
                                    <i class="fa fa-heart" style="font-size:50px;"></i>
                                </div>
                                <div class="content">
                                    <h4 class="title">Help & Donate</h4>
                                    <div class="btn-wrapper">
                                        <a href="<?php echo base_url() ?>donate" class="boxed-btn btn-sanatory style-01 reverse green"><i class="fas fa-arrow-right"></i>Donate</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="our-vision-single-item style-02 wow animate__ animate__fadeInUp animate__delay-1s animated" style="visibility: visible; animation-name: fadeInUp;">
                                <div class="icon">
                                    <i class="fa fa-paper-plane" style="font-size:50px;"></i>
                                </div>
                                <div class="content">
                                    <h4 class="title">Visit Jhabua</h4>
                                    <div class="btn-wrapper">
                                        <a href="https://jhabua.nic.in/en/" class="boxed-btn btn-sanatory style-01 reverse green big"><i class="fas fa-arrow-right"></i>Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Political end -->
<div class="container-fluid row bg-f8 partner px-0 py-5 m-0">
    <div class="col-md-6">
        <div class="row  pt-5">
            <div class="col-md-12 text-center">
                <h4 class="our-purpose" style="font-size:40px">Our Partners</h4>
            </div>
        </div>
        <div id="Owl-Partner" class="owl-partner owl-carousel student-engage">
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/partner/Gail Logo.png" alt="" />
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/partner/nabard.png" alt="" />
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/partner/caringfriends.jpeg" alt="" />
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/partner/pratibha-syntex.jpg" alt="" />
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/partner/yash-technology.png" alt="" />
            </div>
            <div class="item">
                <img src="<?php echo base_url(); ?>assets/img/partner/space.png" alt="" />
            </div>
        </div>
    </div>
    <div class="col-md-6 p-0">
        <img src="<?php echo base_url(); ?>assets/img/bg-new.png" height="100%" width="100%" alt="">
    </div>
</div>
<div class="container py-5">
    <div class="col-md-12 text-center ">
        <h4 class="our-purpose" style="font-size:40px">Interns & Fellows </h4>
    </div>
    <div id="owl-intern" class="row pt-5">
        <div class="col-md-12 col-md-12 px-5">
            <div class="owl-intern owl-carousel student-engage">
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/Delhi.jpg" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/download.png" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/iim.jpg" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/indore.jpg" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/kanpur.png" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/karnatak.png" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/lbsim.jpg" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/varanasi.png" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/mumbai.jpg" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/tiss.jpg" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/niti.jpg" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/patna.png" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/raipur.png" alt="" />
                </div>
                <div class="item wpo-rural-item d-flex self-align-center">
                    <img src="<?php echo base_url(); ?>assets/img/student-engagement/rurkee.jpg" alt="" />
                </div>
            </div>
        </div>
    </div>

</div>
<div class="testimonial-section-area people-say testimonial-bg-02 margin-top-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-6">
                <div class="testimonial-carousel-area">
                    <div class="owl-four owl-carousel stories-change">
                        <div class="item">
                            <div class="people-say-single-item">
                                <img loading="lazy" src="https://themeim.com/demo/senatory/assets/img/quotes.webp" class="quotes" alt="">
                                <div class="content">
                                    <div class="subtitle wow animate__ animate__fadeInUp animated" style="animation-name: fadeInUp;">
                                        <p>Public Comments</p>
                                        <div class="icon">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="subtitle">

                                </div>
                                <h4 class="title">People's Say About us</h4>
                                <p class="description">"The Shivganga… makes use of the tribals’ own cultural resources
                                    to
                                    motivate them to develop. It is one of the more successful among tribal development
                                    schemes."
                                </p>
                                <div class="author-meta">
                                    <span class="author-name">KOENRAAD ELST</span>
                                    <span class="line"></span><br>
                                    <span class="author-name">ORIENTALIST AND INDOLOGIST KNOWN FOR HIS WRITINGS ON COMPARATIVE
                                        RELIGION</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="people-say-single-item">
                                <img loading="lazy" src="https://themeim.com/demo/senatory/assets/img/quotes.webp" class="quotes" alt="">
                                <div class="content">
                                    <div class="subtitle wow animate__ animate__fadeInUp animated" style="animation-name: fadeInUp;">
                                        <p>Public Comments</p>
                                        <div class="icon">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="subtitle">

                                </div>
                                <h4 class="title">People's Say About us</h4>
                                <p class="description">"...Unlike others, Shivganga is neither pushing Aadivasi backwards
                                    as
                                    they are illiterate nor showing them upfront because they’re illiterate. They are
                                    simply
                                    making them realise their energy…so that they themselves can move forward."
                                </p>
                                <div class="author-meta">
                                    <span class="author-name">RAJENDRA SINGH</span>
                                    <span class="line"></span><br>
                                    <span class="author-name">WATERMAN OF INDIA, STOCKHOLM WATER PRIZE-2015</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="people-say-single-item">
                                <img loading="lazy" src="https://themeim.com/demo/senatory/assets/img/quotes.webp" class="quotes" alt="">
                                <div class="content">
                                    <div class="subtitle wow animate__ animate__fadeInUp animated" style="animation-name: fadeInUp;">
                                        <p>Public Comments</p>
                                        <div class="icon">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="subtitle">

                                </div>
                                <h4 class="title">People's Say About us</h4>
                                <p class="description">"Young volunteer organization ( YVO) is proud to be associated with Shivganga Samagra Gramvikas parishad (SSGP) . It has been a pleasure helping these enthusiastic entrepreneurs who have put their hearts and soul in developing 1300 tribal villages in Jhabua. We salute their spirit and feel honored in donating for their cause. Our donations have been utilized effectively to impact over 480 lives."
                                </p>
                                <div class="author-meta">
                                    <span class="author-name">Siddharth Ladsaria</span>
                                    <span class="line"></span><br>
                                    <span class="author-name">CEO , YVO</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="people-say-single-item">
                                <img loading="lazy" src="https://themeim.com/demo/senatory/assets/img/quotes.webp" class="quotes" alt="">
                                <div class="content">
                                    <div class="subtitle wow animate__ animate__fadeInUp animated" style="animation-name: fadeInUp;">
                                        <p>Public Comments</p>
                                        <div class="icon">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="subtitle">

                                </div>
                                <h4 class="title">People's Say About us</h4>
                                <p class="description">"I feel happy that I am contributing to Social capital generation through Akshya Daan of Shivganga, the impact of which will grow manifold with time. I really appreciate what is being done, and I’m honored to be a contributor to this social movement."
                                </p>
                                <div class="author-meta">
                                    <span class="author-name">Amey Mithe</span>
                                    <span class="line"></span><br>
                                    <span class="author-name">Mumbai</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container pt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="row pt-5">
                <div class="col-md-12 text-center">
                    <h4 class="our-purpose">Achievements</h4>
                </div>
            </div>
            <div class="row achievements pt-5">
                <div class="col-md-8 offset-md-2">
                    <div class="owl-one owl-carousel">
                        <div class="item">
                            <img src="<?php echo base_url(); ?>assets/img/award/iprenuer.jpg" alt="" />
                            <div class="insider-div pl-0">
                                <h4 class="f-contact pl-0">Ipreneur'18</h4>
                                <p>Jhabua Naturals represented by Nitin Dhakad and Vijen Amlaiyar received the 1st position in the ‘Enterprise’ category in the event ‘Iprenerur’18’ organised by TISS Mumbai..
                                </p>
                            </div>
                        </div>


                        <div class="item">
                            <img src="<?php echo base_url(); ?>assets/img/IMG_20200506_152549.jpg" alt="" />
                            <div class="insider-div pl-0">
                                <h4 class="f-contact pl-0">Padma Awards</h4>
                                <p>Shri Mahesh Sharma, founder member and President of Shivganga was awarded Padmshri
                                    for his contribution in the empowerment of tribal rural areas in Madhya Pradesh.
                                </p>
                            </div>
                        </div>
                        <!-- new -->
                        <div class="item">
                            <img src="<?php echo base_url(); ?>assets/img/award/chirangjilaldhanuka.jpg" alt="" />
                            <div class="insider-div pl-0">
                                <h4 class="f-contact pl-0">Chiranjilal Dhanuka Smriti Samaj Seva Award</h4>
                                <p>Lions Club New Delhi Alakhnanda awarded Shivganga Samagra Gramvikas Parishad the most prestigious award ‘Chiranjilal Dhanuka Smriti Samaj Seva Award' under Major Ngo category for year 2020 his remarkable contribution in the field of rural development and tribal upliftment.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url(); ?>assets/img/award/watermission.jpg" alt="" />
                            <div class="insider-div pl-0">
                                <h4 class="f-contact pl-0">National Water Mission Award</h4>
                                <p>Shivganga was being awarded by <b>National Water Mission Award 2019</b> by Ministry of Jal Shakti India in category of Focused attention to vulnerable areas including over-exploited areas. Jal Shakti Minister Shree Gajendra Shekhawat Ji awarded this to Shree Mahesh Sharma ji and Shree Rajaram Katara ji of Shivganga.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?php echo base_url(); ?>assets/img/award/antoday.jpg" alt="" />
                            <div class="insider-div pl-0">
                                <h4 class="f-contact pl-0">The Vision of Antodaya</h4>
                                <p>In year 19-20 Shivganga’s approach towards holistic and sustainable village development got a place in the book <b>“The Vision for Antodaya”</b> which was inagratued by Hanourable Vice-President of India, Shree Vainkaya Naidu Ji. This book is on 408 best prectices of India which work on ground level and have substantial changes in society. Shree Mahesh Sharmaji was presented in inaguration ceremony held at President House.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="container-fluid pt-5" style="background: #b5efd4;">
    <div class="text-center">
        <h4 class="our-purpose">Our Social Ventures </h4>
    </div>
    <div style="color:white; ">
        <div class="col-md-12">
            <div class="row py-5">
                <div class="col-md-4 col-12 text-center align-self-center">
                    <div class="fake-div">
                    </div>
                    <a href="https://prebook.jhabuacrafts.com/" style="text-decoration:none;color:white">
                        <img src="<?php echo base_url() ?>assets/img/Jhabua Craft Logo.png" style="width:80%;height:80%">
                    </a>


                </div>
                <div class="col-md-4 col-12 text-center align-self-center">
                    <div id="counter">
                        <a href="https://jhabua.nic.in/en/" style="text-decoration:none;color:white">
                            <img src="<?php echo base_url() ?>assets/img/tour.png" style="width:80%;height:80%">
                        </a>

                    </div>

                </div>
                <div class=" col-md-4 col-12 text-center align-self-center">
                    <div id="counter">

                        <a href="https://play.google.com/store/apps/details?id=com.param.jhabuanaturals&hl=en_IN&gl=US" style="text-decoration:none;color:white">
                            <img src="<?php echo base_url() ?>assets/img/logo-final.png" style="width:80%;height:80%">
                        </a>
                    </div>

                </div>

                <div class=" fake-div">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.owl-one').owlCarousel({
            margin: 20,
            autoWidth: false,
            nav: true,
            autoplay: true,
            autoPlayTimeout: 2000,
            autoplaySpeed: 2500,
            smartSpeed: 2500,
            autoplayHoverPause: true,
            loop: true,
            items: 4,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                }
            },
            navText: ['<i class="fa fa-angle-left"  style =" color:white; "aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" style =" color:white;" aria-hidden="true"></i>'
            ]
        });

        $('.owl-four').owlCarousel({
            margin: 20,
            autoWidth: false,
            nav: true,
            autoplay: true,
            autoPlayTimeout: 2000,
            autoplaySpeed: 2500,
            smartSpeed: 2500,
            autoplayHoverPause: true,
            loop: true,
            items: 3,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                }
            },
            navText: ['<i class="fa fa-angle-left"  style =" color:white; "aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" style =" color:white;" aria-hidden="true"></i>'
            ]
        });

        $('.owl-one1').owlCarousel({
            margin: 20,
            autoWidth: false,
            nav: true,
            loop: true,
            autoplay: true,
            autoPlaySpeed: 2000,
            autoPlayTimeout: 2000,
            autoplayHoverPause: true,
            dots: false,
            items: 1,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                }
            },
            navText: ['<i class="fa fa-angle-left"  style =" color:white; "aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" style =" color:white;" aria-hidden="true"></i>'
            ]
        });

        $('.owl-two2').owlCarousel({
            margin: 20,
            autoWidth: false,
            autoplay: true,
            autoPlaySpeed: 2000,
            autoPlayTimeout: 2000,
            autoplayHoverPause: true,
            nav: true,
            loop: true,
            items: 4,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
            },
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ]
        });

        $('.owl-two8').owlCarousel({
            margin: 20,
            autoWidth: false,
            autoplay: true,
            autoPlaySpeed: 2000,
            autoPlayTimeout: 2000,
            autoplayHoverPause: true,
            nav: true,
            loop: true,
            items: 4,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                }
            },
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ]
        });

        $('.owl-two').owlCarousel({
            margin: 20,
            autoWidth: false,
            autoplay: true,
            autoPlaySpeed: 2000,
            autoPlayTimeout: 2000,
            autoplayHoverPause: true,
            nav: true,
            loop: true,
            dots: false,
            items: 4,
            navText: [
                '<i class="fa fa-angle-left" style =" color:white; border-radius:0px" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" style =" color:white; border-radius:0px" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 1,
                    marginLeft: 20,
                    marginRight: 0,
                },
                768: {
                    items: 3
                },
                912: {
                    items: 4
                }
            }
        });

        $('#Owl-Partner').owlCarousel({
            margin: 20,
            autoWidth: false,
            autoplay: true,
            autoPlaySpeed: 2000,
            autoPlayTimeout: 2000,
            autoplayHoverPause: true,
            nav: true,
            loop: true,
            dots: false,
            items: 4,
            navText: [
                '<i class="fa fa-angle-left" style =" color:white; border-radius:0px" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" style =" color:white; border-radius:0px" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 1,
                    marginLeft: 20,
                    marginRight: 0,
                }
            }

        });
        $('.owl-intern').owlCarousel({
            margin: 20,
            autoWidth: false,
            autoplay: true,
            autoPlaySpeed: 2000,
            autoPlayTimeout: 2000,
            autoplayHoverPause: true,
            nav: true,
            loop: true,
            dots: false,
            items: 4,
            navText: [
                '<i class="fa fa-angle-left" style =" color:white; border-radius:0px" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" style =" color:white; border-radius:0px" aria-hidden="true"></i>'
            ],
            responsive: {
                0: {
                    items: 1,
                    marginLeft: 20,
                    marginRight: 0,
                },
                768: {
                    items: 3
                },
                912: {
                    items: 4
                }
            }
        });
        $('.owl-three').owlCarousel({
            margin: 20,
            autoWidth: false,
            autoplay: true,
            autoPlaySpeed: 2000,
            autoPlayTimeout: 2000,
            autoplayHoverPause: true,
            nav: true,
            loop: true,
            items: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ]

        });
    });
</script>
<script>
    $(function() {
        function isScrolledIntoView($elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();
            var elemTop = $elem.offset().top;
            var elemBottom = elemTop + $elem.height();
            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
        }

        function count($this) {
            var current = parseInt($this.html(), 10);
            if (isScrolledIntoView($this) && !$this.data("isCounting") && current < $this.data('count')) {
                $this.html(++current);
                $this.data("isCounting", true);
                setTimeout(function() {
                    $this.data("isCounting", false);
                    count($this);
                }, 10);
            }
        }

        $(".c-section4").each(function() {
            $(this).data('count', parseInt($(this).html(), 10));
            $(this).html('0');
            $(this).data("isCounting", false);
        });

        function startCount() {
            $(".c-section4").each(function() {
                count($(this));
            });
        };

        $(window).scroll(function() {
            startCount();
        });

        startCount();
    });
</script>
<script>
    var a = 0;
    $(window).scroll(function() {
        var oTop = $('#counter').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
            $('.counter-value').each(function() {
                var $this = $(this),
                    countTo = $this.attr('data-count');
                $({
                    countNum: $this.text()
                }).animate({
                        countNum: countTo
                    },

                    {

                        duration: 2000,
                        easing: 'swing',
                        step: function() {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function() {
                            $this.text(this.countNum);
                            //alert('finished');
                        }

                    });
            });
            a = 1;
        }

    });
</script>