<?php
// if(empty($_SESSION['user_name'])) {
// }else{
// 	redirect('login');

// }
?>
<main>

	<!-- breadcrumb-area -->
	<section class="breadcrumb__wrap">
		<div class="container custom-container">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-md-10">
					<div class="breadcrumb__wrap__content">
						<h2 class="title">Contact us</h2>

					</div>
				</div>
			</div>
		</div>
		<div class="breadcrumb__wrap__icon">
			<ul>
				<li><img src="assets/img/icons/breadcrumb_icon01.png" alt=""></li>
				<li><img src="assets/img/icons/breadcrumb_icon02.png" alt=""></li>
				<li><img src="assets/img/icons/breadcrumb_icon03.png" alt=""></li>
				<li><img src="assets/img/icons/breadcrumb_icon04.png" alt=""></li>
				<li><img src="assets/img/icons/breadcrumb_icon05.png" alt=""></li>
				<li><img src="assets/img/icons/breadcrumb_icon06.png" alt=""></li>
			</ul>
		</div>
	</section>
	<!-- breadcrumb-area-end -->

	<!-- contact-map -->
	<div id="contact-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7359.371681944395!2d74.5976283348877!3d22.739915400000008!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3961734ed2bcacf3%3A0x6bfe3912b13d7f15!2sShivganga%20Gurukul%20Dharmpuri!5e0!3m2!1sen!2sin!4v1652973528620!5m2!1sen!2sin" allowfullscreen="" loading="lazy"></iframe>
	</div>
	<!-- contact-map-end -->

	<!-- contact-area -->
	<div class="contact-area">
		<div class="container">
			<form id="contact" class="contact__form">
				<div class="row">
					<div class="col-md-6">
						<input type="text" name="name" placeholder="Enter your name*">
					</div>
					<div class="col-md-6">
						<input type="email" name="email" placeholder="Enter your mail*">
					</div>
					<div class="col-md-6">
						<select name="subject" id="">
							<option value="">Select your subject*</option>
							<option value="Recurring Donation">Recurring Donation</option>
							<option value="New Donation">New Donation</option>
							<option value="Donation Reciept">Donation Reciept</option>
							<option value="Visit Jhabua">Visit Jhabua</option>
							<option value="Visit Jhabua">Other</option>
						</select>
					</div>
					<div class="col-md-6">
						<input type="text" name="contact" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" placeholder="Your Contact Number">
					</div>
				</div>
				<textarea name="message" id="message" placeholder="Enter your massage*"></textarea>
				<button type="submit" class="btn">send massage</button>
			</form>
		</div>
	</div>
	<!-- contact-area-end -->

	<!-- contact-info-area -->
	<section class="contact-info-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-4 col-md-6">
					<div class="contact__info">
						<div class="contact__info__icon">
							<img src="assets/img/icons/contact_icon01.png" alt="">
						</div>
						<div class="contact__info__content">
							<!-- <h4 class="title">Address 1</h4> -->
							<span>Indore Office :<br />
								Shivganga Samagra Gramvikas Parishad
								50 A Lokmanya Nagar Extension,<br />
								Indore – 452009<br />
								Ph: 9406922130</span>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="contact__info">
						<div class="contact__info__icon">
							<img src="assets/img/icons/contact_icon02.png" alt="">
						</div>
						<div class="contact__info__content">
							<!-- <h4 class="title">Address 2</h4> -->
							<span>Dharampuri Gurukul :<br />
								Shivganga Samagra Gramvikas Parishad
								Gram – Dharampuri, Gram Panchayat- Charoli Pada<br />
								Dist – Jhabua – 4576611<br />
								Ph: 9588296068</span>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="contact__info">
						<div class="contact__info__icon">
							<img src="assets/img/icons/contact_icon03.png" alt="">
						</div>
						<div class="contact__info__content">
							<h4 class="title">Mail Address</h4>
							<span><a href="mailto:Contact@shivgangajhabua.org">Contact@shivgangajhabua.org</a></span>
							<span>Contact Mail</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- contact-info-area-end -->


</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
	$(document).ready(function(e) {
		$("#contact").on('submit', (function(e) {
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url('home_Controller/save_join_us_data') ?>",
				type: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				beforeSend: function() {
					$("#err").fadeOut();
				},
				success: function(data) {
					if (data == 'invalid') {
						$("#err").html("Invalid File !").fadeIn();
					} else {
						alert('Recent Project Added Successfully');
						$("#preview").html(data).fadeIn();
						$("form")[0].reset();
					}
				},
				error: function(e) {
					$("#err").html(e).fadeIn();
				}
			});
		}));
	});
</script>