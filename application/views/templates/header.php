<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>shivganga</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/x-icon">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
    <script src="<?php echo base_url(); ?>assets/js/videojs-ie8.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/video-js.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/shivganga.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/slick.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-new.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/iconmoon.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
    <!-- dcd-->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> -->
    <!-- <script src="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css"></script> -->
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <style>




    </style>
</head>


<body style="background-color:white">
    <!-- preloader-start -->
    <div id="preloader">
        <div class="rasalina-spin-box"></div>
    </div>
    <!-- preloader-end -->

    <!-- Scroll-top -->
    <button class="scroll-top scroll-to-target" data-target="html">
        <i class="fas fa-angle-up"></i>
    </button>
    <!-- Scroll-top-end-->
    <!-- header-area -->
    <header>
        <div id="sticky-header" class="menu__area">
            <div class="container custom-container">
                <div class="row">
                    <div class="col-12">
                        <div class="mobile__nav__toggler"><i class="fas fa-bars"></i></div>
                        <div class="menu__wrap">
                            <nav class="menu__nav">
                                <div class="logo">
                                    <a href="<?php echo base_url(); ?>" class="logo__black"><img src="<?php echo base_url(); ?>assets/img/sj.png" alt=""></a>
                                    <a href="<?php echo base_url(); ?>" class="logo__white"><img src="<?php echo base_url(); ?>assets/img/sj.png" alt=""></a>
                                </div>
                                <div class="navbar__wrap main__menu d-none d-xl-flex">
                                    <ul class="navigation">
                                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                        <li class="menu-item-has-children"><a href="#">About</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo base_url(); ?>aboutshivganga" onclick="active()">About Shivganga</a></li>
                                                <li><a href="<?php echo base_url(); ?>aboutjhabua">About Jhabua</a>
                                                <li>
                                                <li><a href="<?php echo base_url(); ?>award">Awards and Recognition</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Programs</a>
                                            <ul class="sub-menu">
                                                <li><a href="<?php echo base_url(); ?>waterconservation" onclick="active()">WATER
                                                        CONSERVATION</a></li>
                                                <li><a onclick="active()" href="<?php echo base_url(); ?>programforest">Forests Enrichment </a></li>
                                                <li><a onclick="active()" href="<?php echo base_url(); ?>youth_Empower">Community Empowerment
                                                    </a></li>
                                                <li><a onclick="active()" href="<?php echo base_url(); ?>alternate_edu_skill_devep">Entrepreneurship & Livelihood generation
                                                    </a></li>
                                                <!-- <li><a  onclick="active()"
                                                    href="<?php echo base_url(); ?>levelihood_entrepreneurship">LIVELIHOOD &
                                                    ENTREPRENEURSHIP</a></li> -->
                                                <li><a onclick="active()" href="<?php echo base_url(); ?>women_empowerment">WOMEN EMPOWERMENT</a></li>
                                                <li><a onclick="active()" href="<?php echo base_url(); ?>connecting_communities">CONNECTING
                                                        COMMUNITIES</a></li>
                                                <li><a onclick="active()" href="<?php echo base_url(); ?>tribal_culture">TRIBAL CULTURE</a></li>
                                                <!-- <li><a  onclick="active()"
                                                    href="<?php echo base_url(); ?>image_vs_reality_internship">IMAGE VS REALITY INTENSHIP</a></li> -->
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="https://psr.shivgangajhabua.org/">Recurring Donation</a>
                                        </li>
                                        <li>
                                            <!-- <a href="<?php echo base_url(); ?>projectsaadhtalab">Projects</a> -->
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>blogs">Blogs</a>
                                        </li>
                                        <!-- <li>
                                                <a href="<?php echo base_url(); ?>events">Events</a>
                                            </li> -->
                                        <li>
                                            <a href="<?php echo base_url(); ?>joinus">Contact us</a>
                                        </li>
                                        <li><a href="<?php echo base_url(); ?>student_involve">GET INVOLVED</a></li>
                                    </ul>
                                </div>
                                <div class="header__btn d-none d-md-block">
                                    <a href="<?php echo base_url(); ?>donate" class="btn">DONATE</a>
                                </div>
                            </nav>
                        </div>
                        <!-- Mobile Menu  -->
                        <div class="mobile__menu">
                            <nav class="menu__box">
                                <div class="close__btn"><i class="fal fa-times"></i></div>
                                <div class="nav-logo">
                                    <a href="index.html" class="logo__black"><img src="assets/img/logo/logo_black.png" alt=""></a>
                                    <a href="index.html" class="logo__white"><img src="assets/img/logo/logo_white.png" alt=""></a>
                                </div>
                                <div class="menu__outer">
                                    <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
                                </div>
                                <div class="social-links">
                                    <ul class="clearfix">
                                        <li><a href="https://twitter.com/ShivgangaJ"><span class="fab fa-twitter"></span></a></li>
                                        <li><a href="https://www.facebook.com/jhabuashivganga"><span class="fab fa-facebook-square"></span></a></li>
                                        <li><a href="https://www.linkedin.com/company/shivgangajhabua/"><span class="fab fa-pinterest-p"></span></a></li>
                                        <li><a href="https://www.instagram.com/shivganga_jhabua"><span class="fab fa-instagram"></span></a></li>
                                        <li><a href="https://www.youtube.com/channel/UCMRaT0-qWkhenPdo0LfdCmQ"><span class="fab fa-youtube"></span></a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="menu__backdrop"></div>
                        <!-- End Mobile Menu -->
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- header-area-end -->
    <main>
        <!-- <div class="header mb-2">
        <nav class="navbar navbar-expand-lg navbar-light pt-2 pb-0">
           
            
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav mr-auto">
                    
                    
                    <li class="nav-item dropdown">

                        <a class="nav-link dropdown-toggle no-sign new" href="#" id="navbardrop" data-toggle="dropdown">
                            Programs
                        </a>

                        <div class="dropdown-menu mt-0">
                            
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://psr.shivgangajhabua.org/">Recurring Donation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>projectsaadhtalab">Projects</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>blogs">Blogs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>events">Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>joinus">Contact us</a>
                    </li>
                </ul>
                
            </div>
        </nav>
    </div> -->
        <script>
            var activeurl = window.location;
            console.log(activeurl)
            $('a[href="' + activeurl + '"]').parents('li').addClass('active');

            if (activeurl == "<?php echo base_url(); ?>") {
                console.log("<?php echo base_url(); ?>")
                $('a[href="' + activeurl + 'home"]').parents('li').addClass('active');

            }
        </script>

        <!-- navigation close -->