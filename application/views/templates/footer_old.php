<div class="bg-blue">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 hight-50"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h1 class="fl-shiv">Shivganga</h1>
                <h2 class="fl-vikas">Vikas ka Jatan</h2>
            </div>
            <div class="col-md-4">
                <p class="fmt-shiv">
                    Shivganga Samagra Gramviaks Parishad (SSGP) is a group of Social Entrepreneurs committed to
                    restore the ecosystem of sustainable living through holistic village development.
                </p>
                <br />
                <p class="fmb-shiv">
                    Shivganga is a registered 501c3 charitable organization Federal Tax ID #27-1319189.
                </p>
            </div>
            <div class="col-md-4">
                <h4 class="f-contact">CONTACT US</h4>
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-envelope-o"></i>contact@interiittech.org</li>
                    <li><i class="fa-li fa fa-phone"></i>+92 75747 34020</li>
                    <li><i class="fa-li fa fa-map-marker"></i>IIT Roorkee - Haridwar Highway, Roorkee, Uttarakhand
                        247667</li>
                </ul>
            </div>
        </div>
        <div class="row pt-5 pb-3">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <p class="f-policies">
                    <a href="#">Privacy Policy</a>&emsp;|&emsp;
                    <a href="#">Terms of use </a>&emsp;|&emsp;
                    <a href="#">Refund Policy</a>
                </p>
            </div>
            <div class="col-md-4">
                <P class="f-policies">
                    <a href="https://www.facebook.com/jhabuashivganga">
                        <img src="<?php echo base_url(); ?>assets/img/fb-logo.svg" />
                    </a>&emsp;
                    <a href="https://www.instagram.com/shivganga_jhabua/">
                        <img src="<?php echo base_url(); ?>assets/img/insta-logo.svg" />
                    </a>&emsp;
                    <a href="https://twitter.com/ShivgangaJ">
                        <img src="<?php echo base_url(); ?>assets/img/twitter.svg" />
                    </a>&emsp;
                    <a href="https://www.linkedin.com/company/shivgangajhabua/">
                        <img src="<?php echo base_url(); ?>assets/img/linkedin.svg" />
                    </a>&emsp;
                    <a href="https://www.youtube.com/channel/UCMRaT0-qWkhenPdo0LfdCmQ">
                        <img src="<?php echo base_url(); ?>assets/img/yt-logo.svg" />
                    </a>
                </P>
            </div>
        </div>
    </div>
</div>
</body>

</html>