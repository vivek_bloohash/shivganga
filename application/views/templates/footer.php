            <div class="bg-blue">
                <div class="container">
                    <div class="row">
                        <div class="col-12 hight-50"></div>
                    </div>
                    <div style="text-align:center">
                        <h3 class="fl-shiv mb-0">Shivganga</h3>
                        <h3 class="fl-vikas">Vikas ka Jatan</h3>
                    </div>
                    <div style="text-align:center; padding-top:12px">
                        <p class="f-policies">
                            <a href="#">Privacy Policy</a>&emsp;|&emsp;
                            <a href="#">Terms of use </a>&emsp;|&emsp;
                            <a href="#">Refund Policy</a>
                        </p>
                    </div>
                    <div style="text-align:center; padding-top: 20px; margin-left:10px">
                            <P class="f-policies">
                                <a href="https://www.facebook.com/jhabuashivganga">
                                    <img src="<?php echo base_url(); ?>assets/img/fb-logo.svg" />
                                </a>&emsp;
                                <a href="https://www.instagram.com/shivganga_jhabua">
                                    <img src="<?php echo base_url(); ?>assets/img/insta-logo.svg" />
                                </a>&emsp;
                                <a href="https://twitter.com/ShivgangaJ">
                                    <img src="<?php echo base_url(); ?>assets/img/twitter.svg" />
                                </a>&emsp;
                                <a href="https://www.linkedin.com/company/shivgangajhabua/">
                                    <img src="<?php echo base_url(); ?>assets/img/linkedin.svg" />
                                </a>&emsp;
                                <a href="https://www.youtube.com/channel/UCMRaT0-qWkhenPdo0LfdCmQ">
                                    <img src="<?php echo base_url(); ?>assets/img/yt-logo.svg" />
                                </a>
                            </P>
                            <p class="fmb-shiv mb-0 pb-3" style="padding-top: 20px;">
                            Shivganga is registered under Madhya Pradesh Society Registration Adiniyam 1973
                            <br/>
                            Registration number – 03/27/03/10295/07                   
                            <br>
                            IT Exemption U/S 80G, <br>F.No. CIT-I/Ind/Tech/80G/08/09-10, Dated 24/12/2009, Sl.No. 51/2009-10<br> Valid from 01/04/09, Extended from AY 2021-22 to AY 2023-24 with <br>Provisional Approval Number: AADAS4039HF20206.
                            <br>PAN : AADAS4039H
              </p>
                        </div>
                    </div>    
                </div>         
            </div>
        </main>
        <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/element-in-view.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/slick.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ajax-form.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
    </body>
</html>