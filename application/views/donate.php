<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }

?>

<div class="bg-f8">
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-10 col-xl-9 offset-xl-3 offset-md-2">
                <p class="what-we-do">HELP ALONG THE WAY</p>
                <h4 class="our-purpose">Make your contribution</h4>
            </div>
        </div>

        <form>
            <div class="row pt-5">
                <div class="col-md-8 col-xl-6 offset-xl-3 offset-md-2">
                    <h5 class="we-make">Donate for the cause of</h5>

                    <?php

                    if (!$title == '') { ?>
                        <div class="form-group">
                            <?php $str = preg_replace('/20/', '', $title); ?>
                            <h5><?php $str2 = preg_replace('/%/', ' ', $str);
                                echo  $str2;
                                ?> </h5>


                        </div>
                    <?php } else { ?>
                        <div class="form-group">
                            <select class="form-control-plaintext donate-select" onchange="showUser(this.value)" id="sel1" required>
                                <option value="">Select</option>
                                <?php
                                foreach ($showdonateData as $donate) { ?>
                                    <option value="<?php echo $donate->id ?>"><?php echo $donate->title ?></option>

                                <?php  } ?>
                                <option value="10">Others</option>
                            </select>
                        </div>
                    <?php } ?>
                    <p class="event-add">100% of your money will be used for the betterment of tribals</p>
                </div>
                <div class="col-md-10 col-xl-7 offset-xl-3 pt-3 offset-md-2">
                    <!-- pick donation -->
                    <div class="amount-select row" id="amount-select" role="radiogroup">

                        <div class="container">
                            <div id="showprice" class="row">
                            </div>
                        </div>

                    </div>

                    <!-- pick donation end -->
                </div>
                <div class="col-md-8 col-xl-6 offset-xl-3 pt-3 offset-md-2">
                    <p class="event-add mb-0">Donation Occurance</p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-check-inline">
                                <label for="otd1" class="custom-radio">
                                    <input type="radio" checked="" name="otdradio" id="otd1" value="OneTime" class="hidden" />
                                    <span class="custom-label"></span><b>One TIme Donation</b>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check-inline">
                                <label for="otd2" class="custom-radio">
                                    <input type="radio" name="otdradio" id="otd2" value="RecurringDonation" class="hidden" />
                                    <span class="custom-label"></span><b>Recurring Donation</b>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3 offset-md-2">
                    <h5 class="we-make pt-5">Donation Information</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-check-inline">
                                        <label for="optr1" class="custom-radio mb-0">
                                            <input type="radio" name="optradio" id="optr1" value="personal" class="hidden" />
                                            <span class="custom-label"></span><b>Personal</b>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-check-inline">
                                        <label for="optr2" class="custom-radio mb-0">
                                            <input type="radio" name="optradio" id="optr2" value="business" class="hidden" />
                                            <span class="custom-label"></span><b>Business</b>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-check-inline">
                                        <label for="optr3" class="custom-radio mb-0">
                                            <input type="radio" name="optradio" id="optr3" value="inMemor" class="hidden" />
                                            <span class="custom-label"></span><b>In Memory/Honour of</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 pt-3">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-check-inline">
                                        <label for="optr4" class="custom-radio mt-0">
                                            <input type="radio" name="optradio" id="optr4" value="family" class="hidden" />
                                            <span class="custom-label"></span><b>Family</b>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-check-inline">
                                        <label for="optr5" class="custom-radio mt-0">
                                            <input type="radio" name="optradio" id="optr5" value="anonymous" class="hidden" />
                                            <span class="custom-label"></span><b>Anonymous</b>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-check-inline">
                                        <label for="optr6" class="custom-radio mt-0">
                                            <input type="radio" name="optradio" id="optr6" value="inCelebration" class="hidden" />
                                            <span class="custom-label"></span><b>In Celebration of</b>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xl-6 offset-xl-3 offset-md-2">
                    <div class="form-group pt-3 mb-4">
                        <input type="text" class="form-control custom-form" name="fname" placeholder=" Name" id="fname" required>
                    </div>
                    <div class="form-group mb-4">
                        <input type="text" class="form-control custom-form" name="contact" placeholder="Contact" id="contact" required>
                    </div>
                    <div class="form-group mb-4">
                        <input type="text" class="form-control custom-form" name="address" placeholder="Address" id="address" required>
                    </div>
                    <div class="form-row mb-4">
                        <div class="col">
                            <input type="text" class="form-control custom-form" name="city" placeholder="City" id="city" required>
                        </div>
                        <div class="col">
                            <input type="text" class="form-control custom-form" name="state" placeholder="State" id="state" required>
                        </div>
                        <div class="col">
                            <input type="number" class="form-control custom-form" name="pincode" placeholder="Pin Code" id="pincode" required>
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <input type="email" class="form-control custom-form" name="email" placeholder="Email" id="email" required>
                    </div>
                    <div class="form-group mb-4">
                        <input type="test" class="form-control custom-form" name="pan" placeholder="pan" id="pan" required>
                    </div>
                    <p class="event-add">You will recieve periodic reports of the ongoing programs and initiatives
                        through emails</p>
                    <h5 class="we-make pt-5">Choose your mode of payment</h5>
                    <div class="row">
                        <div class="col-md-12 pt-2">
                            <div class="event-add">
                                <label for="opt1" class="custom-radio mb-2">
                                    <input type="radio" checked="" name="modepayradio" value="online" id="opt1" class="hidden" required />
                                    <span class="custom-label"></span><b>Online (UPI/Wallet/Net banking)</b>
                                </label>
                            </div>
                            <div class="event-add">
                                <label for="opt2" class="custom-radio">
                                    <input type="radio" name="modepayradio" id="opt2" value="imps/neft" class="hidden" required />
                                    <span class="custom-label"></span><b>Direct Transfer (IMPS/NEFT)</b>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p class="mb-0 the-primary pt-3"><b>Terms and conditions*</b></p>
                            <p class="the-primary pt-2">Your personal data will be used to process your donation,
                                support your
                                experience
                                throughout this website, and for other purposes described in our Privacy Policy.</p>
                            <div class="form-check event-add pb-5">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" value="">
                                    <b>Yes, I am happy for you to contact me via email or phone.</b>
                                </label>
                            </div>
                            <div class="text-left pb-5 col-md-6 pl-0">
                                <input id="#button" class="main_btn-donate" value="Donate Now" type="submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(".other-amount-input").focus(function() {
        $(this).siblings(":radio").prop("checked", true);
    });
    $(".other-amount-input").focusout(function() {
        var input_val = $(this).val();
        $("#id-amount-other").attr('value', input_val);
        var radio_val = $("input[name^='amount']:checked").val();
        input_val == radio_val;
    });
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var SITEURL = "<?php echo base_url() ?>";

    $('form').on('submit', function(e) {

        var Amount = $("input[name^='amount']:checked").val();
        var eamount = $('#other-amount-input').val();
        var otherAmount = $('#other-amount').val();
        var fname = $('#fname').val();
        var contact = $('#contact').val();
        var address = $('#address').val();
        var city = $('#city').val();
        var state = $('#state').val();
        var pincode = $('#pincode').val();
        var email = $('#email').val();
        var pan = $('#pan').val();

        var selectedDonationCause = $("select.donate-select").children("option:selected").val();
        var donationOccurance = $("input[name^='otdradio']:checked").val();
        var donationInformation = $("input[name^='optradio']:checked").val();
        var modeofpayment = $("input[name^='modepayradio']:checked").val();
        var formcheck = $("input[name^='form-check-input']:checked").val();

        if (Amount) {

            var totalAmount = +Amount + +eamount;

        } else if (eamount) {

            var totalAmount = eamount;

        } else {

            alert("Please Pick Up a Donation Amount!")
        }

        if (totalAmount) {
            var options = {
                "key": "rzp_test_bWP9x7z4ecBQq9",
                "amount": (totalAmount * 100), // 2000 paise = INR 20
                "name": "shivganga",
                prefill: {
                    "name": fname,
                    "email": email,
                    "contact": contact,
                },
                "description": "Payment",
                "image": "http://localhost/shivganga/assets/img/shiv_ganga-logo.png",
                "handler": function(response) {
                    $.ajax({
                        url: SITEURL + 'payment/razorPaySuccess',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            razorpay_payment_id: response.razorpay_payment_id,
                            totalAmount: totalAmount,
                            fname: fname,
                            contact: contact,
                            email: email,
                            pan: pan,
                            address: address,
                            city: city,
                            state: state,
                            pincode: pincode,
                            selectedDonationCause: selectedDonationCause,
                            donationOccurance: donationOccurance,
                        },
                        success: function(response) {

                            // alert(response)
                            window.location.href = SITEURL + 'payment/RazorThankYou';
                        }
                    });

                },
                "theme": {
                    "color": "#528FF0"
                }
            };
        }
        // alert(options)
        var rzp1 = new Razorpay(options);
        rzp1.open();
        e.preventDefault();
    });
</script>

<script>
    function showUser(str) {
        if (str == "") {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("showprice").innerHTML = this.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url('home_Controller/showprice') ?>?q=" + str, true);
        xmlhttp.send();
    }
</script>