<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
// print_r($showblogData);die;
?>
<div class="container">
   <h4 class="blog-head">Latest Updates
   </h4>
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img t1">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>FELLOW </p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">AVINASH MATTUR</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Sometime in July 2020, almost a year back, one would find a guy roaming around villages in Jhabua, trying to interact with villagers in broken Hindi and a Kannada accent. As we say, words are not a necessary communication element; emotions are – the language didn’t deter Avinash. He spent months living in the village, learning both tribal culture and the Bhili tongue. </p>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     Avinash Mattur, a resident of Raichur, Karnataka, is a graduate of University Law College, Bengaluru. He first visited Jhabua during Halma 2020 – in March. In July, just after completing his Masters from Amity University, Avinash made his way to Jhabua, driven by his urge to work with the tribal community. It didn’t take him long to connect with people, so he began with visiting the villages of Jhabua and spending days & weeks there. From his stay, he gained a significant on-ground observation on the economic condition, the vicious cycle of debt, resultant migration, and its adverse impact on the community’s culture and traditions. At the same time, he also experienced the life values, the natural honesty and the Parmarthi nature of the Bhils of Jhabua.

                  </p>
               </div>
            </div>
         </div>
         <div class="col-md-12">
            <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Shivaganga had already initiated the issue of Community Forest’s Rights in Jhabua. It was natural for him to engage in this initiative, and hence, Avinash came forward to lead the progressive unfoldment. In September 2020, assisted by on-ground volunteers of Shivganga, he started holding small awareness meetings in villages. The villagers would go in length in memories to describe how the forest has been an integral part of their ancestors’ lives. Consequently, at a threshold of awareness, Shivganga began organizing workshops under the guidance of experts in the field. Enthusiasm is contagious. Avinash’s enthusiasm met with Vanvasi’s spirit of selflessly working for society. The small informal meetings turned into village-level awareness training camps. The training camps embarked on a remarkable moment for the tribal community. It would be the first instance where the traditional tribal knowledge of forests, flora & fauna, the boundaries of villages were getting documented, as it is. The villages – all elders, women, children – would sit together and draw the map of their village, marking worship places, old trees, water bodies, grazing land and pathways. With Avinash as the bridge to communicate in the formal language, five villages formulated their ‘Vanadhikar Samiti’. In Bawadibadi village, villagers were able to take land from the forest department to construct a reservoir through the formal channel of papers. It was a momentous step for villagers that streamlined never-before confidence that they can take their rights through the formidable tunnel of documents. It was a moment of satisfaction for Avinash as well. A joy that would travel along with him, strengthening his conviction to work for the people.
            </p>
            <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
               Today, 25 villages have initiated the process for Forest Rights. A significant change in the villages now is that they can ask questions to the administration through the legal path, something they wouldn’t have thought of earlier. They know they have a resource person, a friend in the form of Avinash Mattur.
            </p>
         </div>
      </div>
   </div>

   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img t2">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>SOCIAL LEADER </p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">VIJENDRA AMALIYAR</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">A young tribal student, working hard on his studies to better the economic condition of his family, starts feeling that this is not enough. This is not the way he can contribute to society. He begins seeking a path through which he can bring change to all the lives of Jhabua, not just his family. Then one day, he meets Shri Mahesh Sharma and through him gets connected to Shivganga. Growing through the experience and learnings of working with people, he stands to become a Social Leader with a vision and confidence to bring a sustainable change in Jhabua. The student is Shri Vijendra Amaliyar of Palasadi village of Jhabua. </p>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     Shri Vijendra Amaliyar began attending the camps of Shivganga when he was 14. At that curious age, attending Shivganga's Youth Empowerment Camp made him feel that it was where his village's actual pain and needs were being discussed. It was where he could discover an impactful path. Hence, he kept coming to various programs of Shivganga like Kanwad Yatra, Ganeshotsav and Empowerment Camps. Shivganga's programs aim to spark thought in youths of Jhabua and identify those who want to work selflessly & passionately for society.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-md-12">
            <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
               Such programs strengthened the vision of young Vijendra and his concept of working together to resolve the pain of Jhabua's tribal community. In 2012, at the age of 21, he became a full-time volunteer of Shivganga and shifted to Dharampuri Gurukul of Shivganga. There, he led Pashu Samvardhan - a dimension aimed at making cattle a significant contributor to the livelihood of tribal farmers. He also took advanced training to become a Gram Engineer. As a Gram Engineer, he took mega projects in hand, including the construction of three reservoirs - Saadh (720 million litres), Navapada (450 million litres) and Surinala (36 million litres). He has also supervised two Matavan projects - Bochaka (1000 trees) and Saadh (3000 trees). In 2018, Vijendra began to lead the organic farming entrepreneurial venture of Shivganga - Jhabua Naturals. A venture where tribal farmers came together with the determination to make mother earth poison-free and provide fresh veggies to the urban populace. Three years down the line, the farmers have formed an FPO under the title 'Jhabuanchal Krishak Farmers Producers Company'.

            </p>
            <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">A master of many trades, Vijendra is also leading the metal & electricity skill development workshop of Shivganga. Here he designed a flour mill that doesn’t burn the fibres of the flours & pulses, making them more nutritious - very much like desi hand-driven Chakki. It can be used to make various gradients of flours and pulses. While he continues making improvements, three villagers have set up the machine in their house and have sold quintals of pulses already. Thus, Vijendra’s innovation is fostering entrepreneurship in the villages.
            </p>
            <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
               Along with other volunteers of Shivganga, Vijendra has played a significant role in pulling the rural Jhabua out of the Covid-19 pandemic.
               At 27, almost with a journey of 13 years, today Vijendra Amliyar is a master trainer, a social entrepreneur and a social leader leading Jhabua in Holistic Rural Development.
            </p>
         </div>
      </div>
   </div>
</div>
<div class="bg-f8">
   <div class="container">
      <h4 class="blog-head">Blogs
      </h4>
      <div class="row pt-5">
         <?php foreach ($showblogData as $blogdata) {
         ?>
            <div class="col-md-4">
               <div class="issues-single-items">
                  <div class="issue-img" style="background-image: url('<?php echo base_url(); ?><?php echo $blogdata->filename ?>');">
                     <div class="content">
                        <h4 class="title"><a href="<?php echo base_url('home_Controller/blogfullview/') ?><?php echo $blogdata->slug ?>"><?php echo $blogdata->title ?></a></h4>
                        <p><?php echo $blogdata->created_at; ?></p>
                        <p><?php echo substr_replace($blogdata->short_desc, "...", 180); ?></p>
                        <a href="<?php echo base_url('home_Controller/blogfullview/') ?><?php echo $blogdata->slug ?>">
                           <h6 style="color: #fff;">Read More...</h6>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         <?php } ?>
      </div>
   </div>
</div>
<div class="container">
   <h4 class="blog-head">Newsletter
   </h4>
   <p>In this negative time, we bring some positive news from Jhabua. From Youth & Women Empowerment camps to Plantation Training Camps to plantation of 1,25,000 plants to constructing 84 earthen dams, the Shivganga is working on every dimension of sustainable rural development. And all this was not possible without your contribution – the contribution of donors and friends of Jhabua.
      As we aspire together for a prosperous India, let us continue our commitment as ‘Partners – in – Change’.
   </p>
   <p class="text-center"><a href=" https://issuu.com/search?q=shivganga" target="_blank" class="btn donate-page btn-donate waterpage m-auto">Go to the Newsletter </a></p>
</div>