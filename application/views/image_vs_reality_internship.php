<div class="bg-f8">

<div class="container pt-5">
    <div class="row">
        <div class="col-md-12">

            <div class="col-md-8">
                <h4 class="our-purpose"> Image Vs Reality Internship</h4>

                <h6 class="our-purpose" style="font-size:28px; padding-top:65px">A Call for Student</h6>
                <p>
                Image Vs Reality Internship is a Unique Opportunity for Student to experience tribals India and develop their understanding of its culture and ground realities. Thus under its Rural Immersion Program, Shivganga invites volunteers for a month-long internship in Jhabua.
                    <!-- Shivganga has launched Mahasampark Sampark Abhiyan to reach 1 lakh tibal families
                    and invite them respecfully for Hama 2019.
                    Mahasampark Abhiyan is a Unique Opportunity for Student to experience
                    tribals India and develop their understanding of its culture and ground realities.
                    Thus under its Rural Immersion Program, Shivganga invites volunteers for a month-long
                    internship in Jhabua. -->
                </p>
            </div>
            <div class="col-md-8 ">

                <h6 class="our-purpose" style="font-size:28px; padding-top:65px">An experience like never before</h6><br>
                <p><b style="color:black; ">Period:</b>All through out the year </p>
                <p><b style="color:black">Who can apply:</b> All UG/PG students</p>
                <!-- <p><b style="color:black">Last to apply:</b> 15 Nov 2019 </p> -->
                <p><b style="color:black">Role:</b></p>

                <p>
                    <!-- You will get a chance to spend 10-15 days in tribals village. There you will join
                    the Shivganga Karykartas to visit the families and invite them for Halma 2019. it
                    involves creating a healthy discussion with the family, listening to them ,and
                    learning from their experience. You will also be part of other various activities at
                    Shivganga Gurukul.
                    Task at Hand: You are required to do documentations of Mahasampark
                    Abhiyan in whatever ways you find suitable.(Flim/Photos/Sketch/Writings).
                    You can also choose subjects like Community Participation, Environmental
                    Enrichment, Water Conservation, Social-Entrepreneurship, Social Leaders etc.to
                    work on, aligning to your interrest.
                    More Details shall be provided on reaching Jhabua. Meanwhile, you can always
                    contact us.&#128528; -->
                    You will get a chance to spend 10-15 days in tribals village. There you will join the Shivganga Karykartas to visit the families and invite them  join different Shivganga programmes  and activities . it involves creating a healthy discussion with the family, listening to them ,and learning from their experience. You will also be part of other various activities at Shivganga Gurukul. Task at Hand: You are required to do documentations of various activities in whatever ways you find suitable.(Film/Photos/Sketch/Writings). You can also choose subjects like Community Participation, Environmental Enrichment, Water Conservation, Social-Entrepreneurship, Social Leaders etc.to work on, aligning to your interest. More Details shall be provided on reaching Jhabua. Meanwhile, you can always contact us.

                </p>
                <div class="pt-4 pb-4">
                    <a href="https://forms.gle/VmGFLi7DyQqN4yWH6"><button style="width:60%" class="btn btn-success"><b>Apply Now</b></button></a>
                </div>
            </div>
            <div class="col-md-8 mb-5">

                <h6 class="our-purpose" style="font-size:28px; padding-top:65px">**</h6>
                <p>
                ** The applicants shall be selected based on verbal communication and it may be possible that we reject the application based on the vacancy.
                </p>
            </div>
        </div>
    </div>
</div>
</div>