<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<style>
	.halma2020-caption {
		left: 0;
		top: 0%;
		padding-top: 100px;
		bottom: 0;
		right: 0;
	}
</style>
<div class="halma2020-div">
	<img src="<?php echo base_url(); ?>assets/img/halma.png" class="img-halma2020" style="height:715px" />

	<!-- <div class="halma2020-caption ">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">

					<h3 style="font-size: 18px;width: 82%;line-height: 1.6;">
						सैकड़ो, हज़ारो तालाब अचानक शून्य से नहीं प्रकट हुए थे।
						इनके पीछे एक इकाई थी बनवाने वालो की, तो
						दहाई थी बनाने वालो की। यह इकाई, दहाई मिलकर
						सैकड़ा, हज़ार बनती थी। पिछले दो सौ बरस में
						नए किस्म की थोड़ी सी पढाई क्या पढ़ गए, समाज ने
						बस इकाई, दहाई, सैकड़ा, हज़ार को शून्य ही बना दिया...
						
					</h3>
					<h3 style="font-size: 18px;width: 62%;line-height: 1.6; color:#fff !important"><b>...यह शून्य फिर से इकाई दहाई, सैकड़ा और हज़ार बन सकता है।</b></h3>

					<h1 style="color: #e68900 !important"><b>आओ मिल कर प्रयास करे</b></h1>


					<h3 style="font-family: 'open-sans'; font-size: 23px;margin-right: 67px;margin-top:40px">Feb 29-Mar 1,2023</h3>
					<h1 style="color:#fff !important">Halma 2023</h1>
					<h3 style="font-size: 23px; color:#fff !important">Parmarth ki Parampara</h3>
				</div>
			</div>
		</div>

	</div> -->
</div>
<div class="bg-f8">
	<div class="container">
		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<p class="what-we-do">ABOUT HALMA</p>
				<h4 class="our-purpose">What is Halma Abhiyan?</h4>
				<p class="the-primary halma-primary-txt">Halma is an ancient tribal tradition of community
					participation. Shivganga has been organizing a mass scale Halma at Hathipava hills of
					Jhabua since 2009. More than 10000 villagers participate in this mega-event of
					environmental enrichment — the Students and professionals across the country participate
					in this mesmerizing show of tribal values of Parmarth. The Shivganga aims to take Halma to the world as it
					is a potential sustainable solution to many disastrous global problems.
				</p>
			</div>
		</div>
		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<p class="what-we-do">ABOUT HALMA</p>
				<h4 class="our-purpose">Glimpses of Halma</h4>
			</div>
		</div>
		<div class="row pt-4">
			<div class="col-lg-5 offset-lg-1 col-xl-4 col-md-6 offset-xl-2 pb-4 div-vdo-player">
				<video id="my-video" class="video-js" controls preload="auto" width="350" height="200" poster="<?php echo base_url(); ?>assets/img/halma2020-video-img.jpeg" data-setup="{}">
					<source src="<?php echo base_url(); ?>assets/img/halma.mp4" type="video/mp4" />
					<source src="MY_VIDEO.webm" type="video/webm" />
					<p class="vjs-no-js">
						To view this video please enable JavaScript, and consider upgrading to a
						web browser that
						<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
					</p>
				</video>
			</div>
			<div class="col-xl-4 col-lg-6 col-md-6 div-vdo-player">
				<video id="my-video" class="video-js" controls preload="auto" width="350" height="200" poster="<?php echo base_url(); ?>assets/img/halma2020-video-img.jpeg" data-setup="{}">
					<source src="<?php echo base_url(); ?>assets/img/jhabua.mp4" type="video/mp4" />
					<source src="MY_VIDEO.webm" type="video/webm" />
					<p class="vjs-no-js">
						To view this video please enable JavaScript, and consider upgrading to a
						web browser that
						<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
					</p>
				</video>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row pt-5 pb-5">
			<div class="col-12 bg-halma">
				<p class="lets-come-p">Let’s come together and work hand-in-hand to bring prosperity to
					Mother Earth.Show your contribution for Halma.
				</p>
				<a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
			</div>
		</div>
	</div>
</div>

<div class="bg-34">
	<div class="container pt-5 pb-5">
		<div class="row">
			<div class="col-md-6">
				<img src="<?php echo base_url(); ?>assets/img/img-active.jpeg" class="img-activism pb-4" />
			</div>
			<div class="col-md-6">
				<p class="what-we-do text-center">OUR STORY</p>
				<h4 class="our-purpose">Activity not Activism</h4>
				<p class="the-primary">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
					ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
					ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
					reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
					Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
					mollit anim id est laborum.
				</p>
				<p class="the-primary">
					Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et
					commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris.
					Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula.
					Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros
					est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi.
					Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo
					eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing
					sapien, sed malesuada diam lacus eget erat.
				</p>
			</div>
		</div>
	</div>
</div>

<!-- 
<div class="bg-f8">
	<div class="container">
		<div class="row pt-5 pb-5">
			<div class="col-md-12 text-center">
				<p class="what-we-do">PREVIOUS STATS</p>
				<h4 class="our-purpose">Our Impact</h4>
			</div>
		</div>
	</div>
</div>

<div class="contaier-fluid">
	<img src="<?php echo base_url(); ?>assets/img/imgfull.png" class="img-full-screen">
	<img src="<?php echo base_url(); ?>assets/img/imgsmall.png" class="img-small-screen">
</div> -->
<div class="bg-f8">
	<div class="container-fluid  " style="background-color:rgb(11 26 49); color:white; ">
		<div class="row">
			<div class="col-md-12 text-center pt-3" style="">
				<h3><span> Our Impact Since 2007</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 offset-md-2 counter-stats">
				<div class="row py-5 ">
					<div class="col-md-4 mt-4  col-12 text-center align-self-center" style="">
						<i class="fa fa-tint ml-3" style="font-size:50px"></i>
						<div id="counter2">
							<span class="counter-value2" style="font-size:60px" data-count="800">0</span><span style="font-size:60px">M</span>
							<h6>Water Conserved </h6>
						</div>
					</div>
					<div class="col-md-4 mt-4 col-12 text-center align-self-center vl" style="">
						<i class="fa fa-tree ml-3 " style="font-size:50px"></i>
						<div id="counter2">
							<div class="counter-section">
								<span class="counter-value2" style="font-size:60px" data-count="110000">0</span>
								<h6>Trees Planted</h6>
							</div>
						</div>
					</div>
					<div class="col-md-4 mt-4 col-12 vl text-center align-self-center" style="">
						<i class="fa fa-users ml-3 " style="font-size:50px"></i>
						<div id="counter2">
							<div class="counter-section">
								<span class="counter-value2" style="font-size:60px" data-count="12000">0</span>
								<h6>Youth Connected</h6>
							</div>
						</div>
					</div>
					<div class="col-md-4 mt-4 col-12 vl text-center align-self-center" style="">
						<i class="fa fa-user ml-3" style="font-size:50px"></i>
						<div id="counter2">
							<div class="counter-section">
								<span class="counter-value2" style="font-size:60px" data-count="6100">0</span>
								<h6>Women Trained</h6>
							</div>
						</div>

					</div>
					<div class="col-md-4 mt-4 col-12 text-center vl2 align-self-center " style="">
						<i class="fa fa-thumbs-up ml-3" style="font-size:50px"></i>
						<div id="counter2">
							<div class="counter-section">
								<span class="counter-value2" style="font-size:60px" data-count="1800">0</span>
								<h6>Professionals Visited</h6>
							</div>
						</div>
					</div>
					<div class="col-md-4 mt-4 col-12 text-center align-self-center vl3" style="">
						<i class="fa fa-users ml-3" style="font-size:50px"></i>
						<div id="counter2">
							<div class="counter-section">
								<span class="counter-value2" style="font-size:60px" data-count="600">0</span>
								<h6>Training camps</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="color:white; margin-bottom:100px;  ">
			<div class="col-md-12">
				<div class="row py-5">

				</div>
			</div>
		</div>


	</div>
</div>

<div class="bg-f8">
	<div class="container">
		<div class="row pt-5">
			<div class="col-md-12 text-center">
				<p class="what-we-do">JHABUA 2023</p>
				<h4 class="our-purpose">Our Vision</h4>
			</div>
		</div>
	</div>
</div>
<div class="bg-f8">
	<div class="container">
		<div class="row text-center pt-5">
			<div class="col brder-r8">
				<h3 class="trained-worker">12000<span>+</span></h3>
				<p class="what-we-do">Trained Workers</p>
			</div>
			<div class="col brder-r8">
				<h3 class="trained-worker">700<span>+</span></h3>
				<p class="what-we-do">Villages</p>
			</div>
			<div class="col brder-r8">
				<h3 class="trained-worker">70,000<span>+</span></h3>
				<p class="what-we-do">FAMILIES</p>
			</div>
			<div class="col brder-r8">
				<h3 class="trained-worker">20,000<span>+</span></h3>
				<p class="what-we-do">VILLAGERS</p>
			</div>
			<div class="col">
				<h3 class="trained-worker">40,000<span>+</span></h3>
				<p class="what-we-do ls-2">CONTOUR TRENCHES</p>
			</div>
		</div>
	</div>
</div>
<div class="bg-f8">
	<div class="container">
		<div class="row pt-5 pb-5">
			<div class="col-md-12 text-center">
				<p class="what-we-do">JOIN US</p>
				<h4 class="our-purpose">Get Involved</h4>
				<p class="the-primary halma-primary-txt">
					Come along with Shivganga to be a part of this huge step towards Sustainable Development.
					This is a unique opportunity to experience the tribal India and develop their understanding
					of its culture and ground realities.
				</p>
			</div>
		</div>
	</div>
</div>

<div class="bg-f8">
	<div class="container">
		<div class="row pb-5">
			<div class="col"></div>
			<div class="col-md-7">
				<h3 class="we-make text-center">Reach Us</h3>
				<form method="POST" action="<?php echo base_url() ?>Home_Controller/save_join_us_data">
					<div class="form-group mb-4">
						<input type="email" name="email" class="form-control custom-form" placeholder="Email id" id="email" required>
					</div>
					<div class="form-group mb-4">
						<input type="text" name="name" class="form-control custom-form" placeholder="Full name" id="name" required>
					</div>
					<div class="form-group mb-4">
						<input type="text" name="address" class="form-control custom-form" placeholder="Address" id="address" required>
					</div>
					<div class="form-row mb-4">
						<div class="col">
							<input type="number" name="contact" class="form-control custom-form" placeholder="Phone No." id="contact" required>
						</div>
						<div class="col">
							<input type="text" name="rollof" class="form-control custom-form" placeholder="Roll of Interest" id="rollof" required>
						</div>
					</div>
					<div class="form-group mb-4">
						<input type="text" name="purpose" class="form-control custom-form" placeholder="Purpose/Intend" id="purpose" required>
					</div>
					<div class="text-left">
						<button class="btn my-2 my-sm-0 btn-donate" type="submit">Send</button>
					</div>
				</form>
			</div>
			<div class="col"></div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/video.js"></script>
<script>
	var b = 0;
	$(window).scroll(function() {

		var oTop1 = $('#counter2').offset().top - window.innerHeight;
		if (b == 0 && $(window).scrollTop() > oTop1) {
			$('.counter-value2').each(function() {
				var $this = $(this),
					countTo = $this.attr('data-count');
				$({
					countNum: $this.text()
				}).animate({
						countNum: countTo
					},

					{

						duration: 2000,
						easing: 'swing',
						step: function() {
							$this.text(Math.floor(this.countNum));
						},
						complete: function() {
							$this.text(this.countNum);
							//alert('finished');
						}

					});
			});
			b = 1;
		}

	});
</script>