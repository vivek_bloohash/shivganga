<?php
	// if(empty($_SESSION['user_name'])) {
	// 	redirect('login');
	// }
?>
<div class="halma2020-div">
		<img src="<?php echo base_url();?>assets/img/img-event-drum.png" class="img-event-drum">
	</div>
	<div class="bg-f8">
		<div class="container">
			<div class="row pt-5">
				<div class="col-md-7 offset-md-3">
					<h4 class="our-purpose">Jhabua Summer Camp</h4>
					<div class="row pt-4">
						<div class="col-md-5">
							<p class="events-more-date mb-0">Date/Time</p>
							<p class="events-more-loc">NOVEMBER 20, 2019</p>
						</div>
						<div class="col-md-7">
							<p class="events-more-date mb-0">Location</p>
							<p class="events-more-loc">Golden Jubilee Hall, SGSITS, Indore.</p>
						</div>
					</div>
					<p class="the-primary">CampJhabua, a tribal district in Madhya Pradesh, had everything-rich
						forests, water, agriculture and happy life. Everything has changed in the last few
						decades. Several schemes and policies have been made. And yet we see nothing changing
						for them.
					</p>
					<p class="the-primary">We feel pity for them. We go to show sympathy & kindness and shower
						them with small aids-in cash or kind. And in this process of gratifying ourselves,
						we end up snatching their self-esteem, their pride, their honesty…!
					</p>
					<p class="the-primary">We invite you to experience this journey of togetherness, the journey
						of bridging the gap and bringing two dots closer.
					</p>
					<p class="event-schedule pt-4">Schedule</p>
					<p class="the-primary">09:00 am – Welcome, Breakfast and Registration</p>
					<p class="the-primary">09:50 am – Picture presentation</p>
					<p class="the-primary">10:10 am – Harsh Chouhan-Subject Introduction</p>
					<p class="the-primary">10:30 am – Kumar Harsh-Observation of a Youth-“Rural India-How it is…How it should be!”
					</p>
					<p class="the-primary">11:00 am – Tea Break</p>
					<p class="the-primary">11:15 am – Rajaram Katara -“The story of Change in hundreds of villages”</p>
					<p class="the-primary">11:45 am – Nitin Dhakad-Jhabua Naturals-“Steps towards Social Entrepreneurship”</p>
					<p class="the-primary">12:10 pm – A session with Mahesh Sharma</p>
					<p class="the-primary">12:30 pm – Website Launching and Q&A</p>
					<p class="the-primary">01:00 pm – Lunch</p>
				</div>
			</div>
			<div class="row pt-5 pb-5">
				<div class="col-md-10 offset-md-1">
					<p class="event-schedule">Speakers</p>
					<div class="row">
						<div class="col-md-4">
							<img src="<?php echo base_url();?>assets/img/jhabua-baba.png" class="four-img" />
							<div class="active-member-div">
								<h4 class="affor-txt-heading pt-2 pb-1">Mahesh Sharma</h4>
								<p class="the-primary lineht-28">The social leader who pioneered the change of perception about and within
									the
									tribal of Jhabua by focusing on Wyakti-Nirmani through Parmarth ki prerna.
									Nominated for ‘PadmaSri’</p>
							</div>
						</div>
						<div class="col-md-4">
							<img src="<?php echo base_url();?>assets/img/jhabua-baba.png" class="four-img" />
							<div class="active-member-div">
								<h4 class="affor-txt-heading pt-2 pb-1">Mahesh Sharma</h4>
								<p class="the-primary lineht-28">The social leader who pioneered the change of perception about and within
									the
									tribal of Jhabua by focusing on Wyakti-Nirmani through Parmarth ki prerna.
									Nominated for ‘PadmaSri’</p>
							</div>
						</div>
						<div class="col-md-4">
							<img src="<?php echo base_url();?>assets/img/jhabua-baba.png" class="four-img" />
							<div class="active-member-div">
								<h4 class="affor-txt-heading pt-2 pb-1">Mahesh Sharma</h4>
								<p class="the-primary lineht-28">The social leader who pioneered the change of perception about and within
									the
									tribal of Jhabua by focusing on Wyakti-Nirmani through Parmarth ki prerna.
									Nominated for ‘PadmaSri’</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>