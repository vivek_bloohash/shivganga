<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<div class="halma2020-div header-image-involve">
    <div class="carousel-caption">
        <h1>Get Involved</h1>
    </div>
</div>

<div class="container">
    <div class="meet-member-wrapper mt-5">
        <div class="row">
            <div class="col-lg-6">
                <div class="meet-member-img std">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="meet-single-items">
                    <div class="content">
                        <!-- <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">श्री महेश जी का सन्देश</h4> -->
                        <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; font-size:18px;">प्रिय मित्रवर!<br>
                            आप सभी मित्र अलग-अलग समय पर झाबुआ में आये हैं। हमारे लिए, आपका आना एक सुखद अनुभव रहा है। आशा है आपके मन में भी झाबुआ की सुखद अनुभूतियाँ और स्मृतियाँ होंगी।<br>
                            समय-समय पर झाबुआ आने वालों का अब एक ऐसा बड़ा समूह हो गया है, जो झाबुआ आने के बाद समाज को एक नयी दृष्टि से समझ रहे हैं। सभी के मन में समाजिक बदलाव में अपनी भूमिका के निर्वहन को लेकर कईं प्रश्न रहते हैं।<br>
                            मुझे लगता है कि सामर्थ्यशील युवाओं का इतना बड़ा समूह एक बड़ी भूमिका निभाकर भारत के भविष्य की रूपरेखा तय कर सकता है।<br>
                            सामाजिक बदलाव के लिये इच्छुक युवा मित्रों की दो दिवसीय शिविर का आयोजन दिनांक 25-26 मई 2019 को झाबुआ हो रहा है।<br>
                            साग्रह निवेदन है कि परमार्थ की प्रेरणा के इस शिविर में अवश्य सहभागी हों।<br>
                            आपका सदैव<br>
                            महेश शर्मा,<br>
                            शिवगंगा झाबुआ
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;font-size:18px;">
                                <div class="col-md-12 text-center green-txt side-by-btn ">
                                    <span>आओ मिलकर विचार करें, नये भारत का भविष्य गढ़ें।।</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row pt-5">
        <div class="col-md-12 text-left">
            <div>
                <!-- <p class="what-we-do">THE STORY</p> -->
                <h4 class="our-purpose">Students' Involvement</h4>
                <h6 style="font-size:28px; padding-top:65px" class="our-purpose ">Academic trip to villages</h6>

                <p class="the-primary">
                    Student are welcome to organise village trips to experience the life of the tribals and gain values. You can have a closer look at their culture and traditions<br>
                    <a href="<?php echo base_url(); ?>joinus"><b style="color:rgb(68 78 112)">
                            <p>For more queries feel free to enquire</p>
                        </b>
                    </a>
                </p>
                <h6 style="font-size:28px; padding-top:50px" class="our-purpose ">Internship Programs</h6>
                <p class="the-primary">
                    Come and stay with tribal people of Jhabua. Give a real sense to your modern education <br />
                    <a href="<?php echo base_url(); ?>joinus"><b style="color:rgb(68 78 112)">
                            <p>For more queries feel free to enquire</p>
                        </b>
                    </a>
                </p>
                <h6 style="font-size:28px; padding-top:50px" class="our-purpose ">Volunteer</h6>
                <p class="the-primary">
                    If you want to serve mother earth , this opportunity is for you <br />
                    <a href="<?php echo base_url(); ?>joinus"><b style="color:rgb(68 78 112)">
                            <p>For more queries feel free to enquire</p>
                        </b>
                    </a>
                </p>

            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row pt-5">
        <div class="col-md-12 text-left">
            <div>
                <!-- <p class="what-we-do">THE STORY</p> -->
                <h4 class="our-purpose">Contribution / CSR partner</h4>
                <p class="the-primary">
                    We believe tribal societies if preserved and handheld can show us a path of sustainable, social and environmental living. Shivganga looks for a hand holder in its vision and mission who can collaborate and become a partner in change.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="bg-f8">
    <div class="container-fluid">
        <div class="row pt-5">
            <div class="col-md-12 text-center">
                <p class="what-we-do">Make Your Contribution</p>
                <h4 class="our-purpose">Internship Experience</h4>
            </div>
        </div>
        <div class="row pt-5">
            <div class="col"></div>
            <div class="col-md-8">
                <div class="owl-three owl-carousel stories-change">
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/chirag_rajyaguru.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/Divyansh_Saxena.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/kaivlya_swami.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/kimbaly_mahakalkar.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/kumi_dhir.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/manya_agarwal.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/prachi_porwal.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/pranjal.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/udita_wadwa.png" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/अभिषेक_सोमानी.jpg" alt="" />

                    </div>
                    <div class="item">
                        <img data-enlargeable width="100" style="cursor: zoom-in" src="<?php echo base_url(); ?>assets/img/student_anubhuti/नरेश_शर्मा.jpg" alt="" />

                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('.owl-three').owlCarousel({
            margin: 20,
            autoWidth: false,
            nav: true,
            loop: true,
            items: 3,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('.owl-one').owlCarousel({
            margin: 20,
            autoWidth: false,
            nav: true,
            loop: true,
            items: 3,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                }
            },
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
                '<i class="fa fa-angle-right" aria-hidden="true"></i>'
            ]
        });
    });
</script>
<script>
    $('img[data-enlargeable]').addClass('img-enlargeable').click(function() {
        var src = $(this).attr('src');
        var modal;

        function removeModal() {
            modal.remove();
            $('body').off('keyup.modal-close');
        }
        modal = $('<div>').css({
            background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
            backgroundSize: 'contain',
            width: '100%',
            height: '100%',
            position: 'fixed',
            zIndex: '10000',
            top: '0',
            left: '0',
            cursor: 'zoom-out'
        }).click(function() {
            removeModal();
        }).appendTo('body');
        //handling ESC
        $('body').on('keyup.modal-close', function(e) {
            if (e.key === 'Escape') {
                removeModal();
            }
        });
    });
</script>