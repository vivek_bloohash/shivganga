<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<!-- <div class="halma2020-div header-image-forest">
   <div class="carousel-caption">
      <h1>Gaun gaun banega Tirath</h1>
   </div>
</div> -->
<div class="container">
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img connect">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>CONNECTING COMMUNITIES</p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Urban and Tribal Societies</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Over a long time, our country has missed a coercive model of knowledge sharing and working together between tribal and urban communities—a colossal loss in the path of the prosperity of our nation. Shivganga is bridging the gap, and eventually, Jhabua has become a platform where students and professionals across the country and tribal youths abuzz with traditional wisdom are working together. The students and professionals take a lifetime experience of ground reality, enabling them to appropriate policies and innovation. Thus, also ensuring social capital generation.
                  </p>
               </div>
               <div class="row">
                  <div class="col-md-12 ">
                     <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-12 text-center green-txt side-by-btn ">
                           <span>Get involved with Shivganga to make our villages self-reliant and full of self-esteem! </span>
                           <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/16.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Image vs Reality</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           The Tribal Community of Jhabua faced ill repute and misappropriation based on perceptions and images - sometimes at the diagonal end of reality. Under the Image vs Reality of Shivganga, every year, hundreds of students and chunks of the urban populace visit Jhabua. They live in the villages and experience the humility & values of the tribal community - breaking their perceptions forever.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/17.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Facilitated Academic Projects</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           Shivganga facilitates research and study on subjects relevant to the tribal community of Jhabua and rural development. It includes providing accommodation, food and local commutation, and assistance and guidance for the students by Shivganga’s volunteers and Social Leaders.
                        <ol type="number">
                           <li>Kumar Harsh, a final year B.Arch student of IIT Roorkee, did his thesis on rural architecture, taking Chhagola village as his research field. IIT Roorkee awarded the project the Service-Oriented Natural Science Innovation & Application (SONIA) Award. </li>
                           <li>ENACTUS-IIT Delhi team led by one Udita Wadhwa - third-year student - initiated research for a viable model of Biogas Plant in rural-tribal areas like Jhabua. They put an experimental flexi model at Shivganga's Dharampuri Gurukul. </li>
                           <li>Kimbaly Mahakalkar, a pre-final year MBBS student of RCSM Govt Medical College and CPR Hospital, Kolhapur, spent seven months in Jhabua studying the community health conditions of the Bhil tribe. </li>

                        </ol>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/v.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Main aur Mera Parmartha - Camp</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           Owing to the Image vs Reality program today, there's a network of more than 400 students & professionals who are connected with Shivganga. Shivganga organises a two-days camp every year inviting such individuals, where we discuss how together we can contribute to the movement of Holistic Rural Development in Jhabua.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div class="why-choose-us-section choose-bg">
   <div class="choose-bg-02" style=""></div>
   <div class="container">
      <div class="row">
         <div class="section-title col-md-12">
            <h4 class="title style-01">Fellowship Program</h4>
            <p class="description style-01">A choice that makes the difference from others.</p>
         </div>
         <div class="single-items-wrapper col-md-4">
            <div class="why-choose-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
               <img src="<?php echo base_url(); ?>assets/img/Vishwanath.png" height="auto" width="100%" alt="">
               <div class="content">
                  <h4 class="title">Vishwanath Allannavar</h4>
                  <p> BA-LLB-Bangalore University - Law College(2018), has been working with Shivganga for three years. He came to Jhabua in 2016 through the Image vs Reality program. After some visits, he became a fellow for two years, taking up a project of Social Capital Generation through Youth Empowerment Camps. As a full-time volunteer, Vishwanath is engaged in various projects like Community Forests Rights and Jan Samvardhan dimension of Shivganga.</p>
               </div>
            </div>
         </div>
         <div class="single-items-wrapper col-md-4">
            <div class="why-choose-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
               <img src="<?php echo base_url(); ?>assets/img/Rishabh.png" height="auto" width="100%" alt="">
               <div class="content">
                  <h4 class="title">Rishabh Seth</h4>
                  B.Tech-IIT Roorkee(2019), met Shri Mahesh Sharma and Nitin Dhakad in Vision India Foundation’s program in Delhi in 2018. Inspired by their address on Rural Development, post-college, Rishabh left his job and came to Jhabua in 2020. As a fellow, he is now involved in the Jhabua Tourism venture and Bamboo Training Centre at Meghnagar.</p>
               </div>
            </div>
         </div>
         <div class="single-items-wrapper col-md-4">
            <div class="why-choose-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
               <img src="<?php echo base_url(); ?>assets/img/Satyajeet.png" height="auto" width="100%" alt="">
               <div class="content">
                  <h4 class="title">Vishwanath Allannavar</h4>
                  <p> BA-LLB-Bangalore University - Law College(2018), has been working with Shivganga for three years. He came to Jhabua in 2016 through the Image vs Reality program. After some visits, he became a fellow for two years, taking up a project of Social Capital Generation through Youth Empowerment Camps. As a full-time volunteer, Vishwanath is engaged in various projects like Community Forests Rights and Jan Samvardhan dimension of Shivganga.</p>
               </div>
            </div>
         </div>
         <div class="single-items-wrapper col-md-4">
            <div class="why-choose-single-items wow animate__ animate__fadeInUp animate__delay-1s animated" style="visibility: visible; animation-name: fadeInUp;">
               <img src="<?php echo base_url(); ?>assets/img/Avinash.png" height="auto" width="100%" alt="">
               <div class="content">
                  <h4 class="title">Avinash Mattur</h4>
                  LLM-Amity University (2020), came to Jhabua in 2019 under the Image vs Reality program. After completing his studies, he took on the project of Community Forest Rights in Jhabua. Since then, as a fellow, he has been working on the CFR project spread over 25 villages.</p>
               </div>
               <img loading="lazy" src="assets/img/setting-shape.webp" class="shape" alt="">
            </div>
         </div>
         <div class="single-items-wrapper col-md-4">
            <div class="why-choose-single-items wow animate__ animate__fadeInUp animate__delay-2s animated" style="visibility: visible; animation-name: fadeInUp;">
               <img src="<?php echo base_url(); ?>assets/img/Kumar.png" height="auto" width="100%" alt="">
               <div class="content">
                  <h4 class="title">Kumar Harsh</h4>
                  <p>B.Arch-IIT Roorkee(2020), is pursuing the fellowship program of Shivganga for one year. He came to Jhabua in 2018 under the Image vs Reality program and also did his final year thesis in Jhabua. Today, he is involved in the Communication Design team and other projects in Shivganga’s Dharmapuri Gurukul. </p>
               </div>
            </div>
         </div>
         <div class="single-items-wrapper col-md-4">
            <div class="why-choose-single-items wow animate__ animate__fadeInUp animate__delay-2s animated" style="visibility: visible; animation-name: fadeInUp;">
               <img src="<?php echo base_url(); ?>assets/img/Anoop.png" height="auto" width="100%" alt="">
               <div class="content">
                  <h4 class="title">Anoop Singh</h4>
                  <p>B.Tech-Civil-IIT Roorkee(2020) came to Jhabua in Jan 2021 under the Image vs Reality program. He initially spent three months exploring the idea of exploring peer-to-peer learning and also spent a month in Vande Matram Foundation for the same. He is continuing as a fellow and involved in youth empowerment programs of Shivganga. </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/connecting.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Friends of Jhabua - City</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           Friends of Jhabua is an initiative to connect volunteers from different cities and institutes across fields of expertise to the social movement in Jhabua. The volunteers serve the purpose of hand-holding the ground team. They assist different projects through their knowledge of the respective field and play a key role in fundraising for the same. They also organise various events and visits to Jhabua under the Image vs Reality program. Such activities help connect people and institutions to Jhabua, thus adding pace to the movement.
                           At the same time, students get a reliable portal where they seek inputs & guidance for their academic projects, a platform to experiment their ideas and learn from the tribal community.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">

         <p>Cities: Indore, Mumbai, Delhi, Bangalore, Bhopal</p>
         <p>Overseas: US, UK</p>

      </div>
   </div>
   <div class="container py-5">
      <div id="owl-intern" class="row pt-5">
         <div class="col-md-12 col-md-12 px-5">
            <div class="owl-intern owl-carousel student-engage">
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/rurkee.jpg" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/mumbai.jpg" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/Delhi.jpg" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/lbsim.jpg" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/tiss.jpg" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/niti.jpg" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/download.png" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/JNU.png" alt="" />
               </div>

               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/Mnit_logo.png" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/LU_Logo-JPG_100220.jpg" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/NCST.png" alt="" />
               </div>
               <div class="item wpo-rural-item d-flex self-align-center">
                  <img src="<?php echo base_url(); ?>assets/img/student-engagement/Jamia_Millia_Islamia_Logo.svg.png" alt="" />
               </div>
            </div>
         </div>
      </div>

   </div>
</div>

<div class="bg-34">
   <!-- <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8">
                <h3 class="we-make pt-3">Image vs Reality Program</h3>
                <p class="the-primary">
                    This programme is one of our key activities. Every year around 500 students and delegates visit
                    Jhabua and spend time with villagers. They get the first-hand experience of the problems and
                    strengths of the community. After breaking through the perceptions, many of them stay for longer
                    durations. Some students stayed for 2-6 months internships and even for 1-2 years to work on various
                    projects. Students from IIT Bombay, IIT Delhi, IIT Roorkee, TISS Mumbai, NITIE Mumbai, IIM Indore,
                    IIM Ahmedabad, LBSIM have conducted their rural immersion program in Jhabua. </p>
            </div>
        </div>
    </div> -->
   <div class="container">
      <div class="row pb-5">
         <div class="col-md-12 text-center pt-5 green-txt side-by-btn">
            <span>Get involved with Shivganga to make our villages self-reliant and full of self-esteem!</span>
            <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
         </div>
      </div>

   </div>
</div>
<script>
   $('.owl-intern').owlCarousel({
      margin: 20,
      autoWidth: false,
      autoplay: true,
      autoPlaySpeed: 2000,
      autoPlayTimeout: 2000,
      autoplayHoverPause: true,
      nav: true,
      loop: true,
      dots: false,
      items: 4,
      navText: [
         '<i class="fa fa-angle-left" style =" color:white; border-radius:0px" aria-hidden="true"></i>',
         '<i class="fa fa-angle-right" style =" color:white; border-radius:0px" aria-hidden="true"></i>'
      ],
      responsive: {
         0: {
            items: 1,
            marginLeft: 20,
            marginRight: 0,
         },
         768: {
            items: 3
         },
         912: {
            items: 4
         }
      }
   });
</script>