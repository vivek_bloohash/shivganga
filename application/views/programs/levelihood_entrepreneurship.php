<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<!-- <div class="halma2020-div header-image-forest">
   <div class="carousel-caption">
      <h1>Gaun gaun banega Tirath</h1>
   </div>
</div> -->

<div class="container">
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img emp">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>LIVELIHOOD & ENTREPRENEURSHIP</p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Young Entrepreneurs</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">The tribal communities everywhere in India have Shivganga aims at developing an entrepreneurial ecosystem to foster young entrepreneurs from tribal
                    societies. shivganga in collaboration with tata institute of social sciences, mumbai have initiated
                    an incubation centre for these entrepreneurs. the objective of the centre is to facilitate and
                    catalyze the complete cycle from skill training to livelihood generation.</p>
               </div>
               <div class="row">
                  <div class="col-md-12 ">
                     <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-12 text-center green-txt side-by-btn ">
                           <span>Help beating environmental pollution by planting trees</span>
                           <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>




<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/jhabua_naturals.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Jhabua Naturals</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Ramsingh Medha is a marginal farmer. He along with his family used to migrate to
                    sustain himself. Training in organic farming has reduced his input cost. Connecting it to market
                    through Jhabua Naturals has secured him greater income. He now aspires to make his entire land
                    organic and learn growing different crops organically. Jhabua Naturals is a social enterprise by
                    devoted tribal farmers. After training in organic farming, 50+ farmers like Ramsingh Medha are
                    marketing their chemical free farm produce in Indore. The ventures has completed the pilot phase and
                    is ready to embark a long journey.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/jhabua_craft.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Jhabua Crafts</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Kamlesh Damor, is a young creative person. It was never possible for him to express his creativity
                    at construction sites while working as a labourer. Continuous training in Bamboo Handicrafts has
                    made him learn various Bamboo Products. 40+ artisans like Kamlesh are supplying bamboo handicrafts
                    through Jhabua Naturals E-Commerce Platform: <a style="color:green"
                        href="www.jhabuanaturals.com"><span>www.jhabuanaturals.com</a>


                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<div class="bg-34">
    <div class="container">
        <div class="row pb-5">
            <div class="col-md-12 text-center pt-5 green-txt side-by-btn">
                <span>Get involved with Shivganga to make our villages self-reliant and full of self-esteem!</span>
                <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
            </div>
        </div>
    </div>
</div>