<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<!-- <div class="halma2020-div header-image-forest">
   <div class="carousel-caption">
      <h1>Gaun gaun banega Tirath</h1>
   </div>
</div> -->

<div class="container">
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>TRIBAL CULTURE</p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Parmarthi Community</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Tribal people have been viewed historically as ‘backward’ first by foreigners and then, our own country people. This perpetuated inferiority towards native wisdom, traditions and practices, hurting the self-esteem of tribal people and ultimately losing interest and confidence in their traditional knowledge. Why traditions? Traditions of tribal people were formulated to fit local requirements and all of them, empowering and enriching community participation and connection with nature. Refined over time and passed on generations to generations, these traditions still are potent enough to counter many local problems and combined with professional skills, become an ingenious sustainable solution to planetary problems like global warming.</p>
                  
               </div>
               <div class="row">
                  <div class="col-md-12 ">
                     <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-12 text-center green-txt side-by-btn ">
                           <span>Support us to take these traditions to the world.</span>
                           <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/t1.jpeg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Matavan</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        ‘Matawan’ literally means ‘forests of Mother’-mother here signifies the Mother
                    Earth. This is a traditional term used by Bhils for specially protected forest areas where tribal do
                    not cut trees in any case. Thus, Matawan has become a significant tool in afforestation.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/t2.jpeg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Halma</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Halma is an ancient tradition of the Bhil tribe of Jhabua, which conceives and promotes
                    ‘Parmarth’-highest good of others without any expectation. Folklore goes that when a person couldn’t
                    get out of difficulty after best efforts, there’s a call for ‘Halma’ and people of the community
                    come together to rescue without any expectation of any return.
                    Today Halma is prevalent in 1300+ villages in Jhabua-Alirajpur belt, at some places with different
                    names.

                    Halma has been extensively practised and promoted by Shivganga to reinforce water and forest
                    conservation efforts.
                    <br>
                    Bhangodia is a festive celebration of 10 days, parallel with Holi, in which people gather, meet,
                    sing & dance together. This is a time when farmers are done with their cultivation season by
                    bringing the ‘Rabi’ harvest in their home, thus, making it a suitable time for joyous celebration.
                    Shivganga worked on debunking the false narratives about Bhangodia and propagated its original form
                    which is a socio-cultural celebration by tribal people of Jhbua. Today it a mass celebration
                    wheremore than 30k tribal people celebrate their culture with pride, and people from across the
                    world visit to enjoy this cultural extravaganza.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/t3.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Gram Samruddhi Mela</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        This Mela is dedicated to explore the methods and means of bringing prosperity to the villages
                        through enrichment and conservation of natural resources like Jal, Jungle, Jameen, Jan and Janwar.
                        (Water, Forests, Land, People, Animals). Nava-Vigyan or Technology also has its place in the Mela to
                        make the villagers familiar with new technology. It is an important event for tribal villagers to
                        identify the resources at their disposal. Experts from related fields are also invited to have a
                        two-way interaction with villagers.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/3A8A0124.JPG">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Gaiti Yatra</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/jhabua_craft.jpg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Shivganga organises various ‘Yatras’ throughout the year to empower and make the tribal people aware
                        of different issues. Amongst these, ‘Gaiti Yatra’ holds a special place because it showcases Gaiti,
                        the spade or mattock used by these tribal farmers, as a weapon of change.
                        As a soldier walks with pride with a gun on his shoulder, so does a tribal farmer with his Gaiti on
                        his shoulder.
                        This reinstills self-esteem and pride in tribal farmers as well as generates respect and pride for
                        farmers in spectators.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="bg-34">
    <div class="container">
        <div class="row pb-5">
            <div class="col-md-12 text-center pt-5 green-txt side-by-btn">
                <span>Support us to take these traditions to the world.</span>
                <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
            </div>
        </div>
    </div>
</div>