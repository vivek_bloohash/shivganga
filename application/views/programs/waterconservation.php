<?php
   // if(empty($_SESSION['user_name'])) {
   // 	redirect('login');
   // }
   ?>
<!-- <div class="halma2020-div header-image-water">
   <div class="carousel-caption">
      <h1>Gaun gaun banega Tirath</h1>
   </div>
</div> -->
<div class="container">
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>WATER CONSERVATION</p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Tackling water crisis</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Water Crisis is the predominant stress of the tribal community of Jhabua. It sets in motion a vile cycle of woes that goes beyond the compulsion of migration. The reckless deforestation leading to barren hills and heavy surface runoff doesn't allow enough rainwater percolation. A large section of those with agricultural land take at most one crop a year. The youths of Jhabua empowered through training camps took a pledge to quench the thirst of Mother Earth. With the slogan "Gaon-Gaon me jayenge, Shankar Jata banayenge", they revived their age-old tradition Halma and started making Shankar Jata, i.e. water structures to conserve water in Jhabua. Thus began a Jal Andolan. </p>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Deforestation is also contributing to the water crisis problem of Jhabua.
                  </p>
               </div>
               <div class="row">
                  <div class="col-md-12 ">
                     <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-12 text-center green-txt side-by-btn ">
                           <span>Save your share of water</span>
                           <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/newTalab.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Reservoirs (Earthen Dam) Construction</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           Every year during Gram Engineering Training villagers identify sites for water reservoirs. They do Halma to build these reservoirs during dry summer months.
                           Till now
                           80 talab constructed with water holding capacity of 481.43 crore litre
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/1.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Small water structures</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           Apart from water reservoirs, small water bodies such as hand pump recharging, Bori Bandhan, contour trenching helps in recharging groundwater. These one-time structures are Constructed post the monsoon rain, it helps villagers to retain water for immediate use.
                           Till now
                           more than 500 small water bodies were repaired & built
                           209050 counter trenches constructed.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/2.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Watershed Development with NABARD</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                           Under the Watershed Development Project supported by NABARD, Shivganga has treated 100 hectares of land this year in Kheda village. Shivganga, through its network of volunteers, availed the human resource for the project who have constructed:
                        <ul>
                           <li>6320 running meters of earthen field bund
                           </li>
                           <li>965 running meters of stone field bunding
                           </li>
                           <li>117 stone outlets,
                           </li>
                           <li>240 staggered contour trenches
                           </li>
                           <li>Two farm ponds
                           </li>
                           <li>Six stone gully plugs
                           </li>
                           <li>100 horticulture plants and
                           </li>
                           <li>400 agroforestry plants
                           </li>
                        </ul>
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Our counter start -->
<div class="container-fluid p-0 waterpage">
   <div class="wpo-counter-area">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="wpo-counter-grids">
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="80">0</span>+</h2>
                     </div>
                     <p>PONDS</p>
                  </div>
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="500">0</span>+</h2>
                     </div>
                     <p>WELLS</p>
                  </div>
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="200900">0</span>+</h2>
                     </div>
                     <p>CONTOUR TRENCHES</p>
                  </div>
                  <div class="text-center pt-4 text-white">
                    <p class="text-center pt-4 text-white"> Join this mass moment to save your share of water <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Our counter end -->

<script>
   $(document).ready(function() {
       $('.owl-one').owlCarousel({
           margin: 20,
           autoWidth: false,
           nav: true,
           loop: true,
           items: 3,
           navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
   responsive: {
                       0: {
                           items: 1,
       
                       },
                       912: {
                           items: 2
                       },
                       1024: {
                           items: 3
                       },
           }
   });
   });
</script>
<script>
   var a = 0;
   $(window).scroll(function() {
       var oTop = $('#counter').offset().top - window.innerHeight;
       if (a == 0 && $(window).scrollTop() > oTop) {
           $('.counter-value').each(function() {
               var $this = $(this),
                   countTo = $this.attr('data-count');
               $({
                   countNum: $this.text()
               }).animate({
                       countNum: countTo
                   },
   
                   {
   
                       duration: 2000,
                       easing: 'swing',
                       step: function() {
                           $this.text(Math.floor(this.countNum));
                       },
                       complete: function() {
                           $this.text(this.countNum);
                           //alert('finished');
                       }
   
                   });
           });
           a = 1;
       }
   
   });
</script>