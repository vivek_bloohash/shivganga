<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<!-- <div class="halma2020-div header-image-forest">
   <div class="carousel-caption">
      <h1>Gaun gaun banega Tirath</h1>
   </div>
</div> -->


<div class="container">
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img forest">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>FORESTS ENRICHMENT</p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Guarding the Jungle</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">The tribal communities everywhere in India have a tradition of protecting forests. Most often, there is a demarcated community forest that is preserved and protected.
                    Matavan is a similar tradition in Jhabua. It is a social norm to nurture the trees in Matavan. No individual can make any personal use of the woods of Matavan. Matavan also holds essential inclusion in rituals & festivals of tribal people. They offer the first harvest of their crop to the deity in the Matavan shows the faith tribal people have for Matavan. Shivganga recognized the strength of Matavan tradition and identified it as a medium for implementing Jungle Samvardhan or Forests Enrichment programs.</p>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Deforestation is also contributing to the water crisis problem of Jhabua.
                  </p>
               </div>
               <div class="row">
                  <div class="col-md-12 ">
                     <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-12 text-center green-txt side-by-btn ">
                           <span>Help beating environmental pollution by planting trees</span>
                           <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/3.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Matawan</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        After consecutive meetings in villages, the Shivganga team lists potential sites for forest revival. After rainfall during June and July, the community organises Halma and gathers for digging pits and plantations under the supervision of the Gram Engineers. The area is protected from grazing animals and free movement by erecting a fence around it. The villagers take care of the Matavan during heat waves and otherwise and even plant a new sapling if some saplings die.
                104250 trees planted in 736 villages.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/4.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Swastha Parivar fruiting trees adoption</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Post-Swachh Gaon - Swastha Parivar training, the families adopt a set of 12 saplings - chosen to bring a diversity of nutrients. The 12 trees identified for the purpose are - Mango, Moringa, Shehtoot(Mulberry), Lemon, Guava, Neem, Kari Patta, Papaya, Custard-apple, Amla, Jamun and Bel.
                Till now 19000 plants were distributed to 1650 families in 216 villages. Villagers took the pledge to raise the saplings like their child.

                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/forest-2.jpeg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Community Forests Rights</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        The tribal communities used to live in indistinct oneness with nature - as the natural protector of forests. With the British invasion, the tribal communities were snatched off their forests, livelihood and culture. Even after the Forest Right Act 2006, nothing changed on the ground. Therefore, Shivganga started the 'Vanraj Training Camp' in 2019 to ensure that tribal communities get the rights of their forests. <ul>
                    <li>
                        27 Training Camps were organized in different villages in which 1447 villagers trained

                    </li>
                    <li>03 villages have initiated the legal procedure

                    </li>
                    <li>Shri Milind Thatte (Founder, Vayam Foundation, Palghar) and Shri Girish Kuber (Hitrakhsa Pramukh, Vanvasi Kalyan Ashram); Shri Rahul Mungikar (Joint Director, Bombay Natural History Society ), involved as experts


                </ul>
                The Vanraj Training Camp will be pivotal for the Jangal Samvardhan - Forests Enrichment dimension and open possibilities for livelihood.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- Our counter start -->
<div class="container-fluid p-0 waterpage">
   <div class="wpo-counter-area">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="wpo-counter-grids">
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="110">0</span>+</h2>
                     </div>
                     <p>MATAWANS</p>
                  </div>
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="1300">0</span>+</h2>
                     </div>
                     <p>Villages</p>
                  </div>
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="110000">0</span>+</h2>
                     </div>
                     <p>TREES PLANTED</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Our counter end -->

<div class="bg-34">
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-12 pb-5 text-center">
                <p class="green-txt f-l">The average carbon footprint is 21500 Kg/person.</p>
                <p class="green-txt f-l">
                    CO2 removed by a tree is 21.7 Kg/Year.
                </p>
                <p class="green-txt pb-3 f-l">
                    That makes 1000 trees per person!
                </p>
                <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.owl-one').owlCarousel({
            margin: 20,
            autoWidth: false,
            nav: true,
            loop: true,
            items: 3,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
			responsive: {
                        0: {
                            items: 1,
        
                        },
                        912: {
                            items: 2
                        },
                        1024: {
                            items: 3
                        },
            }
		});
	});
    </script>
    <script>
   var a = 0;
   $(window).scroll(function() {
       var oTop = $('#counter').offset().top - window.innerHeight;
       if (a == 0 && $(window).scrollTop() > oTop) {
           $('.counter-value').each(function() {
               var $this = $(this),
                   countTo = $this.attr('data-count');
               $({
                   countNum: $this.text()
               }).animate({
                       countNum: countTo
                   },
   
                   {
   
                       duration: 2000,
                       easing: 'swing',
                       step: function() {
                           $this.text(Math.floor(this.countNum));
                       },
                       complete: function() {
                           $this.text(this.countNum);
                           //alert('finished');
                       }
   
                   });
           });
           a = 1;
       }
   
   });
</script>