<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<!-- <div class="halma2020-div header-image-forest">
   <div class="carousel-caption">
      <h1>Gaun gaun banega Tirath</h1>
   </div>
</div> -->

<div class="container">
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img youth">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>Community Empowerment</p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Developing tribal youth as Social change agents</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">This fundamental vertical of Shivganga aims at creating community leaders. With the inspiration from Parmartha - working selflessly and passionately for society - equipped with knowledge and skills, they lead the community to take up challenges and resolve community problems.</p>
               </div>
               <div class="row">
                  <div class="col-md-12 ">
                     <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-12 text-center green-txt side-by-btn ">
                           <span>More than 12000 empowered youths, let us present this opportunity to some more!
</span>
                           <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/youth.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Youth Empowerment Camps</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        The curriculum of this 4-day camp includes discussions about the village's problems, the reason behind them and ways to solve them. This also includes group activities and inspiring stories of tribal leaders. The camp instils a feeling of self-esteem and inspires youth to come together to fight the distress.
                        </p>
                     </div>
                  </div>
               </div>
            </div>

            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/5.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Exposure Camps</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        The training gives these future leaders the required exposure to different government systems and alternative models of holistic rural development. It builds their confidence and sparks a dream for their village’s holistic development.
                        </p>
                     </div>
                  </div>
               </div>
            </div>

            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/6.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Gram Engineering Camps</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        The 4-day training includes sessions on techniques of water conservation and afforestation. The training is based on a module prepared by Shivganga in association with faculties of SGSITS, Indore.During the training, the village teams make a water conservation action plan for their village. They list the tasks to be completed through collective efforts. The Gram Engineers work on the construction of reservoirs and other water conservation structures and reviving Matavan - community forests.
                        </p>
                     </div>
                  </div>
               </div>
            </div>

            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/7.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Tadavi Baithak</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Shivganga has been doing community mobilization for a decade. In the process, Shivganga realized the profound influence of the intact traditional social system. Tadvi, the traditional head of the village, plays a crucial role in carrying out all the tribal traditions and has a high social reverence. Thus, Shivganga has begun Tadavi Baithak. The Tadavis of villages come together to discuss and reimagine their role as traditional leaders in holistic rural development in their village.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/8.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Adhyatmik Satsang</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Similar in nature to Tadavi, the Spiritual heads of the community also play a significant role in social awareness & change. Shivganga, to integrate different factions of the community, has begun organising a ‘Adhyatmik Satsang’. In the event, the spiritual leaders come together and deliver a message of enriching natural resources (Jal, Jangal, Jameen, Janwar, Jan) through traditions and values. They quote stories of local deities and heroes and deliver a message to give up personal conflicts and come together to serve the legacy - enriching Mother Nature. Thus, it helps in building a conducive atmosphere for Holistic Rural Development.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Our counter start -->
<div class="container-fluid p-0 waterpage">
   <div class="wpo-counter-area">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="wpo-counter-grids">
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="12500">0</span>+</h2>
                     </div>
                     <p>Youths Trained</p>
                  </div>
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="2215">0</span>+</h2>
                     </div>
                     <p>Gram Engineers</p>
                  </div>
                  <div class="grid">
                     <div id="counter">
                        <h2><span class="odometer odometer-auto-theme counter-value" data-count="796">0</span>+</h2>
                     </div>
                     <p>Adolescence Girls Trained</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Our counter end -->
<div class="bg-34">
    <div class="container">
        <div class="row pb-5">
            <div class="col-md-12 text-center pt-5 green-txt side-by-btn">
                <span>More than 12000 empowered youths, let us present this opportunity to some more!</span>
                <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
            </div>
        </div>
    </div>
</div>
<script>
   var a = 0;
   $(window).scroll(function() {
       var oTop = $('#counter').offset().top - window.innerHeight;
       if (a == 0 && $(window).scrollTop() > oTop) {
           $('.counter-value').each(function() {
               var $this = $(this),
                   countTo = $this.attr('data-count');
               $({
                   countNum: $this.text()
               }).animate({
                       countNum: countTo
                   },
   
                   {
   
                       duration: 2000,
                       easing: 'swing',
                       step: function() {
                           $this.text(Math.floor(this.countNum));
                       },
                       complete: function() {
                           $this.text(this.countNum);
                           //alert('finished');
                       }
   
                   });
           });
           a = 1;
       }
   
   });
</script>