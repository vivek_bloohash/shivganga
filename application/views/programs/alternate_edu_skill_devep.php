<?php
// if(empty($_SESSION['user_name'])) {
// 	redirect('login');
// }
?>
<!-- <div class="halma2020-div header-image-forest">
   <div class="carousel-caption">
      <h1>Gaun gaun banega Tirath</h1>
   </div>
</div> -->

<div class="container">
   <div class="meet-member-wrapper mt-5">
      <div class="row">
         <div class="col-lg-6">
            <div class="meet-member-img emp">
            </div>
         </div>
         <div class="col-lg-6">
            <div class="meet-single-items">
               <div class="content">
                  <div class="subtitle wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                     <p>Entrepreneurship and livelihood generation</p>
                  </div>
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Fostering Young Entrepreneur</h4>
                  <p class="description wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">In the course of the journey with tribal people of jhabua, shivganga explored that the current education which measures everyone equal is not equally beneficial for everyone. Also, this education doesn’t guarantee a livelihood to everyone. so, to respond to the situations of jhabua, shivganga initiated alternative education & skill development programs. Shivganga aims at developing an entrepreneurial ecosystem to foster young entrepreneurs from tribal societies. shivganga in collaboration with tata institute of social sciences, mumbai have initiated an incubation centre for these entrepreneurs. the objective of the centre is to facilitate and catalyze the complete cycle from skill training to livelihood generation</p>
               </div>
               <div class="row">
                  <div class="col-md-12 ">
                     <div class="inner-single-items wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="col-md-12 text-center green-txt side-by-btn ">
                           <span>Get involved with Shivganga to make our villages self-reliant and full of self-esteem!</span>
                           <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate waterpage">DONATE</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="container-fluid mb-5 waterpage">
   <div class="container">
      <div class="administration-section">
         <div class="container custom-container mt-5">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/shape-05.webp" class="admin-shape" alt="">
            <img loading="lazy" src="<?php echo base_url(); ?>assets/img/about-shape.webp" class="admin-shape-02" alt="">
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/13.jpg" alt="">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Organic Farming Training Camp</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        The initiative covers the Jameen Samvardhan - Land Enrichment dimension of Shivganga. The team organizes monthly training of farmers in natural and chemical-free farming and horticulture. The training also includes crop planning and sorting & grading of products. The team has developed a supply chain to sell their produce in Indore through a retail counter.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/14.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">GATI- labs (Grassroots Appropriate Technology Innovation labs)</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Rural India lacks innovation for appropriate technology. Consequently, the traditional tools are losing their relevance, and modern tools often misfit the requirements. Understanding this pain of tribal Jhabua Shivganga has conceptualized and established ‘GATI-labs’ on its Dharampuri Gurukul Campus.
                The Lab trains youths in various skills like welding, electric works, plumbing and equipment handling. The tribal youths of Jhabua have a glaring affinity for experiment and innovation. Hence, complemented by this lab, they will develop skills & tools addressing the needs of Jhabua and rural India.


                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row justify-content-center mb-5">
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items wow animate__ animate__fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                     <div class="administrative-bg">
                        <img class="founder-img" src="<?php echo base_url(); ?>assets/img/15.jpg">
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 col-md-10 col-sm-12">
                  <div class="administration-single-items style-01">
                     <div class="content">
                        <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Bamboo Training</h4>
                     </div>
                     <div class="administration-quotes wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="icon">
                           <img loading="lazy" src="assets/icon/quotes-02.svg" alt="">
                        </div>
                     </div>
                     <div class="content">
                        <p class="wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        Shivganga is providing training to youths in bamboo handicrafts and marketing them through an e-commerce platform, www.jhabuacrafts.com. Shivganga has a full-time residential Bamboo Training Centre at Meghnagar Ashram. The team has begun organising Basic Training Camp in villages. Selected youths go for advanced training in Meghnagar and Indore. It envisions a decentralized model where trainees complete training, go back to their village and train others. The trainees today become trainers tomorrow. With the help of a specially designed kit by Dr Parag Vyas ( alumnus - IDC, IIT Bombay), individuals can produce 70-80 finished products at their homes. After giving the final touch, Jhabua Crafts will market these products through various mediums - online & offline.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>




    <div class="bg-34">
        <div class="container pb-4">
                  <div class="row pt-5 text-center">
                  <h4 class="title wow animate__ animate__fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Sucess Stories</h4>
                <div class="col-md-6">
                  <div class="about-wpo-rural-item">
                     <img src="<?php echo base_url(); ?>assets/img/success1.jpeg" class="img-about3">
                     <p class="the-primary text-justify mt-3">Ramsingh Medha is a marginal farmer. He along with his family used to migrate to sustain himself. Training in organic farming has reduced his input cost. Connecting it to market through Jhabua Naturals has secured him greater income. He now aspires to make his entire land organic and learn growing different crops organically. Jhabua Naturals is a social enterprise by devoted tribal farmers. After training in organic farming, 50+ farmers like Ramsingh Medha are marketing their chemical free farm produce in Indore. The ventures has completed the pilot phase and is ready to embark a long journey. 
                     </p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="about-wpo-rural-item">
                     <img src="<?php echo base_url(); ?>assets/img/success2.jpeg" class="img-about3">
                     <p class="the-primary text-justify mt-3">
                     Kamlesh Damor, is a young creative person. It was never possible for him to express his creativity at construction sites while working as a labourer. Continuous training in Bamboo Handicrafts has made him learn various Bamboo Products. 40+ artisans like Kamlesh are supplying bamboo handicrafts through Jhabua Naturals E-Commerce Platform <a href="www.jhabuacrafts.com">www.jhabuacrafts.com</a> and also providing training to villagers.
                     </p>
                     </span>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-34">
    <div class="container">
        <div class="row pb-5">
            <div class="col-md-12 text-center pt-5 green-txt side-by-btn">
                <span>Let us bring them the education that they need!</span>
                <a href="<?php echo base_url(); ?>donate" class="btn donate-page btn-donate">DONATE</a>
            </div>
        </div>
    </div>
</div>