<?php

class home_Controller extends CI_Controller
{


	public function __construct()
	{

		parent::__construct();
		$this->load->model("blog_model", '', TRUE);
		$this->load->model("about_model", '', TRUE);
		$this->load->model("donate_model", '', TRUE);
		$this->load->model("admin_model", '', TRUE);
		$this->load->library("pagination");
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		// $this->load->helper('sendsms_helper');



	}
	public function login()
	{
		if ($_POST) {

			// print_r($_POST);
			$name = $_POST['user_name'];
			$pswd = $_POST['pswd'];
			$this->admin_model->validat_admin();
		} else {
			$this->load->view('login/login');
		}
	}
	public function user_login()
	{
		if ($_POST) {

			// print_r($_POST);
			$name = $_POST['user_name'];
			$pswd = $_POST['pswd'];
			$this->admin_model->validat_user();
		} else {
			$this->load->view('login/login');
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('user_name');
		redirect('login');
	}
	public function index()
	{

		// $this->load->model("blog_model");
		$data['banner'] = $this->blog_model->get_banner1();
		$data['showblogData'] = $this->blog_model->get_blog();
		$data['showbannerData'] = $this->blog_model->get_banner();
		$data['showcampaignData'] = $this->blog_model->get_campaign();
		$data['showrecentprojectData'] = $this->blog_model->get_recentproject();
		$data['show_extraActivities_Data'] = $this->blog_model->get_extraActivities();


		$this->load->view('templates/header');
		$this->load->view('home', $data);
		$this->load->view('templates/footer');
	}
	public function shivganga_sms()
	{
		$this->load->view('templates/header');
		$this->load->view('sms');
		$this->load->view('templates/footer');
	}
	public function sms1()
	{

		if (sendsms('7007742076', 'hii')) {

			echo 'hii';
		} else {
			echo 'bye';
		}
	}
	function sendsms($mobileno, $message)
	{

		// $message1 = 'hii';
		// $mobileno = '7007742076';
		// $message = urlencode($message);
		$sender = 'SEDEMO';
		$apikey = '6mj40q3t7o89qz93cn0aytz8itxg6641';
		$baseurl = 'https://instantalerts.co/api/web/send?apikey=' . $apikey;

		$url = $baseurl . '&sender=' . $sender . '&to=' . $mobileno . '&message=' . $message;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		curl_close($ch);

		// Use file get contents when CURL is not installed on server.
		if ($response) {
			$response = file_get_contents($url);
			echo 'hii';
		}
	}
	public function mms()
	{

		$this->sendsms('7007742076', 'Hi, this is a test message');
	}
	public function sms()
	{
		//put the name;


		$name = "gyanendra";




		//put the message;


		$message = "test message";




		//put the comma separated mobile number;


		$mobileNumber = '7007742076';




		//put the email id;


		$email = "abc@gmail.com";




		$message = "Hi " . $name . " we received your request. from number is: " . $mobileNumber . " and from email " . $email;




		//put the sender id;


		$senderId = "DEMOOS";


		$serverUrl = "msg.msgclub.net";




		//put the auth key;


		$authKey = "authkey";




		$route = "1";








		function sendsmsPOST($mobileNumber, $senderId, $routeId, $message, $serverUrl, $authKey)


		{


			//Prepare you post parameters


			$postData = array(


				'mobileNumbers' => $mobileNumber,


				'smsContent' => $message,


				'senderId' => $senderId,


				'routeId' => $routeId,


				"smsContentType" => 'english'


			);


			$data_json = json_encode($postData);




			$url = "http://" . $serverUrl . "/rest/services/sendSMS/sendGroupSms?AUTH_KEY=" . $authKey;




			// init the resource


			$ch = curl_init();




			curl_setopt_array($ch, array(


				CURLOPT_URL => $url,


				CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen($data_json)),


				CURLOPT_RETURNTRANSFER => true,


				CURLOPT_POST => true,


				CURLOPT_POSTFIELDS => $data_json,


				CURLOPT_SSL_VERIFYHOST => 0,


				CURLOPT_SSL_VERIFYPEER => 0


			));




			//get response


			$output = curl_exec($ch);




			//Print error if any


			if (curl_errno($ch)) {


				echo 'error:' . curl_error($ch);
			}


			curl_close($ch);


			return $output;
		}
	}
	public function aboutshivganga()
	{
		$data['show_shivganga_about_usData'] = $this->about_model->get_shivganga_about_us();
		$this->load->view('templates/header');
		$this->load->view('about/aboutshivganga', $data);
		$this->load->view('templates/footer');
	}
	public function aboutjhabua()
	{
		$this->load->view('templates/header');
		$this->load->view('about/aboutjhabua');
		$this->load->view('templates/footer');
	}
	public function award()
	{
		$this->load->view('templates/header');
		$this->load->view('about/award');
		$this->load->view('templates/footer');
	}
	public function waterconservation()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/waterconservation');
		$this->load->view('templates/footer');
	}
	public function programforest()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/programforest');
		$this->load->view('templates/footer');
	}
	public function joinus()
	{
		$this->load->view('templates/header');
		$this->load->view('joinus');
		$this->load->view('templates/footer');
	}
	public function save_join_us_data()
	{
		$contact_us = array(
			'email' => $_POST['email'],
			'name' => $_POST['name'],
			'contact' => $_POST['contact'],
			'subject' => $_POST['subject'],
			'message' => $_POST['message'],
			'created_at' => date('d-m-Y h:i:s')
		);
		$res1 = $this->db->insert('contact_us', $contact_us);

		// echo $res;die;
		$data = '';
		$res =  $this->send_mail_contact($contact_us);
		if ($res == '1') {
			$_SESSION['res'] = "Your query submmited successfully, We will contact you shortly!";
			header("Location:" . base_url('Home_Controller/joinus'));
		}
	}
	function send_mail_contact($data)
    {
		
        // $to = $email;
        $to = "nktiwarippp@gmail.com";
        $date = date("F jS, Y", strtotime($data['created_at']));
        $this->load->library('phpmailer_lib');
        $mail = $this->phpmailer_lib->load();
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'krharsh.sj@gmail.com';
        $mail->Password = '2007$hiva';
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPDebug = 2;
        $mail->Port     = 465;

        $mail->setFrom($mail->Username, 'Shivganga');
        if (is_array($to)) {
            foreach ($to as $row) {
                $mail->addBCC($row, "");
            }
        } else {
            $mail->addAddress($to, "");
        }
        $mail->Subject = 'Welcome In Shivganga';
        $mail->isHTML(true);
        $email_temp = '
        
        <html>
        <head>
        <style>
        
        @media screen and (max-width: 767px) {
            .main-div{
                width: 100% !important;
            }

        }
        </style>
        </head>
			<body style=" margin: 0px; font-family: "Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;">
				<table class="table">
					<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Contact</th>
						<th>Subject</th>
						<th>Message</th>
						<th>Date</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>'. $data['name'].'</td>
						<td>'. $data['email'].'</td>
						<td>'. $data['contact'].'</td>
						<td>'. $data['subject'].'</td>
						<td>'. $data['message'].'</td>
						<td>'.$date.'<td>
					</tr>
					
					</tbody>
				</table>
			</body>
        </html>
        ';
        $mail->SMTPDebug = true;
        $mail->Body =  $email_temp;
        if ($mail->send()) {

            echo "The mail has been send successfully.";
            return 1;
        }
    }
	public function student_involve()
	{
		$this->load->view('templates/header');
		$this->load->view('student_involment.php');
		$this->load->view('templates/footer');
	}
	public function image_vs_reality_internship()
	{
		$this->load->view('templates/header');
		$this->load->view('image_vs_reality_internship');
		$this->load->view('templates/footer');
	}
	public function youth_Empower()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/youthempower');
		$this->load->view('templates/footer');
	}
	public function alternate_edu_skill_devep()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/alternate_edu_skill_devep');
		$this->load->view('templates/footer');
	}

	public function levelihood_entrepreneurship()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/levelihood_entrepreneurship');
		$this->load->view('templates/footer');
	}
	public function women_empowerment()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/women_empowerment');
		$this->load->view('templates/footer');
	}

	public function connecting_communities()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/connecting_communities');
		$this->load->view('templates/footer');
	}
	public function tribal_culture()
	{
		$this->load->view('templates/header');
		$this->load->view('programs/tribal_culture');
		$this->load->view('templates/footer');
	}
	public function events()
	{
		$this->load->view('templates/header');
		$this->load->view('events');
		$this->load->view('templates/footer');
	}
	public function blogs()
	{

		$data['showblogData'] = $this->admin_model->get_blog2_data();
		$this->load->view('templates/header');
		$this->load->view('blogs', $data);
		$this->load->view('templates/footer');
	}
	public function blogfullview($slug)
	{

		$data['showblogData'] = $this->blog_model->blogfullview2($slug);
		$data['latest_post'] = $this->blog_model->get_latest_blog();

		$data['blog_comment'] = $this->blog_model->get_blog_comment($slug);

		// print_r($data);die;
		$this->load->view('templates/header');
		$this->load->view("blogfullview", $data);
		$this->load->view('templates/footer');
	}
	public function single_blog_view($slug)
	{

		$data['showblogData'] = $this->blog_model->blogfullview($slug);
		// print_r($data);die;
		$this->load->view('templates/header');
		$this->load->view("single-show-program", $data);
		$this->load->view('templates/footer');
	}
	public function campaignfullview($slug)
	{

		$data['showcampaignData'] = $this->blog_model->campaignfullview($slug);
		// print_r($data);die;
		$this->load->view('templates/header');
		$this->load->view("campaignfullview", $data);
		$this->load->view('templates/footer');
	}
	public function projectsaadhtalab()
	{
		$this->load->view('templates/header');
		$this->load->view('projectsaadhtalab');
		$this->load->view('templates/footer');
	}
	public function view_all_program()
	{
		$config = array();
		$config["base_url"] = base_url() . "home_Controller/view_all_program";
		$config["total_rows"] = $this->blog_model->record_count();
		$config["per_page"] = 8;
		$config["uri_segment"] = 3;

		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["showblogData"] = $this->blog_model->get_program($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();

		$this->load->view('templates/header');
		$this->load->view("view-all-program", $data);
		$this->load->view('templates/footer');
		// $data['showblogData'] = $this->blog_model->get_program();
		// $this->load->view('view-all-program',$data);
	}
	public function donate($title = '')
	{
		// $title = $this->site->uri->segment(1);

		$data['title'] = $title;
		$data['showdonateData'] = $this->donate_model->get_donation_data();
		$this->load->view('templates/header');
		$this->load->view('donate', $data);
		$this->load->view('templates/footer');
	}
	public function showprice()
	{

		$q = intval($_GET['q']);

		if ($q == '10') {

			echo '<div class="container"><h5 class="we-make pt-5">Pick a donation amount</h5></div>';

			echo '
			
			<div class="">
			<div class="pick-amount col-md-12 col-lg col-xl-12 other-amount" >
			<input type="number" class="other-amount-input"  id="other-amount-input" value="" min="0" placeholder="Other amount" required>
			</div>
			</div>';
		} else {


			if ($q == '0') {

				echo "Select Donate for the cause ";
				exit;
			}
			if ($q !== 'null') {
				$data = $this->donate_model->get_price($q);


				echo '<div class="container"><h5 class="we-make pt-5">Pick a donation amount</h5></div>';
				foreach ($data as $donate) {

					$amount = explode(',', $donate->price);
					$count = 1;
					foreach ($amount as $value) {

						echo '
					
					<div class="pick-amount col-md col-lg col-xl">
					<input type="radio" class="donation-radio-btn" name="amount" value="' . $value . '" id="id-amount-' . $count . '" >
					<label for="id-amount-' . $count . '" class="donation-label">';
						echo $value;
						echo '</label>
					</div>
					';
						$count++;
					}
				}
				echo '<div class="pick-amount col-md-3 col-lg col-xl-3 other-amount">
			<input type="radio" class="donation-radio-btn other" name="amount" id="id-amount-other">
			<input type="number" class="other-amount-input" id="other-amount-input" value="" min="0" placeholder="Other amount">
			</div>';
			}
		}
	}
	public function showdoner()
	{


		$data['doners'] = $this->donate_model->get_doner();

		$this->load->view('templates/header');
		$this->load->view('showdoner', $data);
		$this->load->view('templates/footer');
	}
	public function halma2020()
	{
		$this->load->view('templates/header');
		$this->load->view('halma2020');
		$this->load->view('templates/footer');
	}
	public function eventsviewmore()
	{
		$this->load->view('templates/header');
		$this->load->view('eventsviewmore');
		$this->load->view('templates/footer');
	}
	public function contribute_now()
	{

		$data['contribute'] = $this->donate_model->get_contribute_amount();
		$this->load->view('templates/header');
		$this->load->view('contribute', $data);
		$this->load->view('templates/footer');
	}
}