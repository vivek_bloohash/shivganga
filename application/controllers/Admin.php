<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');



use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Admin extends CI_Controller
{



    public function __construct()
    {
        parent::__construct();

        $this->load->model("admin_model", '', TRUE);
        $this->load->model("blog_model", '', TRUE);
        $this->load->model("donate_model", '', TRUE);
        date_default_timezone_set("Asia/Kolkata");
        $data['AdminData'] = $this->admin_model->get_admin();

    }
    public function index()
    {
        echo $this->uri->segment(3);
        $data['AdminData'] = $this->admin_model->get_admin();
        $data['total_donation_amount'] = $this->admin_model->get_total_donation_amount();
        $data['doners_data'] = $this->admin_model->get_doners();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/index', $data);
        $this->load->view('admin/template/footer');
    }
    public function create_blog()
    {

        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');

        $this->load->view('admin/blog/create_blog');
        $this->load->view('admin/template/footer');
    }


    public function createExcel()
    {
        // print_r(date("l jS \of F Y h:i:s A"));die;
        $fileName = 'Campaign-Payment-Report-'.date("l jS \of F Y h:i:s A").'.xlsx';
        $paymentData = $this->admin_model->get_payment();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        
        $sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'Campaign');
        $sheet->setCellValue('C1', 'Payment Id');
                $sheet->setCellValue('D1', 'Payment Method');

        $sheet->setCellValue('E1', 'Name');
        $sheet->setCellValue('F1', 'Amount');
        $sheet->setCellValue('G1', 'Email');
        $sheet->setCellValue('H1', 'Contact');
        $sheet->setCellValue('I1', 'Address');
        $sheet->setCellValue('J1', 'Pan No');
        $sheet->setCellValue('K1', 'Payment Status');
        $sheet->setCellValue('L1', 'Date');

        $rows = 2;
        foreach ($paymentData as $row) {
            $sheet->setCellValue('A' . $rows, $row->id);
            $campaign_title = $this->admin_model->get_campaign_by_id($row->campaign_id);
            $sheet->setCellValue('B' . $rows, $campaign_title);
            if ($row->payment_type == "Razorpay") {
                $sheet->setCellValue('C' . $rows,$row->payment_id);
            } else {
                $sheet->setCellValue('C' . $rows, $row->order_id);
            }
                        $sheet->setCellValue('D' . $rows, $row->payment_type);
            $sheet->setCellValue('E' . $rows, $row->name);
            $sheet->setCellValue('F' . $rows, $row->amount);
            $sheet->setCellValue('G' . $rows, $row->email1);
            $sheet->setCellValue('H' . $rows, $row->contact_no1);
            $sheet->setCellValue('I' . $rows, $row->address);
            $sheet->setCellValue('J' . $rows, $row->pan_no);


            if ($row->is_success == 1) {
                $sheet->setCellValue('K' . $rows, "Success");
            } else {
                $sheet->setCellValue('K' . $rows, "Failed");
            }
            
            
            $sheet->setCellValue('L' . $rows, $row->created_at);
            
            
            $rows++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save("uploads/campaign-payment-report/" . $fileName);
        header("Content-Type: application/vnd.ms-excel");
        redirect(base_url() . "/uploads/campaign-payment-report/" . $fileName);
        unlink(base_url() . "/uploads/campaign-payment-report" . $fileName);
    }

    public function save_blog()
    {
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));
        $filename = $_FILES['filename']['name'];
        $temp = $_FILES['filename']['tmp_name'];
        $path = 'uploads/blog/' . $filename;
        move_uploaded_file($temp, $path);
        $data = array(
            'title' => $_POST['title'],
            'writer_name' => $_POST['writer'],
            'slug' => $slug_title,
            'content' => $_POST['content'],
            'filename' => $path,
            'created_at' => date('d-m-Y h:i:s a')
        );

        $res = $this->db->insert('blog', $data);
        $this->create_blog();
    }

    public function show_blog()
    {
        $data['AdminData'] = $this->admin_model->get_admin();
        $data['blog_data2'] = $this->admin_model->get_blog2_data();

        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');

        $this->load->view('admin/blog/show_blog', $data);
        $this->load->view('admin/template/footer');
    }
    public function delete_blog($id)
    {
        $this->admin_model->delete_blog($id);
        redirect('admin/show_blog', 'location');
    }
    public function edit_blog($id)
    {
        
        $data['AdminData'] = $this->admin_model->get_admin();
        $data['blog2_data'] = $this->admin_model->get_edit_blog_data($id);
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/blog/edit_blog', $data);
        $this->load->view('admin/template/footer');
    }
    public function update_blog_data($id)
    {
        // print_r($_FILES);
        // die;
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/blog/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/blog/" . $name)) {
                $update_blog = array(


                    'title' => $this->input->post('title'),
                    'writer_name' => $this->input->post('writer'),
                    'slug' => $slug_title,
                    'content' => $this->input->post('content'),
                    'filename' => $ImgPath,
                    'created_at' => date('d-m-Y h:i:s a')

                );
                // print_r($update_blog);
                // die;
                // print_r($id);die;

                $this->db->where('id', $id);
                $this->db->update('blog', $update_blog);
                echo 'Blog has successfully been updated';
            }
        }
        redirect('admin/show_blog', 'location');
    }

    public function leave_a_comment()
    {





        // $filename = $_FILES['filename']['name'];
        // $temp = $_FILES['filename']['tmp_name'];

        // $path = 'uploads/blog/' . $filename;
        // move_uploaded_file($temp, $path);
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));
        $comment = array(

            'name' => $_POST['name'],
            'slug' => $slug_title,
            'email' => $_POST['email'],
            'comment' => $_POST['comment'],
            // 'filename' => $path,
            'created_at' => date('d-m-Y h:i:s a')


        );

        // print_r($comment);die;
        $res  = $this->db->insert('blog_comments', $comment);

        // = $this->db->insert('blog_comments', $data);
        // echo $res;
        // die;
        redirect('Home_Controller/blogfullview/' . $slug_title . '', 'location');
    }

    public function show_blog_comment()
    {

        $data['blog_comment'] = $this->blog_model->get_comment();
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/blog/show_blog_comment', $data);
        $this->load->view('admin/template/footer');
    }

    public function delete_blog_comment($id)
    {

        $this->blog_model->delete_blog_comment($id);
        redirect('admin/show_blog_comment', 'location');
    }
    public function create_extra_activites()
    {

        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/extra-activities/create-extra-activities');
        $this->load->view('admin/template/footer');
    }
    public function extra_activites()
    {

        $user_id = ($_SESSION['id']);
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/extra-activities/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/extra-activities/" . $name)) {
                $program = array(

                    'slug' => $slug_title,
                    'title' => $this->input->post('title'),
                    'des' => $this->input->post('content'),
                    'image' => $ImgPath,
                    'user_id' => $user_id

                );

                $this->db->insert('extra-activities', $program);
                echo json_encode($program);
                // print_r($blog);die;       
            }
        }
    }
    public function createrecentproject()
    {

        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/recent-project/createrecentprojects');
        $this->load->view('admin/template/footer');
    }
    public function show_recent_project()
    {

        $data['recent_project_data'] = $this->blog_model->get_recentproject();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/recent-project/show-recent-project', $data);
        $this->load->view('admin/template/footer');
    }

    public function show_banner()
    {

        $data['showbannerData'] = $this->blog_model->get_banner();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/bannner/show-banner', $data);
        $this->load->view('admin/template/footer');
    }
    public function show_contact_us()
    {

        $data['show_contact_us'] = $this->blog_model->get_contact_us();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/contact_us/show_contact_us', $data);
        $this->load->view('admin/template/footer');
    }
    public function donor_request()
    {

        $data['request'] = $this->admin_model->donor_request();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/view_donors_request.php', $data);
        $this->load->view('admin/template/footer');
    }
    public function delete_banner($id)
    {

        $this->db->delete('banner', array('id' => $id));
        redirect('admin/show_banner', 'location');
    }
    public function delete_contact_us($id)
    {

        $this->db->delete('contact_us', array('id' => $id));
        redirect('admin/show_contact_us', 'location');
    }
    public function show_activities()
    {

        $data['show_extraActivities_Data'] = $this->blog_model->get_extraActivities();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/extra-activities/show_extra_data', $data);
        $this->load->view('admin/template/footer');
    }
    public function adminLogin()
    {
        $this->load->view('admin/login.php');
    }
    public function login()
    {
        if ($_POST) {

            // print_r($_POST);
            $name = $_POST['user_name'];
            $pswd = $_POST['pswd'];
            $this->login_model->validat_user();
        } else {
            $this->load->view('home');
        }
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('admin/adminLogin');
    }
    public function delete_campaign($id)
    {

        $this->admin_model->delete_campaign($id);
        redirect('admin/view_campaign', 'location');
    }
    public function adminMain()
    {

        $this->load->view('admin/index.php');
    }

    public function donations()
    {

        $data['total_donation_amount'] = $this->admin_model->get_total_donation_amount();
        $this->load->view('admin/donors', $data);
    }
    function fetch_user()
    {
        $this->load->model("admin_model");
        $fetch_data = $this->admin_model->make_datatables();

        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            if ($row->id > 22999) {

                $new = substr($row->id, 2);

                $string = "W/2021-22/";

                $sub_array[] =  '<td>' .  $string . $new . '</td>';
            } else {

                $sub_array[] =  '<td>' . $row->id . '</td>';
            }
            $sub_array[] = $row->payment_id;
            $sub_array[] = $row->name;
            $sub_array[] = $row->amount;
            $sub_array[] = $row->email1;
            $sub_array[] = $row->contact_no1;
            if ($row->address == 'NA') {
                $sub_array[] =  '<td><a style="color:red" href="">N/A</a></td>';
            } else {
                $sub_array[] = ' <td>' . $row->address . '</td>';
            }
            if ($row->pan_no == 'NA') {
                $sub_array[] =  '<td><a style="color:red" href="">N/A</a></td>';
            } else {
                $sub_array[] = ' <td>' . $row->pan_no . '</td>';
            }
            if ($row->is_success == '1') {
                $sub_array[] =  '<td ><a style="color:green" href="">Success</a></td>';
            } else {
                $sub_array[] = '	<td ><a style="color:red" href="">Failed<a/></td>';
            }
            if ($row->updated_at == '1') {
                $sub_array[] =  '<td ><a style="color:green" href="">Delivered</a></td>';
            } else {
                $sub_array[] = '	<td ><a style="color:red" href="">UNDelivered<a/></td>';
            }

            $sub_array[] = $row->created_at;
            if ($row->pan_no == 'NA' || $row->address == 'NA' || $row->name == NULL) {
                $sub_array[] =  '<td>
                    <input type="hidden" value="' . $row->id . '" class="hash">
                    <input type="button"  onclick="send(this)" style="background-color:white; color:red" class="btn btn-success" value="Update">
                </td>';
            } else {
                $sub_array[] =  '<td> <a style="color:green" href="">Updated</a>
                </td>';
            }


            $sub_array[] = '<a target="_blank" href="' . base_url('admin/download_receipt') . '?id=' . $row->id . '">Print</a>
            <a href="' . base_url('admin/send_receipt') . '?id=' . $row->id . '">Send</a>';
            $data[] = $sub_array;
        }
        $output = array(
            "draw"                    =>     intval($_POST["draw"]),
            "recordsTotal"          =>      $this->admin_model->get_all_data(),
            "recordsFiltered"     =>     $this->admin_model->get_filtered_data(),
            "data"                    =>     $data
        );
        echo json_encode($output);
    }

    public function view_details()
    {

        $id = $_GET['id'];

        $data['customer_details'] = $this->admin_model->get_customer_details($id);

        $html = '
        <div class="container">
        <div class="row">
        
        
        <form action="' . base_url() . 'admin/update_pan_address" method="POST">
       
        
        
       
        
            <h1 class="rale-font3  mob-font">Enter Details of ' . $data['customer_details'][0]->name . '</h1></br>
            Name:
            <input type="text" style="width:550px" class=" form-control" value="' . $data['customer_details'][0]->name . '" id="name" placeholder="Enter Enter Name" name="name" required>
            </br>
            Pan No:
            <input type="text" style="width:550px" class=" form-control" value="' . $data['customer_details'][0]->pan_no . '" id="pan_no" placeholder="Enter Pan No" name="pan_no" required>
            </br>
            Address: 
            <textarea style="width:550px" class=" form-control" id="address" placeholder="Enter Address" name="address" required>' . $data['customer_details'][0]->address . '</textarea><br>
            <input type="hidden" value="' . $data['customer_details'][0]->email1 . '" name="email">
        
                <input type="Submit" class=" text-left ml-5 btn btn-success" Value="Update"><br>

        </form>
        </div>
        </div>
        ';
        echo $html;
    }
    function update_pan_address()
    {

        // print_r($_POST);
        // die;
        $otherdb = $this->load->database('otherdb', TRUE);



        $data = array(
            'name' => $_POST['name'],
            'pan_no' => $_POST['pan_no'],
            'address' => $_POST['address']

        );
        $otherdb->where('email1', $_POST['email']);
        $otherdb->update('payment', $data);
        $html = '<script>
        alert("Successfully Updated!")

       
            window.location.href = "' . base_url('admin') . '";

         

        </script>';
        echo $html;
    }
    function download_receipt()
    {

        $receipt_id = $this->input->get('id');


        $data['row'] = $this->admin_model->get_customer_details($receipt_id);


        $this->load->view('admin/reciept/receipt_temp', $data);



        $html = $this->output->get_output();

        $this->load->library('pdf');
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $this->dompdf->stream("Shivganga-Receipt.pdf", array("Attachment" => 0));
    }
    function send_receipt()
    {

        $id = $_GET['id'];


        $_SESSION['success'] = 'mail send';
        // print_r($messge);
        // die;



        $data['details'] =  $this->admin_model->get_customer_details($id);



        $email = $data['details'][0]->email1;
        $res =  $this->send_receipt_mail($data, $email);
        if ($res == 1) {
            $html = '<script>
            alert("Mail Sent Successfully to ' . $data['details'][0]->name . '!")

           
                window.location.href = "' . base_url('admin') . '";

             

            </script>';
        }
        echo $html;
        echo $res;
    }
    function send_receipt_mail($data, $email)
    {


        $to = $email;
        // $to = "gs8400791@gmail.com";

        if ($data['details'][0]->payment_id == 'offline') {

            $method = '<h3>Donation : ' . $data['details'][0]->payment_id . '</h3>';
        } else {

            $method = '<h3>Donation ID : ' . $data['details'][0]->payment_id . '</h3>';
        }

        if ($data['details'][0]->id > 22999) {

            $new = substr($data['details'][0]->id, 2);

            $string = "W/2021-22/";

            $id = $string . $new;
        } else {

            $id = $data['details'][0]->id;
        }



        $date = date("F jS, Y", strtotime($data['details'][0]->created_at));
        $this->load->library('phpmailer_lib');
        $mail = $this->phpmailer_lib->load();
        // $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'krharsh.sj@gmail.com';
        $mail->Password = '2007$hiva';
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPDebug = 2;
        $mail->Port     = 465;

        $mail->setFrom($mail->Username, 'Shivganga');
        if (is_array($to)) {
            foreach ($to as $row) {
                $mail->addBCC($row, "");
            }
        } else {
            $mail->addAddress($to, "");
        }

        $mail->Subject = 'Welcome In Shivganga';


        $mailContent = 'body';


        $mail->isHTML(true);
        $email_temp = '
        
        <html>
        <head>
        <style>
        
        @media screen and (max-width: 767px) {
            .main-div{
                width: 100% !important;
            }

        }
        </style>
        </head>
        <body style=" margin: 0px; font-family: "Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;">
        <div class="main-div" style="margin: 0 auto; text-align: center; width: 60%;">
            <div class="heading-div">
                <h1 style="margin-top: 50px; margin-bottom: 50px;">Your Donation Receipt</h1>
            </div>
            <div style="border: 8px solid grey;">
                <div style="border: 4px solid grey; margin: 20px; padding-bottom: 20px;">
                    <div style ="padding: 15px;">
                        <div class="logo-div" style="margin-top: 20px;">
                            <img src="https://shivgangajhabua.org/wp-content/uploads/2019/03/sj.png" />
                        </div>
                        <div class="SSGP-text-div">
                            <h3 style="color: #83D745; margin-bottom: 10px;">Shivganga Samragra Gramvikas Parishad</h3>
                        </div>
                    </div> 
                    <div style="border-bottom: 1px solid grey;"></div>
                    <div style="padding: 15px;">
                        <p style="font-size: 14px;">IT Exemption U/S 80G, F.No. CIT-I/Ind/Tech/80G/08/09-10, Dated 24/12/2009, Sl.No. 51/2009-10 Valid from 01/04/09, Extended from AY 2021-22 to AY 2023-24 with Provisional Approval Number: AADAS4039HF20206 .<br>PAN: AADAS4039H</p>
                        <h3><i>This is to certify that Shri/Smt.</i> Name: ' . $data['details'][0]->name . ' </h3>
                        <h3>Address: ' . $data['details'][0]->name . '<br>' . $data['details'][0]->address . '</h3>
                        <h3>Pan no : ' . $data['details'][0]->pan_no . ' </h3>
                        <h3><a href="#" target="_blank"></a><i> made a doantion of</i> Amount:&#8377;' . $data['details'][0]->amount . ' on Date:<span style="font-size: 14px;">' . $date . '</span></h3>

        

                        ' . $method . '
                        <h3>Receipt ID : ' . $data['details'][0]->id . ' </h3>
      
                    </div>
                    <div style="margin-top: 40px;">
                        <p style="margin-bottom: 0px;">
                            THANK YOU!
                        </p>
                            <p style="margin-top: 0px;">
                                WE WILL MAKE YOU CONTRIBUTION COUNT.
                             </p>
                    </div>
                    <div style="margin-top: 30px; text-align: right; margin-right: 20px;">
                        <p>-Issued by Treasurer, Shivganga</p>
                    </div>
                </div>
            </div>
            <div style="margin-top: 50px; margin-bottom: 50px;">
                <a href="https://shivgangajhabua.org/">Shivganga</a>
            </div>
        </div>
        </body>
        </html>
       
        
        ';


        $this->load->view('admin/reciept/attachment', $data);



        $html = $this->output->get_output();

        $this->load->library('pdf');
        $this->dompdf->loadHtml($html);
        $this->dompdf->render();

        $filename = 'Shivganga-Receipt-' . $data['details'][0]->id . '.pdf';

        file_put_contents('Shivganga-Receipt-' . $data['details'][0]->id . '.pdf', $this->dompdf->Output());

        $path = FCPATH . $filename;

        $mail->addAttachment($path, 'Shivganga-Receipt-' . $data['details'][0]->id . '.pdf');



        $mail->SMTPDebug = false;

        $mail->Body =  $email_temp;
        if ($mail->send()) {
            // $this->session->set_flashdata('msg', 'Password has been sent to your email address.');

            // echo "The mail has been send successfully.";

            $update_id = $data['details'][0]->id;
            $otherdb = $this->load->database('otherdb', TRUE);

            $otherdb->where('id', $update_id);
            $otherdb->update('payment', array('updated_at' => '1'));

            if (unlink($path)) {
                // echo 'deleted successfully';
            } else {
                echo 'errors occured';
            }

            return 1;
            // $this->receipt();
        }
    }
    public function create_program()
    {

        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/programs/create_program');
        $this->load->view('admin/template/footer');
    }
    public function create_campaign()
    {

        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/campaign/create_campaign');
        $this->load->view('admin/template/footer');
    }
    public function view_campaign()
    {
        $data['view_campaign'] = $this->admin_model->get_campaign();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/campaign/view_campaign', $data);
        $this->load->view('admin/template/footer');
    }
    public function view_payment()
    {
        $data['view_payment'] = $this->admin_model->get_payment();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/campaign/view_payment', $data);
        $this->load->view('admin/template/footer');
    }
    public function save_compaign()
    {
        // print_r($_POST);die;
        if ($_POST['donation_type'] == 'price') {
            $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));

            if (isset($_FILES['filename'])) {
                $name = $_FILES['filename']['name'];
                $tmp_name = $_FILES['filename']['tmp_name'];
                $type = $_FILES['filename']['type'];
                $size = $_FILES['filename']['size'];
                $error = $_FILES['filename']['error'];
                $ImgPath = "uploads/campaign/" . $name;
                if (move_uploaded_file($tmp_name, "uploads/campaign/" . $name)) {
                    $campaign = array(
                        'title' => $this->input->post('title'),
                        'custom_Slug' => $this->input->post('custom_Slug'),
                        'content' => $this->input->post('content'),
                        'image' => $ImgPath,
                        'slug' => $slug_title,
                        'price' => $this->input->post('price'),
                        'donation_type' => $this->input->post('donation_type'),
                        'piece_title' => '----',
                        'end_date' => $this->input->post('end_date'),
                        'user_id' => '1',
                        'target' => $this->input->post('target'),
                        'created_at' => date('d-m-Y h:i:s')

                    );
                    $this->db->insert('campaign', $campaign);
                }
            }
            redirect('admin/view_campaign', 'location');
        } else {

            $addmore = $this->input->post('addmore');

            $item_quantity = implode(',', $addmore);
            // echo ($item_quantity);
            // die;
            $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));

            if (isset($_FILES['file'])) {
                $name = $_FILES['file']['name'];
                $tmp_name = $_FILES['file']['tmp_name'];
                $type = $_FILES['file']['type'];
                $size = $_FILES['file']['size'];
                $error = $_FILES['file']['error'];
                $ImgPath = "uploads/campaign/" . $name;
                if (move_uploaded_file($tmp_name, "uploads/campaign/" . $name)) {
                    $aboutus = array(


                        'title' => $this->input->post('title'),
                        'custom_Slug' => $this->input->post('custom_Slug'),

                        'content' => $this->input->post('content'),
                        'image' => $ImgPath,
                        'slug' => $slug_title,
                        'price' => $this->input->post('price_per_piece'),
                        'donation_type' => $this->input->post('donation_type'),
                        'piece_title' => $this->input->post('piece_title'),
                        'item_quantity' => $item_quantity,
                        'end_date' => $this->input->post('end_date'),
                        'user_id' => '1',
                        'target' => $this->input->post('target'),
                        'created_at' => date('d-m-Y h:i:s')

                    );
                    $this->db->insert('campaign', $aboutus);
                }
            }

            redirect('admin/view_campaign', 'location');
        }
    }


    public function edit_campaign($id)
    {
        $data['edit_campaign'] = $this->admin_model->get_campaign_by_id($id);

        if ($data['edit_campaign'][0]->donation_type == "price") {
            $data['AdminData'] = $this->admin_model->get_admin();
            $this->load->view('admin/template/header', $data);
            $this->load->view('admin/template/side-bar');
            $this->load->view('admin/campaign/edit_campaign', $data);
            $this->load->view('admin/template/footer');
        } else {

            $data['AdminData'] = $this->admin_model->get_admin();
            $this->load->view('admin/template/header', $data);
            $this->load->view('admin/template/side-bar');
            $this->load->view('admin/campaign/edit_campaign_per_price', $data);
            $this->load->view('admin/template/footer');
        }
    }
    public function update_campaign()
    {
        // print_r($_POST);
        // die;
        if ($_POST['donation_type'] == 'price') {


            $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));
            //blank file
            if ($_FILES['filename']['name'] == "" & $_POST['content'] == "") {


                $campaign = array(
                    'title' => $this->input->post('title'),
                    'custom_Slug' => $this->input->post('custom_Slug'),

                    // 'content' => $this->input->post('content'),
                    'slug' => $slug_title,
                    'price' => $this->input->post('price'),
                    'donation_type' => $this->input->post('donation_type'),
                    'piece_title' => '----',
                    'end_date' => $this->input->post('end_date'),
                    'user_id' => '1',
                    'target' => $this->input->post('target'),
                    'created_at' => date('d-m-Y h:i:s')

                );
                $this->db->where('slug', $slug_title);
                $this->db->update('campaign', $campaign);

                redirect('admin/view_campaign', 'location');
            } else if ($_POST['content'] == "") {

                $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));
                $name = $_FILES['filename']['name'];
                $tmp_name = $_FILES['filename']['tmp_name'];
                $type = $_FILES['filename']['type'];
                $size = $_FILES['filename']['size'];
                $error = $_FILES['filename']['error'];
                $ImgPath = "uploads/campaign/" . $name;
                if (move_uploaded_file($tmp_name, "uploads/campaign/" . $name)) {
                    $campaign = array(
                        'title' => $this->input->post('title'),
                        'custom_Slug' => $this->input->post('custom_Slug'),

                        'image' => $ImgPath,
                        'slug' => $slug_title,
                        'price' => $this->input->post('price'),
                        'donation_type' => $this->input->post('donation_type'),
                        'piece_title' => '----',
                        'end_date' => $this->input->post('end_date'),
                        'user_id' => '1',
                        'target' => $this->input->post('target'),
                        'created_at' => date('d-m-Y h:i:s')

                    );
                    $this->db->where('slug', $slug_title);
                    $this->db->update('campaign', $campaign);
                    redirect('admin/view_campaign', 'location');
                }
            } else if ($_FILES['filename']['name'] == "") {

                $campaign = array(

                    'slug' => $slug_title,
                    'price' => $this->input->post('price'),
                    'content' => $this->input->post('content'),

                    'donation_type' => $this->input->post('donation_type'),
                    'piece_title' => '----',
                    'end_date' => $this->input->post('end_date'),
                    'user_id' => '1',
                    'target' => $this->input->post('target'),
                    'created_at' => date('d-m-Y h:i:s')

                );
                $this->db->where('slug', $slug_title);
                $this->db->update('campaign', $campaign);
                redirect('admin/view_campaign', 'location');
            } else {


                $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));
                $name = $_FILES['filename']['name'];
                $tmp_name = $_FILES['filename']['tmp_name'];
                $type = $_FILES['filename']['type'];
                $size = $_FILES['filename']['size'];
                $error = $_FILES['filename']['error'];
                $ImgPath = "uploads/campaign/" . $name;
                if (move_uploaded_file($tmp_name, "uploads/campaign/" . $name)) {
                    $campaign = array(
                        'title' => $this->input->post('title'),
                        'custom_Slug' => $this->input->post('custom_Slug'),

                        'content' => $this->input->post('content'),

                        'image' => $ImgPath,
                        'slug' => $slug_title,
                        'price' => $this->input->post('price'),
                        'donation_type' => $this->input->post('donation_type'),
                        'piece_title' => '----',
                        'end_date' => $this->input->post('end_date'),
                        'user_id' => '1',
                        'target' => $this->input->post('target'),
                        'created_at' => date('d-m-Y h:i:s')

                    );
                    $this->db->where('slug', $slug_title);
                    $this->db->update('campaign', $campaign);
                    redirect('admin/view_campaign', 'location');
                }
            }
        } else {

            $addmore = $this->input->post('addmore');

            $item_quantity = implode(',', $addmore);
            // echo ($item_quantity);
            // die;
            $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));

            if ($_FILES['filename']['name'] == "" && $_POST['content'] == "") {

                $campaign = array(


                    'title' => $this->input->post('title'),
                    'custom_Slug' => $this->input->post('custom_Slug'),

                    // 'content' => $this->input->post('content'),
                    // 'image' => $ImgPath,
                    'slug' => $slug_title,
                    'price' => $this->input->post('price_per_piece'),
                    'donation_type' => $this->input->post('donation_type'),
                    'piece_title' => $this->input->post('piece_title'),
                    'item_quantity' => $item_quantity,
                    'end_date' => $this->input->post('end_date'),
                    'user_id' => '1',
                    'target' => $this->input->post('target'),
                    'created_at' => date('d-m-Y h:i:s')

                );
                $this->db->where('slug', $slug_title);
                $this->db->update('campaign', $campaign);

                redirect('admin/view_campaign', 'location');
            } else if ($_POST['content'] == "") {

                $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));

                $name = $_FILES['filename']['name'];
                $tmp_name = $_FILES['filename']['tmp_name'];
                $type = $_FILES['filename']['type'];
                $size = $_FILES['filename']['size'];
                $error = $_FILES['filename']['error'];
                $ImgPath = "uploads/campaign/" . $name;
                if (move_uploaded_file($tmp_name, "uploads/campaign/" . $name)) {
                    $campaign = array(


                        'title' => $this->input->post('title'),
                        'custom_Slug' => $this->input->post('custom_Slug'),

                        // 'content' => $this->input->post('content'),
                        'image' => $ImgPath,
                        'slug' => $slug_title,
                        'price' => $this->input->post('price_per_piece'),
                        'donation_type' => $this->input->post('donation_type'),
                        'piece_title' => $this->input->post('piece_title'),
                        'item_quantity' => $item_quantity,
                        'end_date' => $this->input->post('end_date'),
                        'user_id' => '1',
                        'target' => $this->input->post('target'),
                        'created_at' => date('d-m-Y h:i:s')

                    );
                    $this->db->where('slug', $slug_title);
                    $this->db->update('campaign', $campaign);
                }
                redirect('admin/view_campaign', 'location');
            } else if ($_FILES['filename']['name'] == "") {
                $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));

                $campaign = array(


                    'title' => $this->input->post('title'),
                    'custom_Slug' => $this->input->post('custom_Slug'),

                    'slug' => $slug_title,
                    'content' => $this->input->post('content'),

                    'price' => $this->input->post('price_per_piece'),
                    'donation_type' => $this->input->post('donation_type'),
                    'piece_title' => $this->input->post('piece_title'),
                    'item_quantity' => $item_quantity,
                    'end_date' => $this->input->post('end_date'),
                    'user_id' => '1',
                    'target' => $this->input->post('target'),
                    'created_at' => date('d-m-Y h:i:s')

                );
                $this->db->where('slug', $slug_title);
                $this->db->update('campaign', $campaign);

                redirect('admin/view_campaign', 'location');
            } else {
                $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($_POST['title'])));

                $name = $_FILES['filename']['name'];
                $tmp_name = $_FILES['filename']['tmp_name'];
                $type = $_FILES['filename']['type'];
                $size = $_FILES['filename']['size'];
                $error = $_FILES['filename']['error'];
                $ImgPath = "uploads/campaign/" . $name;
                if (move_uploaded_file($tmp_name, "uploads/campaign/" . $name)) {
                    $campaign = array(


                        'title' => $this->input->post('title'),
                        'custom_Slug' => $this->input->post('custom_Slug'),

                        'content' => $this->input->post('content'),
                        'image' => $ImgPath,
                        'slug' => $slug_title,
                        'price' => $this->input->post('price_per_piece'),
                        'donation_type' => $this->input->post('donation_type'),
                        'piece_title' => $this->input->post('piece_title'),
                        'item_quantity' => $item_quantity,
                        'end_date' => $this->input->post('end_date'),
                        'user_id' => '1',
                        'target' => $this->input->post('target'),
                        'created_at' => date('d-m-Y h:i:s')

                    );
                    $this->db->where('slug', $slug_title);
                    $this->db->update('campaign', $campaign);
                }
                redirect('admin/view_campaign', 'location');
            }
        }
    }
    public function recentproject()
    {

        // $user_id = ($_SESSION['id']);
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/recentproject/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/recentproject/" . $name)) {
                $recentproject = array(

                    'slug' => $slug_title,
                    'title' => $this->input->post('title'),
                    'program' => $this->input->post('program'),
                    'image' => $ImgPath


                );
                $this->db->insert('recentproject', $recentproject);
            }
        }
        // redirect('home', 'location');

    }
    // public function ee(){


    //     $this->load->view('emailtemp');

    // }
    public function edit_recent_project($id)
    {


        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $data['blog_data'] = $this->admin_model->get_edit_recent_project($id);
        $this->load->view('admin/recent-project/edit-recent-project', $data);
        $this->load->view('admin/template/footer');
    }
    public function update_recent_project()
    {
        // $id = $this->uri->segment(3);
        // print_r($_POST);die;
        $id = $this->input->post('panel');
        // echo $id;die;
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/recentproject/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/recentproject/" . $name)) {
                $update_recent_project = array(


                    'title' => $this->input->post('title'),
                    'slug' => $slug_title,
                    'program' => $this->input->post('program'),
                    'image' => $ImgPath

                );
                // print_r($id);die;

                $this->db->where('id', $id);
                $this->db->update('recentproject', $update_recent_project);
                echo json_encode($update_recent_project);

                echo 'Recent Project has successfully been updated';
            }
        }
        // redirect('admin/show_recent_project', 'location');


    }
    public function save_program()
    {
        // print_r($_POST);die;

        $user_id = ($_SESSION['id']);
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/program/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/program/" . $name)) {
                $program = array(

                    'slug' => $slug_title,
                    'title' => $this->input->post('title'),
                    'content' => $this->input->post('content'),
                    'image' => $ImgPath,
                    'user_id' => $user_id,
                    'created_at' => Date('Y-m-d h:i:s')


                );

                $this->db->insert('program', $program);
                echo json_encode($program);
                // print_r($blog);die;       
            }
        }
        // redirect('admin/show_program', 'location');
    }
    public function show_program()
    {
        $data['blog_data'] = $this->admin_model->get_program();
        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/programs/show_program_data', $data);
        $this->load->view('admin/template/footer');
    }
    public function delete_program($id)
    {

        $this->admin_model->delete_program($id);
        redirect('admin/show_program', 'location');
    }
    public function delete_activities($id)
    {

        $this->admin_model->delete_activities($id);
        redirect('admin/show_activities', 'location');
    }
    public function delete_recent_project($id)
    {

        $this->admin_model->delete_recent_project($id);
        redirect('admin/show_recent_project', 'location');
    }
    public function edit_program($id)
    {


        $data['AdminData'] = $this->admin_model->get_admin();
        $this->load->view('admin/template/header', $data);
        $this->load->view('admin/template/side-bar');
        $data['blog_data'] = $this->admin_model->get_edit_program($id);
        $this->load->view('admin/programs/edit_program', $data);
        $this->load->view('admin/template/footer');
    }
    public function update_program($id)
    {

        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/program/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/program/" . $name)) {
                $update_program = array(


                    'title' => $this->input->post('title'),
                    'slug' => $slug_title,
                    'content' => $this->input->post('content'),
                    'image' => $ImgPath

                );
                // print_r($id);die;

                $this->db->where('id', $id);
                $this->db->update('program', $update_program);
                echo 'program has successfully been updated';
            }
        }
        redirect('admin/show_program', 'location');
    }
    public function createbanner()
    {

        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');
        $this->load->view('admin/bannner/createbanner');
        $this->load->view('admin/template/footer');
    }
    public function resetpassword()
    {

        $this->load->view('templates/header');
        $this->load->view('resetpassword');
        $this->load->view('templates/footer');
    }
    public function checkemail()
    {

        $email = $this->input->post('email');
        $check_email = $this->blog_model->check_email($email);
        // print_r($check_email);die;
        if ($check_email == '1') {

            $n = 4;
            $data['otp'] = $this->blog_model->generateNumericOTP($n);
            // print_r($otp2);die;

            $this->load->view('otptemplate', $data);
            $otp2 = array(

                'otp' =>  $data
            );

            $this->db->insert('otp', $data);


            $this->load->view('templates/header');
            $this->load->view('otp');
            $this->load->view('templates/footer');
            $this->sendotp($email);
        } else {
            redirect('admin/resetpassword');
        }
    }
    public function sendotp($email, $subject = null, $body = null)
    {

        $to = $email;
        $this->load->library('phpmailer_lib');
        $mail = $this->phpmailer_lib->load();
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'amitw3dev@gmail.com';
        $mail->Password = 'amit@1234';
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPDebug = 2;
        $mail->Port     = 465;
        // $mail->Host     = HOST;
        // $mail->SMTPAuth = true;
        // $mail->Username = EMAIL;
        // $mail->Password = PASSWORD;
        // $mail->SMTPSecure = SMTP_CRYPTO;
        // $mail->Port     = PORT;
        $mail->setFrom($mail->Username, 'Gyanendra Singh');
        // $mail->addReplyTo($mail->Username, 'Education hub');
        if (is_array($to)) {
            foreach ($to as $row) {
                // $mail->addAddress($row, "Education hub");
                $mail->addBCC($row, "");
            }
        } else {
            $mail->addAddress($to, "");
        }
        $mail->SMTPDebug = false;
        if ($subject != null) {
            $mail->Subject = $subject;
        } else {
            $mail->Subject = 'Welcome In Shivganga';
        }
        if ($body != null) {
            $mailContent = $body;
        } else {
            $mailContent = "hii";
        }
        $mail->isHTML(true);

        $mail->Body = $this->load->view('otptemplate', '', TRUE);
        if ($mail->send()) {
            $this->session->set_flashdata('msg', 'Password has been sent to your email address.');
            redirect('admin/otp');
        }
    }
    public function send_reciept($id, $subject = null, $body = null)
    {

        $email1 = $this->admin_model->get_user_email($id);

        // print_r($id);die;
        foreach ($email1 as $row) {
            $email = $row->email;
        }
        $data['details'] = $this->blog_model->get_payment_details($email);
        $this->load->view('emailtemp', $data);;

        $to = $email;
        $this->load->library('phpmailer_lib');
        $mail = $this->phpmailer_lib->load();
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'amitw3dev@gmail.com';
        $mail->Password = 'amit@1234';
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPDebug = 2;
        $mail->Port     = 465;
        // $mail->Host     = HOST;
        // $mail->SMTPAuth = true;
        // $mail->Username = EMAIL;
        // $mail->Password = PASSWORD;
        // $mail->SMTPSecure = SMTP_CRYPTO;
        // $mail->Port     = PORT;
        $mail->setFrom($mail->Username, 'Gyanendra Singh');
        // $mail->addReplyTo($mail->Username, 'Education hub');
        if (is_array($to)) {
            foreach ($to as $row) {
                // $mail->addAddress($row, "Education hub");
                $mail->addBCC($row, "");
            }
        } else {
            $mail->addAddress($to, "");
        }
        $mail->SMTPDebug = false;
        if ($subject != null) {
            $mail->Subject = $subject;
        } else {
            $mail->Subject = 'Welcome In Shivganga';
        }
        if ($body != null) {
            $mailContent = $body;
        } else {
            $mailContent = "hii";
        }
        $mail->isHTML(true);

        $mail->Body = $this->load->view('admin/reciept/donation_reciept', '', TRUE);
        if ($mail->send()) {
            $this->session->set_flashdata('msg', 'Password has been sent to your email address.');
            redirect('admin/adminpanel');
        }
    }
    public function otp()
    {

        $this->load->view('templates/header');
        $this->load->view('otp');
        $this->load->view('templates/footer');
    }
    public function resetpass()
    {

        $this->load->view('templates/header');
        $this->load->view('resetpassword1');
        $this->load->view('templates/footer');
    }
    public function updatepassword()
    {

        $id = ($_SESSION['id']);
        $password = md5($this->input->post('password'));
        $data = array(

            'password' => $password
        );

        $this->db->where('id', $id);
        $this->db->update('users', $data);
        redirect('home_Controller/logout');
        // $this->load->view('templates/header');
        // $this->load->view('resetpassword1');
        // $this->load->view('templates/footer');

    }
    public function checkotp2()
    {

        $q = intval($_GET['q']);
        // echo $q;die;
        $data = $this->donate_model->get_otp($q);

        if ($data == '1') {


            echo "otp Matched";
            echo '<div class="text-center mb-4">
        <input type="submit" class="btn btn-sbmit" value="Publish">
      </div>';
        } else {
            echo "otp not match";
        }
    }

    public function savebanner()
    {
        // print_r($_FILES['filename']);die;
        $user_id = ($_SESSION['id']);
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/banner/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/banner/" . $name)) {
                $banner = array(


                    'title' => $this->input->post('title'),
                    'image' => $ImgPath,
                    'user_id' => $user_id
                );
                $this->db->insert('banner', $banner);
            }
        }
        redirect('admin/show_banner', 'location');
    }
    public function createcampaign()
    {

        $this->load->view('templates/header');
        $this->load->view('admin/campaign/createcampaign');
        $this->load->view('templates/footer');
    }
    public function savecampaign()
    {

        // print_r($_FILES['filename']);die;
        $slug_title = preg_replace('/[^a-z0-9]+/i', '-', trim(strtolower($this->input->post('title'))));

        $user_id = ($_SESSION['id']);
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/campaign/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/campaign/" . $name)) {
                $campaign = array(


                    'title' => $this->input->post('title'),
                    'content' => $this->input->post('content'),
                    'image' => $ImgPath,
                    'slug' => $slug_title,
                    'target' => $this->input->post('target'),
                    'achieve' => $this->input->post('achieve'),
                    'user_id' => $user_id
                );
                $this->db->insert('campaign', $campaign);
            }
        }
        redirect('home', 'location');
    }
    public function createaboutus()
    {

        $this->load->view('templates/header');
        $this->load->view('admin/about-us/createaboutus');
        $this->load->view('templates/footer');
    }
    public function saveaboutus()
    {
        // print_r($_FILES['filename']);die;
        $user_id = ($_SESSION['id']);
        if (isset($_FILES['filename'])) {
            $name = $_FILES['filename']['name'];
            $tmp_name = $_FILES['filename']['tmp_name'];
            $type = $_FILES['filename']['type'];
            $size = $_FILES['filename']['size'];
            $error = $_FILES['filename']['error'];
            $ImgPath = "uploads/about_us/" . $name;
            if (move_uploaded_file($tmp_name, "uploads/about_us/" . $name)) {
                $aboutus = array(


                    'title' => $this->input->post('title'),
                    'sub_title' => $this->input->post('sub_title'),
                    'content' => $this->input->post('content'),
                    'image' => $ImgPath,
                    'user_id' => $user_id
                );
                $this->db->insert('about_us', $aboutus);
            }
        }
        redirect('aboutshivganga', 'location');
    }
    public function createcontribution()
    {

        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');

        $this->load->view('admin/contribution/contribution');
        $this->load->view('admin/template/footer');
    }

    public function view_contribution()
    {
        $data['contibution'] = $this->blog_model->get_contribution();

        $this->load->view('admin/template/header');
        $this->load->view('admin/template/side-bar');

        $this->load->view('admin/contribution/view_contribution', $data);
        $this->load->view('admin/template/footer');
    }
    public function savecontribution()
    {


        // $user_id = ($_SESSION['id']);

        $contribution = array(


            'title' => $this->input->post('title'),
            'price' => $this->input->post('price'),

        );
        $this->db->insert('contribution', $contribution);
        redirect('home1', 'location');
    }
    public function delete_contribution($id)
    {

        $this->admin_model->delete_contribution($id);
        redirect('admin/view_contribution', 'location');
    }


    function receipt_form()
    {


        $this->load->view('admin/receipt/donation_form');
    }

    function save_donation_data()
    {

        // echo "<pre>";print_r($_POST);die;
        $email = $_POST['email'];

        $n = 10;
        function getName1($n)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';

            for ($i = 0; $i < $n; $i++) {
                $index = rand(0, strlen($characters) - 1);
                $randomString .= $characters[$index];
            }

            return $randomString;
        }

        $servername = "localhost";
        $username = "newuser";
        $password = "1234";
        $dbname = "prs_emandate";
        // $username = "u251981260_eTest";
        // $password = "test@Abcd1";
        // $dbname = "u251981260_etest";
        $conn = new mysqli($servername, $username, $password, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $order_id = getName1($n);
        $amount = $_POST['campaign_donations'][0]['amount'];
        $cust_name = $_POST['first_name'] . " " . $_POST['last_name'];
        $cust_email = $_POST['email'];
        $cust_contact = $_POST['phone'];
        $date = date('d-m-Y h:i:s A');
        $cust_address = $_POST['address'] . ", " . $_POST['city'] . ", " . $_POST['state'] . ", " . $_POST['postcode'] . ", " . $_POST['country'];
        $cust_pan =  $_POST['pan_number'];
        $res = "INSERT  INTO  payment (payment_id,order_id,customer_id,token_id,cust_sub_id,cust_plan_id,amount,name,email1,email2,contact_no1,contact_no2,address,
        pan_no,is_success,created_at) 
        VALUES('offline','order_$order_id','cust_$order_id','token_$order_id','NA','NA','$amount','$cust_name',

        '$cust_email','$cust_email','$cust_contact','$cust_contact','$cust_address','$cust_pan','1','$date')";


        if ($conn->query($res) === TRUE) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $res . "<br>" . $conn->error;
        }


        // $sql = "SELECT * FROM payment  WHERE email1 ='$cust_email'";
        // $result = $conn->query($sql);

        // $row = $result->fetch_assoc();
        // $id = $row['id'];
        $sql2 =  "SELECT * FROM payment ORDER BY  id  DESC";
        $result2 = $conn->query($sql2);
        $last_idd = $result2->fetch_assoc();




        $idd2 = $last_idd['id'];
        $messge = array('message' => ' Data successfully Saved!', 'class' => 'alert alert-success fade in');
        $this->session->set_flashdata('item', $messge);

        $sql3 = "SELECT * FROM payment  WHERE id ='$idd2'";

        $result3 = $conn->query($sql3);
        $result_data = $result3->fetch_assoc();

        // print_r($result_data);
        // die;


        $this->send_receipt_mail2($result_data, $email);
        // header("Location:" . base_url('Admin/receipt_form'));


        // print_r($result_data['id']);
        // die;
        // $last_row = $this->db->select('id')->order_by('id', "desc")->limit(1)->get('payment')->result();


        // echo $last_row[0]->id;
        // $id = $last_row[0]->id;
        // $this->db->where('id', $id);
        // $query = $this->db->get('payment');

        // $data['details'] =  $query->result();

        // $email = $data['details'][0]->email1;
        // $this->send_receipt_mail($data, $email);
        // header("Location:" . base_url('Subscription/receipt_form'));



        // sendMail($row, $data);

        // } else {
        //     echo "0 results";
        // }

        // if ($res) {

        //     $messge = array('message' => ' Data successfully Saved!', 'class' => 'alert alert-success fade in');
        //     $this->session->set_flashdata('item', $messge);
        // $this->load->model("Subscription_model", '', TRUE);

        // $data['details'] = $this->Subscription_model->get_receipt($email);


        //     $last_row = $this->db->select('id')->order_by('id', "desc")->limit(1)->get('payment')->result();

        //     // echo $last_row[0]->id;
        //     $id = $last_row[0]->id;
        //     $this->db->where('id', $id);
        //     $query = $this->db->get('payment');

        //     $data['details'] =  $query->result();

        //     $email = $data['details'][0]->email1;
        //     $this->send_receipt_mail($data, $email);
        //     header("Location:" . base_url('Subscription/receipt_form'));
        // }
    }

    function send_receipt_mail2($data, $email)
    {


        $to = $email;
        // $to = "gs8400791@gmail.com";

        if ($data['details'][0]->payment_id == 'offline') {

            $method = '<h3>Donation : ' . $data['payment_id'] . '</h3>';
        } else {

            $method = '<h3>Donation ID : ' . $data['payment'] . '</h3>';
        }

        $date = date("F jS, Y", strtotime($data['created_at']));
        $this->load->library('phpmailer_lib');
        $mail = $this->phpmailer_lib->load();
        $mail->isSMTP();
        $mail->Host     = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'sgyanendra8400@gmail.com';
        $mail->Password = '1542310011';
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPDebug = 2;
        $mail->Port     = 465;

        $mail->setFrom($mail->Username, 'Gyanendra Singh');
        if (is_array($to)) {
            foreach ($to as $row) {
                $mail->addBCC($row, "");
            }
        } else {
            $mail->addAddress($to, "");
        }

        $mail->Subject = 'Welcome In Shivganga';


        $mailContent = 'body';


        $mail->isHTML(true);
        $email_temp = '
        
        <html>
        <head>
        <style>
        
        @media screen and (max-width: 767px) {
            .main-div{
                width: 100% !important;
            }

        }
        </style>
        </head>
        <body style=" margin: 0px; font-family: "Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;">
        <div class="main-div" style="margin: 0 auto; text-align: center; width: 60%;">
            <div class="heading-div">
                <h1 style="margin-top: 50px; margin-bottom: 50px;">Your Donation Receipt</h1>
            </div>
            <div style="border: 8px solid grey;">
                <div style="border: 4px solid grey; margin: 20px; padding-bottom: 20px;">
                    <div style ="padding: 15px;">
                        <div class="logo-div" style="margin-top: 20px;">
                            <img src="https://shivgangajhabua.org/wp-content/uploads/2019/03/sj.png" />
                        </div>
                        <div class="SSGP-text-div">
                            <h3 style="color: #83D745; margin-bottom: 10px;">Shivganga Samragra Gramvikas Parishad</h3>
                        </div>
                    </div> 
                    <div style="border-bottom: 1px solid grey;"></div>
                    <div style="padding: 15px;">
                        <p style="font-size: 14px;">IT Exemption U/S 80G, F.No. CIT-I/Ind/Tech/80G/08/09-10, Dated 24/12/2009, Sl.No. 51/2009-10 Valid from 01/04/09, Extended from AY 2021-22 to AY 2023-24 with Provisional Approval Number: AADAS4039HF20206 .<br>PAN: ' . $data['pan_no'] . '</p>
                        <h3><i>This is to certify that Shri/Smt.</i> Name: ' . $data['name'] . ' </h3>
                        <h3>Address: ' . $data['name'] . '<br>' . $data['address'] . '</h3>
                        <h3><a href="#" target="_blank"></a><i> made a doantion of</i> Amount:&#8377;' . $data['amount'] . ' on Date:<span style="font-size: 14px;">' . $date . '</span></h3>

        

                        ' . $method . '
      
                    </div>
                    <div style="margin-top: 40px;">
                        <p style="margin-bottom: 0px;">
                            THANK YOU!
                        </p>
                            <p style="margin-top: 0px;">
                                WE WILL MAKE YOU CONTRIBUTION COUNT.
                             </p>
                    </div>
                    <div style="margin-top: 30px; text-align: right; margin-right: 20px;">
                        <p>-Issued by Treasurer, Shivganga</p>
                    </div>
                </div>
            </div>
            <div style="margin-top: 50px; margin-bottom: 50px;">
                <a href="https://shivgangajhabua.org/">Shivganga</a>
            </div>
        </div>
        </body>
        </html>
       
        
        ';
        $mail->SMTPDebug = false;

        $mail->Body =  $email_temp;
        if ($mail->send()) {
            // $this->session->set_flashdata('msg', 'Password has been sent to your email address.');
            echo "The mail has been send successfully.";
            // $this->receipt();
        }
    }


    public function storePost()
    {
        $price = $this->input->post('addmore');
        function subArraysToString1($ar, $sep = ', ')
        {
            $str = '';
            foreach ($ar as $val) {
                $str .= implode($sep, $val);
                $str .= $sep; // add separator between sub-arrays
            }
            $str = rtrim($str, $sep); // remove last separator
            return $str;
        }

        $exactprice = subArraysToString1($price);
        // print_r($exactprice);die;  
        $value = array(

            'title' => $this->input->post('title'),
            'price' => $exactprice



        );
        $this->db->insert('contribution', $value);

        redirect('admin/view_contribution', 'location');
    }
}
